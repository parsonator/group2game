﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "HealthAbility")]

public class HealthAbility : Ability
{
    public float throwPower = 5;
    public float aoeRad = 3;


    public GameObject HealthPack;

    public override void InitAbility()
    {
        ObjectPoolManager.Main.createPool(HealthPack, 2);
    }



    public override void UseAbility(bool replicate = true)
    {

            if (replicate && playerControler.networked){
                Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Ability"), Client.reliableChannel);
                
            }
            else PlayerAnalytics.OnUseAbility("HealthPack");

        Transform barrle = playerControler.combat.GunBarrle;
        Rigidbody HP;
        HP = ObjectPoolManager.Main.instantiateObject(HealthPack,
                        barrle.position + (barrle.forward *0.1f),
                        Quaternion.identity).GetComponent<Rigidbody>();

        HP.velocity = (barrle.forward + (Vector3.up * 0.2f)).normalized * throwPower;

    }



    public override void RefreshAbility()
    {

    }

}

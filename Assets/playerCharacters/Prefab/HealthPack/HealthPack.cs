﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour {

	public float addHp = 50;
float t;
	public AudioClip HPack;
	AudioSource source;

private void OnEnable() {
t = 0;
		source = GetComponent<AudioSource> ();
		if (HPack == null) {
			Debug.Log ("Health pack sound not assigned");
		}
}
private void Update() {
	t+= Time.deltaTime;
	if(t>2)
	{
		AOEHealth(transform.position,1,addHp);
	}
}
public void AOEHealth (Vector3 poz, float rad, float hp) {
		List<Attributes> hits = new List<Attributes> ();
		Collider[] c = Physics.OverlapSphere (poz, rad);

		for (int i = 0; i < c.Length; i++) {

			Attributes a;
			if (a = c[i].GetComponentInParent<Attributes> ()) {
				if (!hits.Contains (a)) {
					hits.Add (a);
				}
			}
		}

		if (hits.Count != 0) {
			foreach (Attributes a in hits) {
				a.AddHeath (hp);
				source.clip = HPack;
				source.Play ();
				ObjectPoolManager.Main.destroyPooledObject(gameObject);
				t = 0;
				
				return;
			}
		}
		// hit.collider.gameObject.SendMessageUpwards("Damage", damage, SendMessageOptions.DontRequireReceiver);
	}
}
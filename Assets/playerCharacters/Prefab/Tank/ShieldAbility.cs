﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ShieldAbility")]

public class ShieldAbility : Ability
{
    public float throwPower = 5;
    public GameObject shield;

    public override void InitAbility()
    {
        ObjectPoolManager.Main.createPool(shield, 6);
    }
    // public override void SetUpAbility(float _abilityCooldownTime,GameObject _shield, float _throwPower) : bace.SetUpAbility(_abilityCooldownTime)
    // {
    //     // shield n
    // }



    public override void UseAbility(bool replicate = true)
    {

            if (replicate && playerControler.networked){
                Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Ability"), Client.reliableChannel);
                
            }
            else PlayerAnalytics.OnUseAbility("ShieldAbility");

        Transform barrle = playerControler.combat.GunBarrle;
        Rigidbody shieldRB;
        shieldRB = ObjectPoolManager.Main.instantiateObject(this.shield,
                        barrle.position + (barrle.forward *0.1f),
                        Quaternion.LookRotation(playerControler.transform.forward)).GetComponent<Rigidbody>();

        shieldRB.velocity = (barrle.forward + (Vector3.up * 0.2f)).normalized * throwPower;

    }



    public override void RefreshAbility()
    {

    }

}

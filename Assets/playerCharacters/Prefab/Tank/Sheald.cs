﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheald : MonoBehaviour {

	Rigidbody rigidbody;
	public float lifeTime = 20;

	public AudioClip Throw;
	AudioSource audioSource;

	// Use this for initialization
	private void OnEnable() {
		if(audioSource == null) audioSource = GetComponent<AudioSource>();
		audioSource.PlayOneShot(Throw);
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.isKinematic = false;
		rigidbody.useGravity = true;
		GetComponent<Animator>().SetTrigger("Start");
		StartCoroutine(ShealdLife());
	}
	IEnumerator ShealdLife () {
		yield return new WaitForSeconds (lifeTime);
		ObjectPoolManager.Main.destroyPooledObject(gameObject);
	}

	private void Update() {
		if(rigidbody.velocity == Vector3.zero && rigidbody.isKinematic == false){
		rigidbody.isKinematic = true;
		rigidbody.useGravity = false;
		}

		
	}

}

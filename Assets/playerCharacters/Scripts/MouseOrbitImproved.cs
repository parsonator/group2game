﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved : MonoBehaviour
{
    public Transform target;
    public Vector3 lookOffset;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;
    public float ZoomSpeed = 10f;
    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;
    public float mouseDampening = 1f;
    public LayerMask CamNoCollide;
    public bool coll;
    public float camRecoverySpeed = .5f;
    public bool RightSide;
    public float distanceMin = .5f;
    public float distanceMax = 15f;
    private Rigidbody camRigidbody;
    float x = 0.0f;
    [SerializeField]
    float y = 0.0f;
    [SerializeField]
    Vector3 camAngle;
    float PlayerY;

    // Use this for initialization
    void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        camRigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (camRigidbody != null)
        {
            camRigidbody.freezeRotation = true;
        }
    }
    void setFocus(bool focus)
    {
        if (focus)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    bool focus = true;
    void LateUpdate()
    {

        if (Input.GetKeyDown("b"))
        {
            setFocus(!focus);
        }

        if (target)
        {
            Vector3 lookPoz;
            if (RightSide)
            {
                lookPoz = target.position + ((target.right * lookOffset.x) + (target.up * lookOffset.y));
            }
            else
            {
                lookPoz = target.position + ((-target.right * lookOffset.x) + (target.up * lookOffset.y));
            }

            RaycastHit hit;
            Vector3 v = transform.position + (-transform.forward * 0.1f);
            coll = Physics.Raycast(lookPoz, v - lookPoz, out hit, Vector3.Distance(lookPoz, v), CamNoCollide);
            Debug.DrawLine(transform.position, lookPoz);
            distance = Mathf.MoveTowards(distance, distanceMax, Time.deltaTime * camRecoverySpeed);

            if (coll == true)
            {
                distance = Vector3.Distance(lookPoz, hit.point);
            }

            PlayerY = target.eulerAngles.y;
            Vector3 playerAngle = target.eulerAngles;

            x = PlayerInput.MouseAxis().x * xSpeed* (mouseDampening*2) * 0.01f;
            y -= PlayerInput.MouseAxis().y * ySpeed * mouseDampening * 0.01f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);

            //camAngle = Vector3;
            camAngle = transform.eulerAngles - target.eulerAngles;

            camAngle = transform.eulerAngles - target.eulerAngles;

            Quaternion rotation = Quaternion.Euler(y, PlayerY, 0);
            target.transform.Rotate(new Vector3(0, x, 0));

            // distance = Mathf.Lerp(distance, Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * ZoomSpeed, distanceMin, distanceMax), 0.2f);

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + lookPoz;

            transform.rotation = rotation;
            transform.position = position;

        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {

        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;

        return Mathf.Clamp(angle, min, max);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "GrenadeAbility")]

public class GrenadeAbility : Ability
{
    public float throwPower = 10;
    public float damage = 50;
    public GameObject Grenade;
    public override void InitAbility()
    {
        ObjectPoolManager.Main.createPool(Grenade, 10);
    }

    public void throwGrenade()
    {
        Transform barrle = playerControler.combat.GunBarrle;
        Rigidbody thrownGrenade;
        thrownGrenade = ObjectPoolManager.Main.instantiateObject(Grenade,
                        barrle.position + (barrle.forward *0.1f),
                        barrle.rotation).GetComponent<Rigidbody>();

        thrownGrenade.velocity = (thrownGrenade.transform.forward + (Vector3.up * 0.2f)).normalized * throwPower;
    }


    public override void UseAbility(bool replicate = true)
    {

            if (replicate && playerControler.networked){
                Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Ability"), Client.reliableChannel);
                
            }
            else PlayerAnalytics.OnUseAbility("GRENADE");
            throwGrenade();


    }



    public override void RefreshAbility()
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public float abilityCooldownTime = 10;

    [HideInInspector]
    public PlayerControler playerControler;
    

    public abstract void UseAbility(bool replicate = true);
    public abstract void RefreshAbility();
    public abstract void InitAbility();
    public virtual void SetUpAbility(float _abilityCooldownTime)
    {
        abilityCooldownTime = _abilityCooldownTime;
    }

}

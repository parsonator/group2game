﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbility : MonoBehaviour {

	public Ability ability;
	public List<Ability> allAbility;

	public float cooldown = 0;
    public bool abilityReady;

    public float abilityCooldownPercentage{
        get
        {
            return cooldown / ability.abilityCooldownTime;
        }
    }

	private void Awake() {
		ability.playerControler = GetComponent<PlayerControler>();
		ability.InitAbility();
	} 
	// Update is called once per frame
	void Update () {
		
	}
}

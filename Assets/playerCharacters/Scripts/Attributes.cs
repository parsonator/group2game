﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Attributes : MonoBehaviour, IKillable, IDamageable<float>
{

    public delegate void OnDeath();

    public OnDeath onDeath;
    public float MaxHeath =100;

    public float Health = 100;

    public int XP = 10;
    public bool invulnerable;
    bool Dead;
    public AudioClip DamageSound;
    AudioSource aSource;
    void Start()
    {
        Health = MaxHeath;
        aSource = GetComponent<AudioSource>();
    }

    public void Damage(float damageTaken)
    {
        if(!invulnerable){
            Health -= damageTaken;
            aSource.clip = DamageSound;
            aSource.Play();
        }
        if(GetComponent<PlayerControler>())
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.HEALTH, Health), Client.reliableChannel);
    }
    public void Kill()
    {
        Health = 0;
        Dead = true;
    }

    public bool IsDead()
    {
        return Dead;
    }

    public bool CheckDead()
    {
        if (Health <= 0)
        {
            Kill();
            return true;
        }
        else
        {
            Dead = false;
            return false;
        }
    }

    public void Revive()
    {
        Dead = false;
        gameObject.SetActive(true);
        Health = MaxHeath;
    }

    public void fullHealth()
    {
        Health = MaxHeath;
    }

    public float GetHealthPercent()
    {
        return Health / MaxHeath;
    }

    public float AddHeath(float HP)
    {
        Health += HP;
        Client.Send(NetworkParser.ParseIn(NetworkMsgType.HEALTH, Health), Client.reliableChannel);
        return Health =  Mathf.Clamp(Health, 0, MaxHeath);
    }






}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

	// public Transform iconPivot;

	// Camera mainCamera;
	HudManager hudManager;
	PlayerControler controler;

	public void Init()
	{
		controler = GetComponent<PlayerControler>();
		hudManager = GameObject.Find("PlayerHUD").GetComponent<HudManager>();
		if(GameObject.Find("DamageMarkerContainer") == null){
			this.gameObject.GetComponent<PlayerUI>().enabled = false;
			return;
		}

		// iconPivot = GameObject.Find("DamageMarkerContainer").transform;
		// iconPivot.gameObject.SetActive(false);
		// mainCamera = Camera.main;
	}

    public void hitMark(Vector3 poz)
    {
		// if(!controler.livePlayer)
        // hudManager.ShowDamageMarker(poz,transform);
    }

	public void Start()
	{
		controler = GetComponent<PlayerControler>();
		hudManager = GameObject.Find("PlayerHUD").GetComponent<HudManager>();

		
			if(GameObject.Find("DamageMarkerContainer") == null){
				this.gameObject.GetComponent<PlayerUI>().enabled = false;

				return;
			}

		// }
	}

	void Update()
	{
		hudManager.statusBarManager.hpBarValue = controler.attributes.GetHealthPercent();
		hudManager.statusBarManager.currentAmmo = (int)controler.combat.currentAmmo;
		hudManager.statusBarManager.maxAmmo = (int)controler.combat.GunPrefs.maxAmmo;
		hudManager.statusBarManager.abilityCoolDown = controler.combat.playerAbility.abilityCooldownPercentage;
		bool dead = controler.attributes.IsDead();

		hudManager.setCrosshairDilution(controler.combat.AccuracyP());

		hudManager.Crosshair.setElementActive(!dead);
		hudManager.RespawnCountDownTxt.setElementActive(dead);
		
		if(controler.attributes.IsDead())
			hudManager.RespawnTxt.text = controler.secondsToRevive.ToString();
	
	}

	// IEnumerator CountDown(){
	// 	yield return new WaitForSeconds(2);
	// 	iconPivot.gameObject.SetActive(false);
	// }

	// public void ShowDamageMarker(Vector3 attackerPos){
	// 	if(iconPivot != null){
	// 		//if the icon pivot was not visible make it visible and get a reference to the camera
	// 		if(iconPivot.gameObject.activeSelf == false)
	// 		{
	// 			iconPivot.gameObject.SetActive(true);
	// 			mainCamera = Camera.main;
	// 		}
	// 		//if we are in here we want to stop all the coroutines started 
	// 		//that would make the marker disappear
	// 		StopAllCoroutines();

	// 		/*
	// 		**LET'S MATH AROUND A BIT
	// 		*/
	// 		//We need the direction from the attacker to the player
	// 		Vector3 playerAttackerDir = (attackerPos - transform.position).normalized;
	// 		//But we only need a straight line so we don't consider the height and exclude the Y axis
	// 		playerAttackerDir = new Vector3(playerAttackerDir.x, 0, playerAttackerDir.z);
	// 		//We then need the forward vector of the camera but only on the Y axis
	// 		if(mainCamera == null)
	// 			Debug.Log("hi");
	// 		Vector3 camForward = Quaternion.Euler(0, mainCamera.transform.eulerAngles.y, 0) * Vector3.forward; 
	// 		//We need to get an angle based on the two unit vectors calculated and we know that
	// 		//arctan2(b_y, b_x) - arctan2(a_y, a_x) gives us exactly the angle
	// 		float angleRadCam = (Mathf.Atan2(playerAttackerDir.z, playerAttackerDir.x) - Mathf.Atan2(camForward.z, camForward.x));
	// 		//We need the angle in degrees
	// 		float angleDegCam = (180 / Mathf.PI) * angleRadCam;
	// 		//The angle returned will be between 0-270 then we get a negative angle
	// 		//If it's negative just add 360
	// 		angleDegCam = angleDegCam < 0 ? (angleDegCam + 360) : angleDegCam;
	// 		//Finally we can apply the rotation
	// 		iconPivot.rotation = Quaternion.Euler(0, 0, angleDegCam);
	// 	}
	// }

	IEnumerator RespawnCountDown(GameObject respawnTxt, bool replicate = true){
		int seconds = 8;
		while(seconds > 0){
			yield return new WaitForSeconds(1);
			seconds--;
			hudManager.RespawnTxt.text = seconds.ToString();
		}
		
		// Revive(Client.ourClientID);
	}

// called form combat
	// public void UpdateHealthBar(float damage){
	// 	if(GetComponent<PlayerControler>().connectionID == Client.ourClientID){
	// 		Image slider = GameObject.Find("HealthBar").GetComponent<Image>();
	// 		float value = slider.fillAmount;
	// 		slider.fillAmount = value - damage;

	// 		if(value <= 0){
	// 			Die();
	// 			GameObject respawnTxt = GameObject.Find("RespawnCountDownTxt");
	// 			respawnTxt.GetComponent<CanvasGroup>().alpha = 1;
	// 			StartCoroutine(RespawnCountDown(respawnTxt));
	// 		}
	// 	}
	// }

	// public void Die(bool replicate = true){
	// 	if(replicate && GetComponent<PlayerControler>().networked)
	// 		Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Die"), Client.reliableChannel);
		
	// 	gameObject.transform.Find("MainCharacterMesh").gameObject.SetActive(false);
	// 	if(GetComponent<PlayerControler>().connectionID == Client.ourClientID){
	// 		GetComponent<PlayerCombat>().enabled = false;
	// 		GetComponent<PlayerControler>().enabled = false;
	// 		GameObject.Find("Crosshair").GetComponent<CanvasGroup>().alpha = 0;
	// 	}else{
	// 		gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(false);
	// 	}


	// }

	// public void Revive(int cnnID = 0, bool replicate = true){
	// 	if(replicate && GetComponent<PlayerControler>().networked)
	// 		Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Revive"), Client.reliableChannel);		

	// 	if(GetComponent<PlayerControler>().connectionID == Client.ourClientID){
	// 		GetComponent<PlayerCombat>().enabled = true;
	// 		GetComponent<PlayerControler>().enabled = true;
	// 		GameObject.Find("HealthBar").GetComponent<Image>().fillAmount = 1;
	// 		Client.UpdateSpawnCount(Client.ourPlayerTeam);
	// 		GameObject.Find("RespawnCountDownTxt").GetComponent<CanvasGroup>().alpha = 0;
	// 		GameObject.Find("Crosshair").GetComponent<CanvasGroup>().alpha = 1;
	// 		GameObject.Find("RespawnCountDownTxt").GetComponent<Text>().text = "8";
	// 	}else{
	// 		gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(true);
	// 	}

	// 	if(Client.players[cnnID].team == Team.BLUE){
	// 		gameObject.transform.position = Client.spawnPointsBlue[Client.spawnPointIndexBlue].transform.position;
	// 	}else if(Client.players[cnnID].team == Team.RED){
	// 		gameObject.transform.position = Client.spawnPointsRed[Client.spawnPointIndexRed].transform.position;
	// 	}

	// 	gameObject.transform.Find("MainCharacterMesh").gameObject.SetActive(true);
	// }
}

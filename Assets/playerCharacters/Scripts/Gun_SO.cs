﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CreatGun")]

public class Gun_SO : ScriptableObject {

    public float damage = 50;
    public float maxAmmo = 30;
    public float reloadTime = 2;
    public float accuracy = 0.1f;
    public float stability = 0.05f;
    public float range = 60;
    public float Zoom = 40;

    public float hitPower = 10;
    public float shotDelay = 0.5f;
    [Range(0f,1f)]
    public float penetration = 0;

    public bool CanShoot, CanExplode;

    public bool rapidFire = true;
    public bool slowReload = false;

    public GameObject Shoot, Hit, eHit;

    public AudioClip gunSound;
    public AudioClip reloadSound;


}

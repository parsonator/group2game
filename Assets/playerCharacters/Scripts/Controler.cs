﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controler : MonoBehaviour {

	CharacterController characterController;
	
	// Use this for initialization
	void Start()
	{
		if(gameObject.GetComponent<CharacterController>() == null)
			characterController =  gameObject.AddComponent<CharacterController>();
		else
		characterController = GetComponent<CharacterController>();
	}
	
	public void MoveCharacter(Vector3 _direction)
	{
		characterController.Move(_direction);
	}

	public void applyGravity()
	{
		characterController.Move(Physics.gravity * Time.deltaTime);
	}


	public void Jump()
	{

	}

	public void Dash()
	{

	}

}

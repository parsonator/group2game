﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour {

	public float aoeRad = 3;
	public float lifeTime = 5;
	public float damage = 50;
	public GameObject grenadeExplosion;
	static bool  setupDone = false;
	AudioSource source;
	//public AudioClip spawn;
	public AudioClip explode;
	public AudioClip thrown;
	private void OnEnable() {
		ObjectPoolManager.Main.createPool (grenadeExplosion, 10);
		StartCoroutine (GrenadeLife ());
		source = GetComponent<AudioSource>();
		source.clip = thrown;
		source.Play();
	}
	IEnumerator GrenadeLife () {
		ParticleSystem explosion = ObjectPoolManager.Main.instantiateObject (grenadeExplosion,
			transform.position,
			transform.rotation).GetComponent<ParticleSystem> ();

		yield return new WaitForSeconds (lifeTime);

		AOEDammage (this.transform.position, aoeRad, damage);
		explosion.transform.position = this.transform.position;
		explosion.transform.rotation = Quaternion.Euler (-90, 0, 0);
		explosion.transform.localScale = Vector3.one * aoeRad * 2;
		source.clip = explode;
		source.Play();
		explosion.Play ();


		yield return new WaitForSeconds (explosion.duration);

		ObjectPoolManager.Main.destroyPooledObject (explosion.gameObject);
		ObjectPoolManager.Main.destroyPooledObject (gameObject);
	}

	public void AOEDammage (Vector3 poz, float rad, float damage) {
		List<Attributes> hits = new List<Attributes> ();
		Collider[] c = Physics.OverlapSphere (poz, rad);

		for (int i = 0; i < c.Length; i++) {

			Attributes a;
			if (a = c[i].GetComponentInParent<Attributes> ()) {
				if (!hits.Contains (a)) {
					hits.Add (a);
					print (c[i].gameObject.name);
				}
			}
		}

		if (hits.Count != 0) {
			foreach (Attributes a in hits) {
				a.Damage (damage);
			}
		}
		// hit.collider.gameObject.SendMessageUpwards("Damage", damage, SendMessageOptions.DontRequireReceiver);
	}
}
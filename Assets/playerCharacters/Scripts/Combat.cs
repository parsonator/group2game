﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Combat : MonoBehaviour
{
	[HideInInspector]
	public PlayerControler playerControler;

	public bool Shooting, reloading;
	public Transform GunBarrle;

	public float currentAmmo = 6;

	public Gun_SO GunPrefs;
	public List<Gun_SO> allGuns;
	public float currentAccuracy;
	[SerializeField] AudioSource source;
	public AudioClip dealDamage;
	

	ParticleSystem shootFX, HitFX, entityHit;
	LineRenderer shotLine;
	Light MussleLight;

	void Start()
	{
		if(source == null)
		source = GunBarrle.GetComponent<AudioSource>();
		// if (Shoot == null)
		// { Shoot = Resources.Load<GameObject>("Effects/ShootFX"); }

		// if (Hit == null)
		// { Hit = Resources.Load<GameObject>("Effects/HitFX"); }

		ShootSetup();


		if (GunPrefs.CanShoot)
		{
			ShootSetup();
		}

		currentAmmo = GunPrefs.maxAmmo;

	}

	public IEnumerator Reload()
	{
		source.PlayOneShot(GunPrefs.reloadSound);

		if (reloading)
			yield break;

		reloading = true;

		yield return new WaitForSeconds(GunPrefs.reloadTime);
		currentAmmo = GunPrefs.maxAmmo;
		reloading = false;

	}

	public float AccuracyP()
	{
		return (currentAccuracy == 0) ? 0 :  currentAccuracy/GunPrefs.accuracy;
	}
	public IEnumerator SlowReload()
	{
		source.PlayOneShot(GunPrefs.reloadSound);

		while (currentAmmo != GunPrefs.maxAmmo)
		{
			reloading = true;

			currentAmmo++;
			yield return new WaitForSeconds(GunPrefs.reloadTime / GunPrefs.maxAmmo);

		}
		reloading = false;
	}
	public class ShotData
	{
		public Vector3 noAngle;
		public int team;
	}



	public Attributes ShootGun(Vector3 accuracy, int team, bool replicate = true)
	{
		source.PlayOneShot(GunPrefs.gunSound);

		// print(string.Format("{0},{1}",(Team)team));
		LayerMask Mask;
		float range = 20;
		RaycastHit hit;
		Attributes attributes = null;

		if (replicate && playerControler.networked)
		{
			string msg = NetworkParser.ParseIn(NetworkMsgType.SHOOT, new string[]{  accuracy.x.ToString(),
																					accuracy.y.ToString(),
																					accuracy.z.ToString(),
																					team.ToString(),
																					} );
			msg = NetworkParser.ParseIn(msg, GunBarrle.position); //Add the gunbarrable position as the last argument

			Client.Send(msg, Client.reliableChannel);

			Mask = GlobleLayers.DetectEneimes;
		}
		else Mask = GlobleLayers.DetectPlayer;
		GunPrefs.CanShoot = true;

		if (shootFX == null)
			ShootSetup();
		shootFX.transform.position = GunBarrle.position;
		shootFX.transform.rotation = Quaternion.LookRotation(GunBarrle.forward);
		shootFX.Play();
		currentAmmo--;
		

		Vector3 latsHit = Vector3.zero;
		ParticleSystem hitEffect = HitFX;
		if (Physics.Raycast(GunBarrle.position, accuracy, out hit, GunPrefs.range, Mask))
		{
			Debug.DrawLine(hit.point, Camera.main.transform.position);
			int hitLayer = hit.transform.gameObject.layer;
			int enemyLayer = (team == 1) ? 14 : 13;
			int teamLayer = (team == 1) ? 13 : 14;
			// print(string.Format("team {1} hit {0}, vs {2}", hitLayer, enemyLayer, teamLayer));

			// if (IsInLayerMask(hit.transform.gameObject.layer, GlobleLayers.DetectEneimes))
			if (hitLayer == enemyLayer || hitLayer == 8)//hits player
			{
				latsHit = hit.point;
				if (attributes = hit.collider.gameObject.GetComponentInParent<Attributes>())
				{
					if (!attributes.IsDead())
					{
						float damage = GunPrefs.damage;
						switch (hit.collider.gameObject.tag)
						{
							case "HBlow":
								damage *= 0.5f;
								source.clip = dealDamage;
								source.Play();
								break;
							case "HBhigh":
								damage *= 2f;
								damage *= 0.5f;
								source.clip = dealDamage;
								source.Play();
								break;
						}	

						attributes.Damage(damage);
						if(hit.transform.root.tag == "Player")
							{
							HudManager[] hudManagers = Resources.FindObjectsOfTypeAll<HudManager>();
							foreach(HudManager hud in hudManagers)
							{
								if(hud.isActiveAndEnabled)
									hud.ShowDamageMarker(transform, hit.transform);
							}
						}

						if (attributes.CheckDead())
						{
							onKill(attributes);
						}
						hit.collider.gameObject.SendMessageUpwards("hitMark", transform.position, SendMessageOptions.DontRequireReceiver);
					}
					hitEffect = entityHit;
				}
			}
			else if (hitLayer == teamLayer || hitLayer == enemyLayer)
			{
				latsHit = hit.point;
				hitEffect = entityHit;
			}
			else
			{
				latsHit = hit.point;
				hitEffect = HitFX;
			}
		}
		if (latsHit != Vector3.zero)
		{
			StartCoroutine(shootEffect(GunBarrle, latsHit));
			hitEffect.gameObject.transform.position = latsHit;
			hitEffect.Play();
		}
		else
		{
			StartCoroutine(shootEffect(GunBarrle, GunBarrle.position + (accuracy * range)));
		}
		return attributes;
	}


	void onKill(Attributes killed)
	{

		//send Analytics   
		if(killed.GetComponent<PlayerControler>() != null){
		PlayerAnalytics.OnDeath(killed.GetComponent<PlayerControler>().PlayerAbilityClass.ToString(),
			playerControler.PlayerAbilityClass.ToString(), GunPrefs.name);

		PlayerAnalytics.GunKills(GunPrefs.name);

		//send to server                            
		Client.Send(NetworkParser.ParseIn(NetworkMsgType.GAMEMODE, new string[]{
								((int)GetComponent<PlayerControler>().team).ToString(),
								"0",
								"1"
							}), Client.reliableChannel);
		}
							
	}

	IEnumerator shootEffect(Transform Barrle, Vector3 hitPoint)

	{
		bool Effect = true;
		shotLine.useWorldSpace = true;

		Vector3 hit = hitPoint;
		Vector3 barrle = Barrle.position;
		// MussleLight.enabled = true;

		while (Effect)
		{
			shotLine.SetPosition(0, barrle);
			shotLine.SetPosition(1, hit);
			yield return new WaitForSeconds(0.001f);
			Effect = false;

		}
		shotLine.useWorldSpace = false;
		// MussleLight.enabled = false;

		shotLine.SetPosition(0, transform.InverseTransformPoint(barrle));
		shotLine.SetPosition(1, transform.InverseTransformPoint(barrle));
	}

	void ShootSetup()
	{
		GameObject Particles;
		Particles = Instantiate(GunPrefs.Shoot, transform.position, Quaternion.identity) as GameObject;
		shootFX = Particles.GetComponent<ParticleSystem>();
		shootFX.transform.parent = transform;
		shotLine = shootFX.gameObject.GetComponent<LineRenderer>();

		// MussleLight = shootFX.GetComponent<Light>();
		// MussleLight.enabled = false;
		Particles = Instantiate(GunPrefs.Hit, transform.position, Quaternion.identity) as GameObject;
		HitFX = Particles.GetComponent<ParticleSystem>();
		HitFX.transform.parent = transform;

		Particles = Instantiate(GunPrefs.eHit, transform.position, Quaternion.identity) as GameObject;
		entityHit = Particles.GetComponent<ParticleSystem>();
		entityHit.transform.parent = transform;

	}

	public void AOEDammage(Vector3 poz, float rad, float damage)
	{
		List<Attributes> hits = new List<Attributes>();
		Collider[] c = Physics.OverlapSphere(poz, rad);

		for (int i = 0; i < c.Length; i++)
		{

			Attributes a;
			if (a = c[i].GetComponentInParent<Attributes>())
			{
				if (!hits.Contains(a))
				{
					hits.Add(a);
					print(c[i].gameObject.name);
				}
			}
		}

		if (hits.Count != 0)
		{
			foreach (Attributes a in hits)
			{
				a.Damage(damage);
			}
		}
		// hit.collider.gameObject.SendMessageUpwards("Damage", damage, SendMessageOptions.DontRequireReceiver);
	}

	public static bool IsInLayerMask(int layer, LayerMask layermask)
	{
		return layermask == (layermask | (1 << layer));

	}




}

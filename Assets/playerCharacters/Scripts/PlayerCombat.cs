﻿using UnityEngine;
using System.Collections;

public class PlayerCombat : Combat
{
    public PlayerAbility playerAbility;
    public AudioSource CombatSounds;
    Animator animator;
    //float currentAccuracy; this is already declared in combat.cs the field needs to be protected in order to be accessible in a child class

    void Awake()
    {
        playerAbility = GetComponent<PlayerAbility>();
        CombatSounds = GetComponent<AudioSource>();
        if (GunPrefs == null)
            Debug.LogError("No Gun Set");
        currentAmmo = GunPrefs.maxAmmo;

        playerControler = GetComponent<PlayerControler>();
        animator = GetComponent<Animator>();

    }
    // Update is called once per frame
    void Update()
    {

        GunBarrle.LookAt(PlayerAiming.CroshairHit);
        if (PlayerInput.shootingButton() && !reloading)
        {
            // animator.SetBool("Fire", true);
            if (currentAccuracy <= GunPrefs.accuracy)
                currentAccuracy = Mathf.MoveTowards(currentAccuracy, GunPrefs.accuracy, GunPrefs.stability * Time.deltaTime);

            StartCoroutine(NormalShoot());

        }
        else
        {
            // animator.SetBool("Fire", false);
            currentAccuracy = Mathf.MoveTowards(currentAccuracy, 0, GunPrefs.stability*2 * Time.deltaTime);
        }

        if (PlayerInput.AbilityButtonDown() && playerAbility.abilityReady)
        {
            playerAbility.ability.UseAbility();
            playerAbility.cooldown = 0;
            playerAbility.abilityReady = false;

        }

        if (currentAmmo <= 0 || PlayerInput.ReloadButtonDown())
        {
            if (GunPrefs.slowReload){
                StartCoroutine(SlowReload());

            }
            else {StartCoroutine(Reload());

            }
        }
        Debug.DrawLine(GunBarrle.position, GunBarrle.forward * 1000, Color.red);


        if (!playerAbility.abilityReady)
        {
            playerAbility.cooldown += Time.deltaTime;
            if (playerAbility.cooldown > playerAbility.ability.abilityCooldownTime)
            {
                playerAbility.abilityReady = true;
            }
        }
    }

    public IEnumerator NormalShoot()
    {
        if (Shooting) yield break;

        Shooting = true;
        Vector3 noAngle = GunBarrle.forward;
        noAngle += new Vector3(Random.Range(-currentAccuracy, currentAccuracy), Random.Range(-currentAccuracy, currentAccuracy), Random.Range(-currentAccuracy, currentAccuracy));
        Attributes EnemyHit = ShootGun(noAngle, (int)playerControler.team);



        yield return new WaitForSeconds(GunPrefs.shotDelay);
        Shooting = false;
    }





    public void FootL()
    {

    }

    public void FootR()
    {

    }
}

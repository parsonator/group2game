﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {
 CursorLockMode wantedMode;
    bool mouseLock;
    void Start() {
        SetCursorState();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Cursor.lockState = wantedMode = CursorLockMode.None;
            mouseLock = !mouseLock;

            if(mouseLock)
            {
                LockCursor();
            }
            else
            {
                unLockCursor();
            }
        }

    }
    // Apply requested cursor state
    void SetCursorState()
    {
        Cursor.lockState = wantedMode;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != wantedMode);
    }

    public void LockCursor()
    {
        wantedMode = CursorLockMode.Locked;
        SetCursorState();
        
    }

    public void unLockCursor()
    {
        wantedMode = CursorLockMode.None;
        SetCursorState();
        
    }

    // void OnGUI()
    // {
    //     GUILayout.BeginVertical();
    //     // Release cursor on escape keypress


    //     switch (Cursor.lockState)
    //     {
    //         case CursorLockMode.None:
    //             GUILayout.Label("Cursor is normal");
    //             if (GUILayout.Button("Lock cursor"))
    //                 wantedMode = CursorLockMode.Locked;
    //             if (GUILayout.Button("Confine cursor"))
    //                 wantedMode = CursorLockMode.Confined;
    //             break;
    //         case CursorLockMode.Confined:
    //             GUILayout.Label("Cursor is confined");
    //             if (GUILayout.Button("Lock cursor"))
    //                 wantedMode = CursorLockMode.Locked;
    //             if (GUILayout.Button("Release cursor"))
    //                 wantedMode = CursorLockMode.None;
    //             break;
    //         case CursorLockMode.Locked:
    //             GUILayout.Label("Cursor is locked");
    //             if (GUILayout.Button("Unlock cursor"))
    //                 wantedMode = CursorLockMode.None;
    //             if (GUILayout.Button("Confine cursor"))
    //                 wantedMode = CursorLockMode.Confined;
    //             break;
    //     }

    //     GUILayout.EndVertical();

    //     SetCursorState();
    // }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Cinemachine;
public class CameraController : MonoBehaviour
{
	public Vector2 rotateSpeed;
	public Transform player;
	public Vector3 offset;
	public float currentDistance; 
	public float minDistence; 
	public float maxDistence; 

	
	public Vector2 lookDirection;
	public Vector3 lookPoz;

	CinemachineVirtualCamera playerCamera;

	void Start()
	{
		playerCamera = GetComponent<CinemachineVirtualCamera>();
	}

	void Update()
	{
		lookDirection = PlayerInput.MouseAxis();
		lookPoz = player.position + ((player.right*offset.x) + (player.up*offset.y));
		// transform.position = transform.forward * currentDistance;

		transform.LookAt(lookPoz);
		Vector3 h = lookPoz - transform.position;
		transform.position = Vector3.MoveTowards(transform.position, h * currentDistance,1);
		OrbitCamera(lookPoz,-player.right,0,lookDirection.y*rotateSpeed.y);
		// playerCamera.gameObject.transform.RotateAround(player.position + offset, Vector3.left, lookDirection.y);
	}
	
	public void OrbitCamera(Vector3 target,Vector3 _left, float y_rotate, float x_rotate)
	{
		Vector3 angles = transform.eulerAngles;
		angles.z = 0;
		transform.eulerAngles = angles;
		transform.RotateAround(target, Vector3.up, y_rotate);
		transform.RotateAround(target, _left, x_rotate);
	}

	// float collectionsChecker(Transform _target,float _checkRad)
	// {
	// 	if(Physics.CheckCapsule(transform.position,_target.position,_checkRad)
	// 	{
	// 		transform.position
	// 	}
	// }

	void LateUpdate()
	{


	}
}
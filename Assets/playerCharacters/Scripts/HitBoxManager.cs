﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxManager : MonoBehaviour
{

    public int mask = 11;
    public List<BoneHitbox> Hitboxbones;
    [HideInInspector]
    public List<GameObject> Hitboxs;

    public void setLayer(int layer)
    {
        foreach (GameObject obj in Hitboxs)
        {
            obj.layer = layer;
        }
    }

    public void makeBones()
    {
        delBones();
        foreach (BoneHitbox bone in Hitboxbones)
        {
            bone.name = bone.boneTransform.name + "_hitbox";
            GameObject Hitbox = makeHitBox(bone);
            switch (bone.damageMultiplayer)
            {
                case BoneHitbox.DamageMultiplayer.NONE:
                    Hitbox.tag = "HBnormal";
                    break;

                case BoneHitbox.DamageMultiplayer.LOW:
                    Hitbox.tag = "HBlow";
                    break;

                case BoneHitbox.DamageMultiplayer.HIGH:
                    Hitbox.tag = "HBhigh";
                    break;
            }
            Hitboxs.Add(Hitbox);
        }
    }
    public void delBones()
    {
        if (Hitboxs.Capacity != 0)
        {
            foreach (GameObject Hitbox in Hitboxs)
            {
                DestroyImmediate(Hitbox);
            }
            Hitboxs.Clear();
        }
        Hitboxs.Capacity = 0;
    }


    public GameObject makeHitBox(BoneHitbox bone)
    {
		Transform _from = bone.boneTransform;
		Transform _to = (bone.perentOverride ==null) ? bone.boneTransform.GetChild(0):bone.perentOverride;
        // GameObject hb = Instantiate(new ,_to.position,Quaternion.LookRotation(_from.position));
        float lenth = Vector3.Distance(_to.position, _from.position);
        GameObject hb = new GameObject(_to.gameObject.name + "_hitbox");
        hb.transform.position = _from.position;
		hb.transform.localPosition += bone.positionOffset;
        hb.transform.parent = _from;

        hb.transform.LookAt(_to);
		hb.transform.Rotate(bone.rotationOffset);

        hb.transform.Translate(Vector3.forward * lenth / 2);
        hb.layer = mask;
		
		

        CapsuleCollider capsuleCollider = hb.AddComponent<CapsuleCollider>();
        // capsuleCollider.isTrigger = true;
        capsuleCollider.direction = 2;
        capsuleCollider.height = lenth + bone.boxSize;
        capsuleCollider.radius = bone.boxSize;

        return hb;

    }
}

[System.Serializable]
public class BoneHitbox
{
    [HideInInspector]
    public string name = "new Hit Box";
    public Transform boneTransform;
	public Transform perentOverride;

    public DamageMultiplayer damageMultiplayer;
    public float boxSize = 0.1f;
	public Vector3 positionOffset;
	
	public Vector3 rotationOffset;
	

    public enum DamageMultiplayer
    {
        NONE,
        HIGH,
        LOW
    }



}
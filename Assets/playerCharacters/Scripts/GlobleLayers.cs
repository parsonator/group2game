﻿using UnityEngine;
using System.Collections;

public class GlobleLayers : MonoBehaviour
{
    public static LayerMask DetectAllMask;
    public static LayerMask ShootingMask;
    public static LayerMask DetectEnviroment;
    public static LayerMask DetectPlayer;
    public static LayerMask DetectEneimes;

    public static LayerMask NormalMove;

    [SerializeField]
    LayerMask detectAllMask;
    [SerializeField]
    LayerMask shootingMask;
    [SerializeField]
    LayerMask detectEnviroment;
    [SerializeField]
    LayerMask detectPlayer;
    [SerializeField]
    LayerMask detectEneimes;

    [SerializeField]
    LayerMask normalMove;

    void Awake()
    {
        DetectAllMask = detectAllMask;
        ShootingMask = shootingMask;
        DetectEnviroment = detectEnviroment;
        DetectPlayer = detectPlayer;
        DetectEneimes = detectEneimes;


        NormalMove = normalMove;

    }
}

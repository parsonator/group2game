﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeMeeleAttack : MonoBehaviour {

	public float MeeleAttackDamage = 10f;

	[HideInInspector]
	public bool canAttackLeft = false;
	[HideInInspector]
	public bool canAttackRight = false;

	public void SetCanAttackLeft(string value)
	{
		canAttackLeft = bool.Parse(value);
	}

	public void SetCanAttackRight(string value)
	{
		canAttackRight = bool.Parse(value);
	}

	private void OnTriggerEnter(Collider other)
	{
		if(canAttackLeft) 
		{
			if(other.GetComponent<PlayerControler>())
			{
				other.GetComponent<Attributes>().Damage(MeeleAttackDamage);
				canAttackLeft = false;
				if(other.GetComponent<PlayerControler>().connectionID == Client.ourClientID)
				{
					HudManager hud = Resources.FindObjectsOfTypeAll<HudManager>()[0];
					if(!hud.enabled) hud.enabled = true;
					hud.ShowDamageMarker(transform.root, other.transform);
				}
			}
		}

		if(canAttackRight)
		{
			if(other.GetComponent<PlayerControler>())
			{
				other.GetComponent<Attributes>().Damage(MeeleAttackDamage);
				canAttackRight = false;
				if(other.GetComponent<PlayerControler>().connectionID == Client.ourClientID)
				{
					HudManager hud = Resources.FindObjectsOfTypeAll<HudManager>()[0];
					if(!hud.enabled) hud.enabled = true;
					hud.ShowDamageMarker(transform.root, other.transform);
				}
			}
		}
	}
}

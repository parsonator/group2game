﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rootWave : MonoBehaviour {
	public float speed;
	public Material vineMat;
	public float maxLifeTime = 3;
	float LifeTime = 0;
	
	// Update is called once per frame
	void Update () {
		vineMat.SetFloat("_Speed",speed);
		transform.position += transform.forward * (speed * Time.deltaTime);
		LifeTime += Time.deltaTime;
		if(LifeTime>maxLifeTime)
		{
			LifeTime = 0;
			ObjectPoolManager.Main.destroyPooledObject(this.gameObject);
		}
	}
}

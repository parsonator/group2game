﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeCallDamage : MonoBehaviour {

	public TreeMeeleAttack leftArm;

	public TreeMeeleAttack rightArm;

	public void SetCanAttackLeft(string value)
	{
		transform.parent.GetComponentInChildren<TreeMeeleAttack>().SetCanAttackLeft(value);
	}

	public void SetCanAttackRight(string value)
	{
		rightArm.SetCanAttackRight(value);
	}
}

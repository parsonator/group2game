﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePerSecond : MonoBehaviour {
public float damage = 10;
public float perSecond = 1f;
float damageTick;

	// Update is called once per frame
	private void OnTriggerStay(Collider other) {

		if(damageTick > perSecond)
		{
			damageTick =0;
			other.gameObject.SendMessageUpwards("Damage", damage, SendMessageOptions.DontRequireReceiver);
		}
		// print(other.gameObject.name);
		damageTick += Time.deltaTime;
			
		
		
	}
}

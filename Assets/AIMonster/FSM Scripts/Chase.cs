﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : NPC_FSM_BASE
{
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        base.OnStateEnter(animator, stateInfo, layerIndex);
	}

	// OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        //We project the player position on the XZ plane - we use the enemy height (Y) for look at
        if(playerOpon == null) 
        {
            OnStateExit(animator, stateInfo, layerIndex);
            return;
        }
        
        Vector3 playerPos = new Vector3(playerOpon.transform.position.x, NPC.transform.position.y, playerOpon.transform.position.z);
        LookTo(playerPos);
        MoveTo(playerOpon.transform.position);
    }

	// OnStateExit is called before OnStateExit is called on any state inside this state machine
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class NPC_FSM_BASE : StateMachineBehaviour {

#region ProtectedVars
	protected NavMeshAgent agent;
    protected GameObject NPC;
    protected GameObject playerOpon;
#endregion

#region PublicVars
    public float speed = 4.0f;
    public float rotSpeed = 1.0f;
    public float accuracy = 3.0f;
#endregion

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        NPC = animator.gameObject;
        playerOpon = NPC.GetComponentInParent<MutantAI>().GetPlayer();
        agent = NPC.GetComponentInParent<MutantAI>().GetNav();
    }

    public void MoveTo(Vector3 _poz){        
        agent.destination = _poz;
    }
    public void LookTo(Vector3 _poz){
        agent.transform.LookAt(_poz);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class MeleeAttack : NPC_FSM_BASE
{
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
		base.OnStateEnter(animator, stateInfo, layerIndex);
		NPC.GetComponentInParent<MutantAI>().StartFiring();
	}

	// OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
		//We project the player position on the XZ plane - we use the enemy height (Y) for look at
		playerOpon = NPC.GetComponentInParent<MutantAI>().GetPlayer();
		
		if(playerOpon == null) 
		{	
			OnStateExit(animator, stateInfo, layerIndex);
			animator.SetBool("isAttackingMelee", false);
			return;
		}



		Vector3 playerPos = new Vector3(playerOpon.transform.position.x, agent.transform.position.y, playerOpon.transform.position.z);
		LookTo(playerPos);
	}

	// OnStateExit is called before OnStateExit is called on any state inside this state machine
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
		NPC.GetComponentInParent<MutantAI>().StopFiring();
	}

	
}


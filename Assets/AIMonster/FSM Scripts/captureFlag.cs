﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureFlag : MonoBehaviour
{
    #region PublicVars
    public GameObject playerWflag;
    public string playerTag;
    public GameObject flag;
    public bool isCaptured;
    #endregion

    void Start(){
        isCaptured = false;
    }

    void Update(){
        IsCaptured();
    }

    public void OnTriggerEnter(Collider other){
        //MutantAI mutantScript = mutant.GetComponent<mutantAI>();

        if (other.gameObject.tag == playerTag){
            isCaptured = true;
            playerWflag = other.gameObject;
        }
    }

    public void IsCaptured(){
        if (isCaptured)
            flag.SetActive(false);
        else
            flag.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MutantAI : MonoBehaviour {



    // #region PublicVars
    public GameObject bullet;
	public GameObject bDirection;
    public CaptureFlag flagScript;
    public float chaseDistance = 20;
    public float attackLongRangeDistance = 15;
    public float attackMeleeDistance = 10;
    public AudioClip shout;
    AudioSource source;
  
	// #endregion

    // #region PrivateVars
	private GameObject closestPlayer;
	private NavMeshAgent agent;
    private Animator anim;

    // #endregion

    void Awake ()
    {
        ObjectPoolManager.Main.createPool(bullet,10);        
        anim = GetComponentInChildren<Animator>();
        agent = GetComponent<NavMeshAgent>();
        source = GetComponent<AudioSource>();
        Server.Send(NetworkParser.ParseIn(NetworkMsgType.SPAWNAI, transform.position, transform.rotation, transform.name), Server.reliableChannel, ServerUtils.clients);
       if(shout == null)
       {Debug.Log("Shout sounds not assigned");}

    }

	void Update ()
    {
        float dist;
        // If player captures the flag it will chase and attack whoever captured.
        if (flagScript != null && flagScript.isCaptured == true){    
            anim.SetBool("isAttacking", false);
            anim.SetBool("isChasing", true);

            closestPlayer = flagScript.playerWflag;

            dist = Vector3.Distance(closestPlayer.transform.position, transform.position);

            if (closestPlayer)
                anim.SetBool("isChasing", true);
            
            if (dist <= 10 && closestPlayer != null)
            {
                anim.SetBool("isAttacking", true);
            
            }
        

            else if (dist > 10)
                anim.SetBool("isAttacking", false);
            
            return;
        }

        closestPlayer = FindClosestPlayer();

        if(closestPlayer)
        {
            Debug.Log("Should Play Sound");
            source.clip = shout;
            source.Play();
            dist = Vector3.Distance(closestPlayer.transform.position, transform.position);

            if (dist <= chaseDistance)
                anim.SetBool("isChasing", true);
            else if (dist > chaseDistance)
                anim.SetBool("isChasing", false); // goes back to patrolling

            if (dist <= attackLongRangeDistance)
                anim.SetBool("isAttacking", true);
                
            else if (dist > attackLongRangeDistance)
                anim.SetBool("isAttacking", false);

            if(dist <= attackMeleeDistance)
            {
                StopFiring();
                anim.SetBool("isAttacking", false);
                anim.SetBool("isAttackingMelee", true);
            }
            else if(dist > attackMeleeDistance)
                anim.SetBool("isAttackingMelee", false);

        }else{
            anim.SetBool("isAttacking", false);
            anim.SetBool("isChasing", false);
        }
       
        GetPlayer();
    }

    public GameObject FindClosestPlayer()
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("Player");
        if(gos.Length == 0) return null;
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            if(go == null) continue;
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }     
        return closest;
    }

    public float GetDistanceFromPlayer()
    {
        if(closestPlayer)
        {
            return Vector3.Distance(closestPlayer.transform.position, transform.position);
        }
            else return -1;
    }

    public void CallResetAnimState(Animator anim, string state)
	{
		StartCoroutine(ResetAnimState(anim, state));
	}

    IEnumerator ResetAnimState(Animator animator, string state)
    {
        yield return null;
		anim.SetBool(state, false);
    }

    public GameObject GetPlayer()
    {
        return closestPlayer;
    }

    public NavMeshAgent GetNav()
    {
        return agent;
    }

    public void StartFiring()
    {
        InvokeRepeating("AttackAgain", 10f, 10f);
    }

    public void StopFiring()
    {
        CancelInvoke("AttackAgain");
    }

    public void AttackAgain()
    {
        anim.SetBool("resetAttack", true);
        ResetAnimState(anim, "resetAttack");
    }

    public void Fire()
    {
        GameObject b = ObjectPoolManager.Main.instantiateObject(bullet, bDirection.transform.position, bDirection.transform.rotation);
        b.GetComponent<Rigidbody>().AddForce(bDirection.transform.forward * 1500);
        string msg = NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Fire");
        Server.Send(msg, Server.reliableChannel, ServerUtils.clients);       

    }
    
}

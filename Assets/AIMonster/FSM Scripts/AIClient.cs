﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIClient : MonoBehaviour {

	public GameObject bullet;
	public Transform b_direction;
	
	void Start(){
		ObjectPoolManager.Main.createPool(bullet,10);  
	}

	public void Fire(){
		GameObject b = ObjectPoolManager.Main.instantiateObject(bullet, b_direction.position, b_direction.rotation);
        b.GetComponent<Rigidbody>().AddForce(b_direction.forward * 1500);
	}
}

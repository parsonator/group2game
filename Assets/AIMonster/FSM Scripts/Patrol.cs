﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : NPC_FSM_BASE
{
    #region PrivateVars
    private GameObject[] waypoints;
    private int currentWayp;
    #endregion

    void Awake(){
        waypoints = GameObject.FindGameObjectsWithTag("waypoint");
    }

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
        if(animator == null) Debug.Log("animator is null");

        base.OnStateEnter(animator, stateInfo, layerIndex);
        currentWayp = 0;    
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){
	    if(waypoints.Length == 0)
            return;

        if(Vector3.Distance(waypoints[currentWayp].transform.position, NPC.transform.position) < accuracy){
            currentWayp++;
            if(currentWayp >= waypoints.Length)
                currentWayp = 0;
        }

        MoveTo(waypoints[currentWayp].transform.position);
	}

	//OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex){}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMinion : MonoBehaviour {

	//Minion
	public GameObject MActual = null;
	public GameObject MinionActual = null;
	public bool FSMActive = false;
	public float SpawnDistanceFromPlayer = 2.0f;
	public PlayerControler playerControler;
	AudioSource source;
	public AudioClip MinionReady;
	bool isReady, hasPlayed;
	//float Distance;
	//public float PreviewDistance = 10f; //  The max distance the minion can be spawned away from the player.



	public enum State {
		Default,
		Preview,
		Spawned,
	}
	public State state;

	void UpdateMinionState () {
		switch (state) {
			case State.Default:
				Idle ();
				break;
			case State.Preview:
				//PreviewSpawn();
				break;
		}
	}

	// Use this for initialization
	void Start () {
		state = State.Default;
		playerControler = GetComponent<PlayerControler> ();
		source = GetComponent<AudioSource> ();
		if (MinionReady == null) {
			Debug.Log ("Minion Ready sound not assigned");
		}
		hasPlayed = false;
	}

	void Update () {
		UpdateMinionState ();

		if (MActual == null) {
			FSMActive = false;
			isReady = true;
			MinionReadySound ();
			
		} 
	}

	void Idle () {
		if (PlayerInput.Spawn () && MActual == null) {
			SpawnMinion ();
		}
	}

	void SpawnMinion () {
		//Destroy (MPreview);
		if(playerControler.connectionID == Client.ourClientID){
			SpawnFinalMinion (false, PlayerAiming.CroshairHit + (Vector3.up * 0.5f), Quaternion.identity, playerControler.team);
			Client.Send (NetworkParser.ParseIn (NetworkMsgType.SPAWNMINION, MActual.transform.position, MActual.transform.rotation, MActual.transform.name), Client.reliableChannel);
		}
	}

	public void SpawnFinalMinion (bool client, Vector3 position, Quaternion quaternion, Team team) {
		MActual = Instantiate (MinionActual, transform.position + (transform.forward * SpawnDistanceFromPlayer), quaternion);
		isReady = false;
		hasPlayed = false;
		GetComponent<PlayerControler>().MyMinion = MActual;
		MActual.name += playerControler.connectionID;
		MActual.GetComponent<MinionController> ().player = transform;
		if (team == Team.BLUE) {
			MActual.layer = 13;
		} else {
			MActual.layer = 14;
		}
		if (client) {
			MActual.GetComponent<NetworkLocationMinion> ().Owner = false;
			MActual.GetComponent<NetworkLocationMinion> ().Local = true;

		}
		FSMActive = true;
	}

	void MinionReadySound()
	{
		if (isReady == true && hasPlayed == false) {
			source.clip = MinionReady;
			source.Play ();
			hasPlayed = true;
		}
	}
	public void SpawnOtherClientMinion(int cnnID, PlayerControler playerController, Team team)
	{
		MActual = Instantiate (MinionActual, transform.position + (transform.forward * SpawnDistanceFromPlayer), Quaternion.identity);
		MActual.GetComponent<MinionController> ().player = playerController.transform;
		MActual.name += cnnID;
		playerController.MyMinion = MActual;

		if (team == Team.BLUE) {
			MActual.layer = 13;
		} else {
			MActual.layer = 14;
		}

		MActual.GetComponent<NetworkLocationMinion> ().Owner = false;
		MActual.GetComponent<NetworkLocationMinion> ().Local = true;
		FSMActive = true;
	}

}
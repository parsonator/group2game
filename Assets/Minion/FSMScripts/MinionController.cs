﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MinionController : MonoBehaviour {
	public Transform goal;
	public Transform player;
	public PlayerMinion playerMinionControler;
	public Image healthBar;
	public Attributes attributes;
	UnityEngine.AI.NavMeshAgent agent;	
	public float mionionApplyDamage = 5f;
	bool Attacking;
	bool MinionActive;
	bool EnemyLocked = false;

	public float Range = 5.0f;
	public int healCounter = 5;
	public float healthAddition = 5f;
	bool startHeal = true;
	bool startDamage = true;

	public AudioClip spawn;
	AudioSource aSource;
	public enum State {
		Idle,
		Defending,
		Attacking,
	}

	public State state;

	void UpdateState () {
		if (player != null) {
			switch (state) {
				case State.Idle:
					Idle ();
					break;
				case State.Defending:
					Defend ();
					break;
				case State.Attacking:
					Attack ();
					break;
			}
		}
	}

	void Start () {
		state = State.Idle;
		attributes = GetComponent<Attributes>();
		playerMinionControler = player.GetComponent<PlayerMinion>();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		aSource = GetComponent<AudioSource>();
		aSource.clip = spawn;
		aSource.Play();
	}

	void Idle () {
		if (playerMinionControler.FSMActive == true) {
			state = State.Defending;
		}
	}

	void Defend () {

		agent.destination = player.position;

		if (PlayerInput.ChangeMode ()) {
			state = State.Attacking;
		}
		float Distance = Vector3.Distance (playerMinionControler.MActual.transform.position, player.transform.position);

		if (Distance <= Range) {
			if (startHeal) {
				StartCoroutine (Heal ());
			}

			startHeal = false;
		} else {
			StopAllCoroutines ();
			startHeal = true;
		}

	}

	void Attack () {

		goal = FindClosestEnemy ();

		if (goal.gameObject.GetComponent<Attributes>().Health == 0) {
			state = State.Defending;
		} else {
			agent.destination = goal.position;

			float Distance = Vector3.Distance (playerMinionControler.MActual.transform.position, goal.position);
			if (Distance <= Range) {
				if (startDamage) {
					StartCoroutine (Damage (goal));
				}

				startDamage = false;
			} else {
				StopAllCoroutines ();
				startDamage = true;
			}
		}

		

		if (PlayerInput.ChangeMode ()) {
			state = State.Defending;
		}

	}

	IEnumerator Heal () {
		yield return new WaitForSeconds (healCounter);
		player.GetComponent<Attributes>().AddHeath(healthAddition);
		StartCoroutine (Heal());
	}

	IEnumerator Damage(Transform goal)
	{
		yield return new WaitForSeconds (healCounter);
		Client.Send(NetworkParser.ParseIn(NetworkMsgType.SENDDAMAGE, new float[]{goal.gameObject.GetComponent<PlayerControler>().connectionID, mionionApplyDamage}), Client.reliableChannel);
		goal.gameObject.GetComponent<Attributes>().Health -= mionionApplyDamage;
		if(goal.gameObject.GetComponent<Attributes>().Health <= 0) 
		{
			if(Client.ourPlayerTeam == Team.BLUE)
				Client.Match.RedTeam.Score++;
			else if(Client.ourPlayerTeam == Team.RED)
				Client.Match.BlueTeam.Score++;

			//send to server                            
			Client.Send(NetworkParser.ParseIn(NetworkMsgType.GAMEMODE, new string[]{
									((int)Client.ourPlayerTeam).ToString(),
									"0",
									"1"
								}), Client.reliableChannel);
		}
		
		StartCoroutine (Damage(goal));
	}

	void Update () {
		UpdateState ();

		if (player != null) {
			if (playerMinionControler.FSMActive == false) {
				state = State.Idle;
			}
		}
		
		healthBar.fillAmount = attributes.GetHealthPercent();

		if(attributes.GetHealthPercent() <= 0 && player.GetComponent<PlayerControler>().connectionID == Client.ourClientID)
		{
			Client.Send(NetworkParser.ParseIn(NetworkMsgType.MINIONDIE, "1"), Client.reliableChannel);
			Destroy(this.gameObject);
		}
	}	

	public void SetupMinion (Transform player) {
		this.player = player;
	}

	public Transform FindClosestEnemy () //This finds the closest object tagged 'Enemy'
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag ("Player");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			if(go.layer != gameObject.layer&&go.transform != player)
			{
				Vector3 diff = go.transform.position - position;
				float curDistance = diff.sqrMagnitude;
				if (curDistance < distance) {
					closest = go;
					distance = curDistance;
				}
			}
		}
		if (closest != null)
			return closest.transform; //Set the closest Enemy as the Minion's target
		else
			return null;
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinionBehaviour : MonoBehaviour {

	public Transform goal; //Closest Enemy - updates automatically
	public Transform Player; //Player - Currently needs to be set manually in inspector
	bool moving = false;

	public bool attack;
	//public GameObject attacking;
	//public GameObject defending;

	
	void Start(){
		attack = true;
		//attacking = GameObject.Find ("Canvas/Attacking");//Find the attacking text
		//defending = GameObject.Find ("Canvas/Defending");//Find the defending text

	
	}

	// Update is called once per frame
	void Update () {


		FindClosestEnemy ();


		if(PlayerInput.ChangeMode()){// ChangeMode is currently set to - Z
		attack = !attack;
			moving = false;
		}

		if (attack == true && moving == false) { 
		UnityEngine.AI.NavMeshAgent agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ();
		agent.destination = goal.position; //Go towards nearest enemy

		//attacking.SetActive(true);
		//defending.SetActive(false);
		}	

		if (attack == false && moving == false) {
		UnityEngine.AI.NavMeshAgent agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ();
		agent.destination = Player.position; //Follow Player
		
		//defending.SetActive(true);
		//attacking.SetActive (false);
		}


		// if (PlayerAiming.ZoomedIn == true){ //If player is aiming down the sights
		if(PlayerInput.MoveMinion()){ // AND then presses X
			UnityEngine.AI.NavMeshAgent agent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ();
			agent.destination = PlayerAiming.CroshairHit; //Move Minion to wherever the player is looking
			moving = true;

		}
		// }
			


	}

	public GameObject FindClosestEnemy()//This finds the closest object tagged 'Enemy'
	{
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("Enemy");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos)
		{
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance)
			{
				closest = go;
				distance = curDistance;
			}
		}
		goal = closest.transform;//Set the closest Enemy as the Minion's target
		return closest;
	}




}



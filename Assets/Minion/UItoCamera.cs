﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UItoCamera : MonoBehaviour {

	public Camera CameraMain;

	void Start ()
	{
		CameraMain = GameObject.Find ("MainCamera").GetComponent<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {

		transform.LookAt (transform.position + CameraMain.transform.rotation * Vector3.forward, CameraMain.transform.rotation * Vector3.up);

		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlagControl : MonoBehaviour {

	GameObject player;
	public GameObject objectiveA;//Capture point 1

	Vector3 startingPos;
	Quaternion startingRot; 
	public Text  CaptureText;

	int timesCapturedTeam1;
	int timesCapturedTeam2;

	// Use this for initialization
	void Start () {

		player = GameObject.Find ("Ethan");
		startingPos = transform.position; //Get the starting location of this flag.
		startingRot = transform.rotation; // Get the starting rotation of this flag.
		CaptureText.text = "";
		timesCapturedTeam1 = 0;


	}
	
	// Update is called once per frame
	void Update () {

		if (Vector3.Distance (gameObject.transform.position, player.transform.position) <= 1) { //Check collision on Player
			CaptureText.text = "Has the Flag!";
			gameObject.transform.parent = player.transform;
		}


		if(Vector3.Distance (gameObject.transform.position, objectiveA.transform.position) <= 1) { //Check collision on Capture Point 1
			CaptureText.text = "Captured the Flag!";
			gameObject.transform.parent = null;
			transform.position = startingPos;
			transform.rotation = startingRot;
			timesCapturedTeam1 = timesCapturedTeam1 + 1;

		}


		//if (timesCapturedTeam1 == 3) {
			//Do Something
		//}
		
	}
}

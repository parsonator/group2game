﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

	public static Vector2 MouseAxis()
	{
		return new Vector2(Input.GetAxis("Mouse X"),Input.GetAxis("Mouse Y"));
	}
	public static Vector2 MoveAxis()
	{
		return new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
	}
	public static bool SprintButton()
	{
		return Input.GetKey(KeyCode.LeftShift);
	}
	public static bool shootingButton()
	{
		return Input.GetMouseButton(0);
	}
	public static bool ZoomButtonDown()
	{
		return Input.GetMouseButton(1);
	}
	public static bool ReloadButtonDown()
	{
		return Input.GetKeyDown("r");
	}
	public static bool ChangeMode()
	{
		return Input.GetKeyDown("z");
	}
	public static bool MoveMinion()
	{
		return Input.GetKeyDown("x");
	}
	public static bool Spawn()
	{
		return Input.GetKeyDown ("e");
	}
	public static bool Inventory()
	{
		return Input.GetKey ("tab");
	}
	public static bool AbilityButtonDown()
	{
		return Input.GetKeyDown("g");
	}

	public static bool Jump()
	{
		return Input.GetKeyDown("space");
	}

}

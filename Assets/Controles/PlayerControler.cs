﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class PlayerControler : Controler
{

    public delegate void Event();

    public Event onDisconect;
    public Event onDeath;

    public static PlayerControler main;

    public PlayerAbilityClass PlayerAbilityClass = PlayerAbilityClass.SOLDIER;
    public Attributes attributes;
    public PlayerCombat combat;
    public PlayerAbility playerAbility;
    public PlayerUI playerUI;
    public PlayerMinion playerMinion;
    public HitBoxManager hitBoxManager;
    public float speed = 5;
    public float rotationSpeed = 5;
    public bool blockControl = false;
    public bool networked = false;
    public Team team;
    public Animator animator;
AudioSource aSource;
    public AudioClip footsteps;
    public GameObject MainCharacterMeshRef;

    [HideInInspector]
    public int connectionID = 0;
    [HideInInspector]
    public GameObject MyMinion;

    public bool livePlayer
    {
        get
        {
            return (connectionID == Client.ourClientID);
        }
    }
    bool alive = true;
    public void Init(Team team, Gun_SO gun, PlayerAbilityClass playerAbilityClass, int connectionID)
    {
        if (connectionID != -1)
        {
            this.connectionID = connectionID;
        }
        this.team = team;

        if (livePlayer)
        {
            hitBoxManager.setLayer(8);
            main = this;
        }
        else
        {
            switch (team)
            {
                case Team.BLUE:
                    hitBoxManager.setLayer(13);
                    break;
                case Team.RED:
                    hitBoxManager.setLayer(14);
                    break;
            }

        }

        combat.GunPrefs = gun;

        Ability selectedAbility = null;
        switch (playerAbilityClass)
        {

            case PlayerAbilityClass.SOLDIER:
                // selectedAbility = playerAbility.allAbility.Find(x => (x as GrenadeAbility) != null);
                GrenadeAbility selectedSOLDIER = ScriptableObject.CreateInstance(typeof(GrenadeAbility)) as GrenadeAbility;
                GrenadeAbility baceAbilitySOLDIER = playerAbility.allAbility.Find(x => (x as GrenadeAbility) != null) as GrenadeAbility;
                selectedSOLDIER.abilityCooldownTime = baceAbilitySOLDIER.abilityCooldownTime;
                selectedSOLDIER.throwPower = baceAbilitySOLDIER.throwPower;
                selectedSOLDIER.Grenade = baceAbilitySOLDIER.Grenade;
                selectedSOLDIER.damage = baceAbilitySOLDIER.damage;
                selectedAbility = selectedSOLDIER;
                break;

            case PlayerAbilityClass.MEDIC:
                // selectedAbility = playerAbility.allAbility.Find(x => (x as HealthAbility) != null);
                HealthAbility selectedMEDIC = ScriptableObject.CreateInstance(typeof(HealthAbility)) as HealthAbility;
                HealthAbility baceAbilityMEDIC = playerAbility.allAbility.Find(x => (x as HealthAbility) != null) as HealthAbility;
                selectedMEDIC.abilityCooldownTime = baceAbilityMEDIC.abilityCooldownTime;
                selectedMEDIC.throwPower = baceAbilityMEDIC.throwPower;
                selectedMEDIC.HealthPack = baceAbilityMEDIC.HealthPack;
                selectedMEDIC.aoeRad = baceAbilityMEDIC.aoeRad;
                selectedAbility = selectedMEDIC;
                break;

            case PlayerAbilityClass.PALADIN:
                ShieldAbility selectedPALADIN = ScriptableObject.CreateInstance(typeof(ShieldAbility)) as ShieldAbility;
                ShieldAbility baceAbilityPALADIN = playerAbility.allAbility.Find(x => (x as ShieldAbility) != null) as ShieldAbility;
                selectedPALADIN.abilityCooldownTime = baceAbilityPALADIN.abilityCooldownTime;
                selectedPALADIN.throwPower = baceAbilityPALADIN.throwPower;
                selectedPALADIN.shield = baceAbilityPALADIN.shield;
                selectedAbility = selectedPALADIN;
            break;
        }

        playerAbility.ability = selectedAbility;
        playerAbility.ability.playerControler = this;
        playerAbility.ability.InitAbility();
    }

    void Awake()
    {
        attributes = GetComponent<Attributes>();
        combat = GetComponent<PlayerCombat>();
        playerAbility = GetComponent<PlayerAbility>();
        playerMinion = GetComponent<PlayerMinion>();
        playerUI = GetComponent<PlayerUI>();
        hitBoxManager = GetComponentInChildren<HitBoxManager>();
        aSource = GetComponent<AudioSource>();
    }

    void applyMovement()
    {
        if (blockControl == false)
        {
            //We only want to update if we are the owner of the character or if the game is not networked
            if (!networked || connectionID == Client.ourClientID)
            {
                Vector3 MoveDirection = Vector3.zero;
                float axisY = PlayerInput.MoveAxis().y;
                MoveDirection += transform.forward * axisY;
                MoveDirection += transform.right * PlayerInput.MoveAxis().x;

                if(MoveDirection.magnitude > 0.5f)
                {
                    if(!aSource.isPlaying)
                    aSource.PlayOneShot(footsteps);
                }

                if (PlayerInput.SprintButton() && axisY >= 0)
                {
                    animator.SetBool("Sprint", true);
                    MoveDirection *= 2;

                    
                }
                else animator.SetBool("Sprint", false);

                animator.SetFloat("XVol", PlayerInput.MoveAxis().x);
                animator.SetFloat("YVol", axisY);


                float s = speed * MoveDirection.magnitude;

                MoveCharacter(MoveDirection.normalized * s * Time.deltaTime);
                // gameObject.transform.Rotate(new Vector3 (0,PlayerInput.MouseAxis().x,0)*rotationSpeed);
            }
        }

        if (blockControl == true)
        {
            gameObject.GetComponent<PlayerCombat>().enabled = false;
        }

        applyGravity();

    }

    public void Revive(int cnnID = 0, bool replicate = true)
    {
        if(GetComponent<PlayerControler>().connectionID != Client.ourClientID)

        if (replicate && networked)
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Revive"), Client.reliableChannel);

        if (connectionID == Client.ourClientID)
        {
            this.enabled = true;
            combat.enabled = true;
            attributes.enabled = true;
            playerUI.enabled = true;            
            Client.UpdateSpawnCount(Client.ourPlayerTeam);
        }
        else
        {
            gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(true);
            gameObject.FindInChildren("NameTagHealthBar").GetComponent<UnityEngine.UI.Image>().fillAmount = 1;
        }

        attributes.Revive();
        MainCharacterMeshRef.SetActive(true);

        if (Client.players[cnnID].team == Team.BLUE)
        {
            gameObject.transform.position = Client.spawnPointsBlue[Client.spawnPointIndexBlue].transform.position;
        }
        else if (Client.players[cnnID].team == Team.RED)
        {
            gameObject.transform.position = Client.spawnPointsRed[Client.spawnPointIndexRed].transform.position;
        }

        if(connectionID == Client.ourClientID)
            Debug.Log(gameObject.transform.Find("MainCharacterMesh").gameObject);
        alive = true;
    }

    public int secondsToRevive = 8;

    IEnumerator RespawnCountDown()
    {
        GameObject.Find("Crosshair").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("RespawnCountDownTxt").GetComponent<CanvasGroup>().alpha = 1;
        while (secondsToRevive > 0){
            yield return new WaitForSeconds(1);
            secondsToRevive--;
            
            GameObject.Find("RespawnCountDownTxt").GetComponent<UnityEngine.UI.Text>().text = secondsToRevive.ToString();
        }
        secondsToRevive = 8;
        GameObject.Find("Crosshair").GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("RespawnCountDownTxt").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("RespawnCountDownTxt").GetComponent<UnityEngine.UI.Text>().text = secondsToRevive.ToString();
        Revive(Client.ourClientID);
    }

    public void Die(bool replicate = true)
    {
        if (onDeath != null)
            onDeath();

        if (replicate && networked)
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Die"), Client.reliableChannel);
        
        if(connectionID == Client.ourClientID || (connectionID != Client.ourClientID && replicate))
            MainCharacterMeshRef.SetActive(false);


        if (connectionID == Client.ourClientID)
        {
            this.enabled = false;
            combat.enabled = false;
            attributes.enabled = false;
            playerUI.enabled = false;
            Destroy(MyMinion);
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.MINIONDIE, "1"), Client.reliableChannel);
        }

        if(connectionID != Client.ourClientID && replicate)
            gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(false);
        

        if(connectionID == Client.ourClientID)
            StartCoroutine("RespawnCountDown");
    }

    void Update()
    {
        applyMovement();

        if (attributes.CheckDead() && alive)
        {
            alive = false;
            Die();
        }

        if(transform.position.y < -30)
            Die();

    }

}

public enum PlayerAbilityClass
{
    SOLDIER = 0,
    MEDIC,
    PALADIN

}
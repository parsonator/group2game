﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WiningScreen : CanvasScreen {

	public Text redScore;
	public Text blueScore;
	public Image teamImageText;
	public Image WinImage;
	
	public Sprite redTeam;
	public Sprite blueTeam;
	AudioSource source;
	public AudioClip WinCheer;

	public void ShowEndScreen(Team winingTeam)
	{
		CanvasManager.main.setActiveScreen(CanvasManager.GetScreen("WiningScreen"));
		redScore.text = Client.Match.RedTeam.Score.ToString();
		blueScore.text = Client.Match.BlueTeam.Score.ToString();

		
		switch(winingTeam)
		{
			case Team.BLUE:
				if(PlayerControler.main.team == Team.BLUE){
					WinImage.enabled = true;
							source.clip = WinCheer;
							source.Play();
				}else{
					WinImage.enabled = false;
				}
				teamImageText.sprite = blueTeam;
			break;

			case Team.RED:
				if(PlayerControler.main.team == Team.RED){
					WinImage.enabled = true;
				source.clip = WinCheer;
				source.Play();
				}else{
					WinImage.enabled = false;
				}
				teamImageText.sprite = redTeam;

			break;
		}
	}

	// Use this for initialization
	void Awake() {
		source = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

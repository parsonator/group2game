﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : CanvasScreen
{

    // public static HudManager main;
    public StatusBarManager statusBarManager;
    public hudObject RespawnCountDownTxt, Crosshair, StatusBar, DamageMarker ,winScreen;
    public RectTransform crosshair;
    public GameModeBar gameModeBar;

    public Text RespawnTxt;


    [SerializeField] Transform iconPivot;
    Camera mainCamera;


    // Use this for initialization
    void Awake()
    {
        statusBarManager = gameObject.GetComponentInChildren<StatusBarManager>();
    }
    private void Start()
    {
        DamageMarker.canvasGroup.alpha = 0;
    }

	void Update()
	{
		gameModeBar.redScore.text = Client.Match.RedTeam.Score.ToString();
		gameModeBar.blueScore.text = Client.Match.BlueTeam.Score.ToString();	
	}

    public void pointToWorld(Transform pivot, Vector3 attackerPos, Transform player)
    {
        if(!player || !pivot) return;
        //We need the direction from the attacker to the player
        Vector3 playerAttackerDir = (attackerPos - player.position).normalized;
        //But we only need a straight line so we don't consider the height and exclude the Y axis
        playerAttackerDir = new Vector3(playerAttackerDir.x, 0, playerAttackerDir.z);
        //We then need the forward vector of the camera but only on the Y axis

        Vector3 camForward = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0) * Vector3.forward;
        //We need to get an angle based on the two unit vectors calculated and we know that
        //arctan2(b_y, b_x) - arctan2(a_y, a_x) gives us exactly the angle
        float angleRadCam = (Mathf.Atan2(playerAttackerDir.z, playerAttackerDir.x) - Mathf.Atan2(camForward.z, camForward.x));
        //We need the angle in degrees
        float angleDegCam = (180 / Mathf.PI) * angleRadCam;
        //The angle returned will be between 0-270 then we get a negative angle
        //If it's negative just add 360
        angleDegCam = angleDegCam < 0 ? (angleDegCam + 360) : angleDegCam;
        //Finally we can apply the rotation
        pivot.rotation = Quaternion.Euler(0, 0, angleDegCam);   
    }

    public void setCrosshairDilution(float spread)
    {
        crosshair.sizeDelta = Vector2.Lerp(Vector2.one*64, Vector2.one*200,spread);
    }

    public void ShowDamageMarker(Transform attackerPos, Transform player)
    {
        if (iconPivot != null)
        {
            StartCoroutine(BlendAlpha(DamageMarker, 1));
            //if the icon pivot was not visible make it visible and get a reference to the camera
            if (iconPivot.gameObject.activeSelf == false)
            {
                iconPivot.gameObject.SetActive(true);
                mainCamera = Camera.main;
            }

            pointToWorld(iconPivot.transform, attackerPos.position, player);

            if (!damageMarkerCountDownRunning)
            {
                StartCoroutine(damageMarkerCountDown(2, attackerPos, player));
            }
            else
            {
                timer = 0;
            }
        }

    }

    IEnumerator BlendAlpha(hudObject obj, float target)
    {
        float blendTimer = 0;
        float blendWait = 1;
        while(true)
        {
            if(target == 1) obj.canvasGroup.alpha += 0.05f;
            else if(target == 0) obj.canvasGroup.alpha -= 0.05f;
            blendTimer += Time.deltaTime;
            if(blendTimer > blendWait)
                break;
            yield return new WaitForFixedUpdate();   
            
        }
    }

    bool damageMarkerCountDownRunning;
    float timer;
    
    IEnumerator damageMarkerCountDown(float wait, Transform attackerPos, Transform player)
    {
        damageMarkerCountDownRunning = true;
        while (true)
        {
            pointToWorld(iconPivot.transform, attackerPos.position, player);
            timer += Time.deltaTime;
            if (timer > wait)
                break;
            yield return new WaitForFixedUpdate();
        }
        timer = 0;
        StartCoroutine(BlendAlpha(DamageMarker, 0));
        damageMarkerCountDownRunning = false;
    }
}

[System.Serializable]
public class hudObject
{
    public CanvasGroup canvasGroup;

    public void setElementActive(bool set)
    {
        canvasGroup.alpha = set ? 1 : 0;
        canvasGroup.interactable = set;
        canvasGroup.blocksRaycasts = set;
    }

}

[System.Serializable]
public class GameModeBar
{
    public Text redScore;
    public Text blueScore;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageMarker : MonoBehaviour {
    
    public Transform iconPivot;
    private Camera mainCamera;

    void Update()
    {
        if(iconPivot.gameObject.activeSelf == true)
        {
            StartCoroutine(CountDown());
        }
    }

    IEnumerator CountDown()
    {
        yield return new WaitForSeconds(2);
        iconPivot.gameObject.SetActive(false);
    }

    public void ShowDamageMarker(Vector3 attackerPos)
    {
        //if the icon pivot was not visible make it visible and get a reference to the camera
        if(iconPivot.gameObject.activeSelf == false)
        {
            iconPivot.gameObject.SetActive(true);
            mainCamera = Camera.main;
        }

        //if we are in here we want to stop all the coroutines started 
        //that would make the marker disappear
        StopAllCoroutines();

        /*
        **LET'S MATH AROUND A BIT
        */
        //We need the direction from the attacker to the player
        Vector3 playerAttackerDir = (attackerPos - transform.position).normalized;
        //But we only need a straight line so we don't consider the height and exclude the Y axis
        playerAttackerDir = new Vector3(playerAttackerDir.x, 0, playerAttackerDir.z);
        //We then need the forward vector of the camera but only on the Y axis
        Vector3 camForward = Quaternion.Euler(0, mainCamera.transform.eulerAngles.y, 0) * Vector3.forward; 
        //We need to get an angle based on the two unit vectors calculated and we know that
        //arctan2(b_y, b_x) - arctan2(a_y, a_x) gives us exactly the angle
        float angleRadCam = (Mathf.Atan2(playerAttackerDir.z, playerAttackerDir.x) - Mathf.Atan2(camForward.z, camForward.x));
        //We need the angle in degrees
        float angleDegCam = (180 / Mathf.PI) * angleRadCam;
        //The angle returned will be between 0-270 then we get a negative angle
        //If it's negative just add 360
        angleDegCam = angleDegCam < 0 ? (angleDegCam + 360) : angleDegCam;
        //Finally we can apply the rotation
        iconPivot.rotation = Quaternion.Euler(0, 0, angleDegCam);
    }
}



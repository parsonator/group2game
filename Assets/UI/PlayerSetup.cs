﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSetup : CanvasScreen
{

    [Header("PlayerSetup")]
    [SerializeField]
    InputField PlayerNameInput;
    [Header("Class")]

    [SerializeField]
    Dropdown ClassSelect;
    [Header("Gun")]
    [SerializeField]
    Dropdown GunSelect;
    [SerializeField] List<Gun_SO> GunsList;
    [Space()]
    [SerializeField]
    Text GunNameText;
    [SerializeField] Text GunDPSText;
    [SerializeField] Text GunAmmoText;
    [SerializeField] Text GunAccuracyText;
    [SerializeField] Text GunStabilityText;

    private PlayerAbilityClass selectedClass;
    private Gun_SO selectedGun;

    private void Start()
    {
        List<string> gunNames = new List<string>();
        GunSelect.ClearOptions();
        foreach (var gun in GunsList)
        {
            gunNames.Add(gun.name);
        }
        GunSelect.AddOptions(gunNames);

        List<string> ClassNames = new List<string>();
        ClassSelect.ClearOptions();
        foreach (var c in System.Enum.GetValues(typeof(PlayerAbilityClass)))
        {
            ClassNames.Add(c.ToString());
        }
        ClassSelect.AddOptions(ClassNames);

        UpdateGunInfo();

    }

    public void UpdateGunInfo()
    {
        Gun_SO g = GunsList.Find(x => x.name == GunSelect.captionText.text);
        GunNameText.text = g.name;
        GunDPSText.text = "DPS: " + g.damage.ToString();
        GunAmmoText.text = "AMMO: " + g.maxAmmo.ToString();
        GunAccuracyText.text = "Accuracy: " + g.accuracy.ToString();
        GunStabilityText.text = "Stability: " + g.stability.ToString();
        selectedGun = g;
    }

    public PlayerProfile GetPlayerProfile()
    {
        PlayerProfile temp = new PlayerProfile();
        temp.playerName = PlayerNameInput.text;

        temp.PlayerAbilityClass = (PlayerAbilityClass)System.Enum.Parse(typeof(PlayerAbilityClass),
                                        ClassSelect.captionText.text);

        temp.gunType = selectedGun.name;

        return temp;
    }


}

[System.Serializable]
public class PlayerProfile
{
    public string playerName;
    public PlayerAbilityClass PlayerAbilityClass;
    public string gunType;
	public PlayerProfile()
	{}

	public PlayerProfile(string name, PlayerAbilityClass playerAbilityClass, string gunType)
	{
		this.playerName = name;
		this.PlayerAbilityClass = playerAbilityClass;
		this.gunType = gunType;
	}

}

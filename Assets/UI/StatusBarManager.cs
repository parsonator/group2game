﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusBarManager : MonoBehaviour {

	[SerializeField] Image hpBar,staminaBar,abilityIcon,playerIcon;
	[SerializeField] Text ammoText;
	[SerializeField] Sprite redPlayerImage,bluePlayerImage;

	
[Range(0,1)]

	public float hpBarValue, staminaBarValue,abilityCoolDown = 1;
	public int currentAmmo, maxAmmo;

	void Start () {
	}

	public void setUpImage(Team team){
		switch (team)
		{
			case Team.RED:
			playerIcon.sprite = redPlayerImage;
			break;

			case Team.BLUE:
			playerIcon.sprite = bluePlayerImage;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		hpBar.fillAmount = hpBarValue;
		
		staminaBar.fillAmount = staminaBarValue;
		abilityIcon.fillAmount = abilityCoolDown;

		ammoText.text = string.Format("{0}/{1}",currentAmmo,maxAmmo);

	}
	
	public enum Team{
		RED,
		BLUE
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : CanvasScreen {

	public PlayerSetup playerSetup;

	void Start () 
	{
		playerSetup = GetComponentInChildren<PlayerSetup>();
		InputField[] fields = GetComponentsInChildren<InputField>();		
		foreach(InputField field in fields){
			if(field.transform.gameObject.name == "NameInput")
				field.ActivateInputField();

			if(field.transform.gameObject.name == "IPAddressInput"){
				Client client = GameObject.Find("Client").GetComponent<Client>();
				field.text = client.IPaddress;
			}
		}
	}

	public void CloseModalWindow(GameObject caller)
	{
		caller.transform.parent.gameObject.SetActive(false);
	}

	public void ValidateData(GameObject modalWindow)
	{
		Client client = GameObject.Find("Client").GetComponent<Client>();
		bool result = true;
		string IPAddress = "";
		int port = 8000;
		string playerName = "";

		//Get a random text to assign the variable to a value
		Text errorMessageTxt = GetComponentInChildren<Text>();

		//Get the error message text to assign the error message later
		Text[] texts = modalWindow.GetComponentsInChildren<Text>();
		foreach(Text text in texts){
			if(text.gameObject.name == "ErrorMessageTxt")
				errorMessageTxt = text;
		}

		//Get all the fields and check for emptiness or wrong values
		InputField[] fields = GetComponentsInChildren<InputField>();
		if(fields.Length == 0)
			return;

		foreach(InputField field in fields){
			if(field.text.Trim() == ""){
				result = false;
				errorMessageTxt.GetComponentInChildren<Text>().text = "One or more fields have been left empty. Please make sure to fill in all the fields.";
			}

			if(field.gameObject.name == "NameInput"){
				playerName = field.text;
			}
			
			if(field.gameObject.name == "IPAddressInput"){
				if(!client.useDefaultServerAddress){
					string[] data = field.text.Split('.');
					if(data.Length < 4){
						result = false;
						errorMessageTxt.text = "Double check that the IP address provided is written correctly. An IP address should be written in the form of XXX.XX.XX.XX, in which the Xs are replaced by numbers.";
					}

					foreach(string s in data){
						if(s.Trim() == ""){
							result = false;
							errorMessageTxt.text = "Double check that the IP address provided is written correctly. An IP address should be written in the form of XXX.XX.XX.XX, in which the Xs are replaced by numbers.";
						}
					}
				}
				IPAddress = field.text;
			}

			if(field.gameObject.name == "PortInputField")
			{
				port = int.Parse(field.text);
				if(port < 8000 || port > 8009)
				{
					result = false;
					errorMessageTxt.text = "The port number must be in range of 8000 to 8009.";
				}
			}			
		}
		//If the result is true then connect otherwise show error message
		if(result){
			GameObject.Find("Client").GetComponent<Client>().Connect(IPAddress.Trim(), port, playerSetup.GetPlayerProfile());
		}else{
			modalWindow.SetActive(true);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (CanvasGroup))]
public class CanvasScreen : MonoBehaviour {

	CanvasGroup screen;

	private void Awake () {
		screen = GetComponent<CanvasGroup> ();
	}
	public void setScreen (bool set) {
		screen = GetComponent<CanvasGroup> ();

		if (set) {
			screen.alpha = 1;
			screen.interactable = true;
			screen.blocksRaycasts = true;

		} else {
			screen.alpha = 0;
			screen.interactable = false;
			screen.blocksRaycasts = false;
		}
	}

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CanvasManager : MonoBehaviour {

	public static CanvasManager main;
	public List<CanvasScreen> Screens;

	public int startScreen;

	// Use this for initialization
	private void Awake() {
		main = this;
	}
	void Start () {
		setActiveScreen(Screens[startScreen]);
	}

	public static CanvasScreen GetScreen(string _screenType)
	{
		return main.Screens.Find(x => (x.GetType().ToString() == _screenType));
	}
	
	public CanvasScreen setActiveScreen(CanvasScreen _screen)
	{
		CanvasScreen outScreen = null;
		foreach (CanvasScreen item in Screens)
		{
			if(item == _screen)
			{
				item.setScreen(true);
				outScreen = item;
			}
			else {
				item.setScreen(false);
			}
		}
		return outScreen;
	}
}
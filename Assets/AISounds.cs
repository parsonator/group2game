﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISounds : MonoBehaviour {

AudioSource source;
public AudioClip JumpAttack;
public AudioClip MeleeAttack;
public AudioClip footstep;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();

		if(JumpAttack == null)
		{
			Debug.Log("Jump Attack sound not assigned");
		}
		if(MeleeAttack == null)
		{
			Debug.Log("Melee Attack sound not assigned");
		}
		if(footstep == null)
		{
			Debug.Log("Footstep sound not assigned");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void JumpSound() //The AI Jump attack.
	{
	source.clip = JumpAttack;
	source.Play();
	}

	public void MeleeSound()//Ai Melee attack
	{
	source.clip = MeleeAttack;
	source.Play();

	}

	public void AIStomp()
	{
		source.clip = footstep;
		source.Play ();
	}
}

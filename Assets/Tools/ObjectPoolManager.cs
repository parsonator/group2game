﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour {
	public static ObjectPoolManager Main ;
	static Vector3 poolLocation = new Vector3(0,-100,0);
	public List<PoolObject> Pool;


	/// <summary>
	/// Creates a pool of objects for the object spawnfrom;
	/// </summary>
	/// <param name="_toPool"> the GameObject you want to pool</param>
	/// <param name="_amount">How many you want</param>
	public void createPool(GameObject _toPool, int _amount)
	{
		if(getPoolGameObject(_toPool) == null)
		{
			//print(_toPool.name + "pool created");
			PoolObject newPool = new PoolObject(_toPool,_amount);
			Pool.Add(newPool);
		}
		//print(_toPool.name + "pool exsistes");

	}
	/// <summary>
	/// like the defalt instantieate function, spawns an object form the pool;
	/// </summary>
	/// <param name="_gameObject"> the object to spawn</param>
	/// <param name="_poz"></param>
	/// <param name="_rotation"></param>
	/// <returns></returns>
	public GameObject instantiateObject(GameObject _gameObject, Vector3 _poz, Quaternion _rotation)
	{
		GameObject g = getPoolGameObject(_gameObject);

		g.transform.position = _poz;
		g.transform.rotation = _rotation;
		g.SetActive(true);
		return g;
	}
	/// <summary>
	/// deleates the object from the pool and makes it avalible againe
	/// </summary>
	/// <param name="_gameObject">in object to remove</param>
	public void destroyPooledObject(GameObject _gameObject)
	{
		GameObject g = getPoolGameObjectInst(_gameObject);
		if(g != null){
			g.SetActive(false);		
			g.transform.position = poolLocation;
		}else Debug.LogError(_gameObject.name+ "is not in pool");
	}

	GameObject getPoolGameObject(GameObject _gameObject)
	{
		foreach (PoolObject poolObject in Pool)
		{
			if(poolObject.Template.name == _gameObject.name)
			{
				return poolObject.getnewObject();
			}
		}
		//print("ERROR_ PoolNotFound " + _gameObject.name);
		return null;
	}
	GameObject getPoolGameObjectInst(GameObject _gameObject)
	{
		PoolObject p = Pool.Find(x=> x.Template.name == _gameObject.name);
		if(p != null)
		return p.getObject(_gameObject);
		else return null;

	}
	
	void Awake()
	{
		Main = this;
	}
	// Update is called once per frame
	void Update () {
		
	}

	[System.Serializable]
public class PoolObject
{
	public string poolName;
	public GameObject Template;
	int amount;
	
	Queue<PoolInt> Pool;

	public PoolObject(GameObject _toPool,int _amount)
	{
		Template = _toPool;
		poolName = _toPool.name +"_Pool";
		amount = _amount;
		Pool = new Queue<PoolInt>();
		for (int i = 0; i < _amount; i++)
		{
			GameObject g = Instantiate(_toPool,poolLocation,Quaternion.identity);
			g.name = _toPool.name;
			g.SetActive(false);
			Pool.Enqueue(new PoolInt(g));
		}
	}
	
	public GameObject getnewObject()
	{
		foreach (PoolInt item in Pool)
		{
			if(item.Spawned == false) 
			{
				item.Spawned = true;
				return item.pooledObject;
			}
		}

			PoolInt p = Pool.Dequeue();
			Pool.Enqueue(p);
			return p.pooledObject;
	}

	public GameObject getObject(GameObject _gameObject)
	{
		foreach (PoolInt item in Pool)
		{
			if(item.pooledObject == _gameObject) 
			{	
				item.Spawned = false;
				return item.pooledObject;
			}
		}
		PoolInt p = Pool.Dequeue();
		Pool.Enqueue(p);
		return p.pooledObject;

	}

	public class PoolInt
	{
		public bool Spawned;
		public GameObject pooledObject; 

		public PoolInt(GameObject _gameObject)
		{
			pooledObject = _gameObject;
			Spawned = false;
		}
	}
}
}



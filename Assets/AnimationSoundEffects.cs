﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSoundEffects : MonoBehaviour {

	AudioSource aSource;
    public AudioClip footsteps;

	// Use this for initialization
	void Start () {
		aSource = GetComponentInParent<AudioSource>();
	}
	
	// Update is called once per frame
	public void Footstep()
	{
		aSource.PlayOneShot(footsteps);
	}
}

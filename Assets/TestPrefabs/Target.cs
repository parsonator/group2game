﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    public Attributes attributes;
    void Start()
    {
        attributes = GetComponent<Attributes>();
    }
    float t;

    // Update is called once per frame
    void Update()
    {	
        if (attributes.CheckDead())
        {
			transform.rotation = Quaternion.Euler(0,0,-90);
			
            t += Time.deltaTime;
            if (t > .5)
            {
				transform.rotation = Quaternion.Euler(0,0,0);
				attributes.Revive();
                t = 0;
            }
        }
    }

}

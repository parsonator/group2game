﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Torret : MonoBehaviour {

	private GameObject[] Players;
	public float Distance = 4;

	void Awake()
	{
		Players = GameObject.FindGameObjectsWithTag("Player");
	}
	
	void Update () 
	{
		foreach(GameObject obj  in Players)
		{
			if(Vector3.Distance(transform.position, obj.transform.position) < Distance){
				Vector3 end = new Vector3(obj.transform.position.x, transform.position.y, obj.transform.position.z);
				DrawLine(transform.position, end, Color.red, 0.1f, 0.01f);
				PlayerUI playerUI = obj.GetComponent<PlayerUI>();
				
				obj.SendMessageUpwards("Damage", 1, SendMessageOptions.DontRequireReceiver);
                obj.SendMessageUpwards("hitMark", transform.position, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	
	void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f, float width = 0.01f)
	{
		GameObject myLine = new GameObject();
		myLine.transform.position = start;
		myLine.AddComponent<LineRenderer>();
		LineRenderer lr = myLine.GetComponent<LineRenderer>();
		lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
		lr.startColor = color;
		lr.endColor = color;
		lr.startWidth = width;
		lr.endWidth = width;
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
		GameObject.Destroy(myLine, duration);
	}
}


 

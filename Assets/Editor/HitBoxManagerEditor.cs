﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(HitBoxManager))]

public class HitBoxManagerEditor : Editor {

 public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        HitBoxManager BonelussPizza = (HitBoxManager)target;
        if(GUILayout.Button("Make Bones"))
        {
            BonelussPizza.makeBones();
        }
        if(GUILayout.Button("Delete Bones"))
        {
            BonelussPizza.delBones();
        }
		
    }
}
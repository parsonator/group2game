﻿using UnityEngine;
using UnityEditor;
using System.Collections;
// CopyComponents - by Michael L. Croswell for Colorado Game Coders, LLC
// March 2010

public class ReplaceGameObjects : ScriptableWizard
{
    public bool copyValues = true;
    public GameObject useGameObject;
    public GameObject[] Replace;

    [MenuItem("NiceTools/Replace GameObjects")]


    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Replace GameObjects", typeof(ReplaceGameObjects), "Replace");
    }

    void OnWizardCreate()
    {
        for (int i = 0; i < Replace.Length; i++)
        {

            GameObject Replaces;
            Replaces = Replace[i]; ;

            GameObject newObject;
            newObject = (GameObject)PrefabUtility.InstantiatePrefab(useGameObject);
            newObject.transform.position = Replaces.transform.position;
            newObject.transform.parent = Replaces.transform.parent;
            newObject.transform.rotation = Replaces.transform.rotation;

            Destroy(Replace[i]);

        }

    }
}

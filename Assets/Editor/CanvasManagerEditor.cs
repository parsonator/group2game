﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(CanvasManager))]
public class CanvasManagerEditor : Editor {
public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        CanvasManager cm = (CanvasManager)target;
        if(GUILayout.Button("Set screen"))
        {
			cm.setActiveScreen(cm.Screens[cm.startScreen]);

        }

		
    }
}

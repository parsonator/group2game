﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IK{
	public enum Axis{
		UP,
		RIGHT,
		FORWARD,
	    NUP,
		NRIGHT,
		NFORWARD
	}
}
[System.Serializable]
public struct BoneAndAxis{
	public Transform Bone;
	public IK.Axis ForwardAxis;
	public IK.Axis RightAxis;
	public IK.Axis UpAxis;
	public float Weight;
	public float AngleLimit;
	public bool ShowDebugVector;
	[HideInInspector]
	public Quaternion initialAngle;
}

public class AimIK : MonoBehaviour {

	public BoneAndAxis[] boneAndAxis;
	private Vector3 target;
	private Animator animator;

	void Start(){
		for(int i = 0; i < boneAndAxis.Length - 1; i++){
			boneAndAxis[i].initialAngle = boneAndAxis[i].Bone.localRotation;
		}
		animator = GetComponent<Animator>();
	}

	void LateUpdate () {
		if(animator.GetFloat("XVol") > 0 || animator.GetFloat("YVol") > 0) return;

		//Get Target Location
		target = transform.position + new Vector3(0, 1.2f, 0) + Camera.main.transform.forward;

		Vector3 forwardVector;
		Vector3 rightVector;
		Vector3 upVector;

		foreach(BoneAndAxis bone in boneAndAxis){
			if(bone.Weight == 0) continue; //if weight is 0 just go to next bone

			GetAxisVectors(out forwardVector, out rightVector, out upVector, bone);
			Vector3 boneToTarget = (target - bone.Bone.position).normalized;
			Vector3 boneToTargetXZ = new Vector3(boneToTarget.x, 0, boneToTarget.z).normalized;
			float xAngle = Vector3.SignedAngle(upVector, boneToTargetXZ, forwardVector);
			float zAngle = Vector3.SignedAngle(Vector3.ProjectOnPlane(boneToTargetXZ, rightVector).normalized, Vector3.ProjectOnPlane(boneToTarget, rightVector).normalized, rightVector);

			xAngle = Mathf.Lerp(0, xAngle, bone.Weight);
			zAngle = Mathf.Lerp(0, zAngle, bone.Weight);
			xAngle = ClampAngle(bone.Bone.rotation.x, bone.initialAngle.x, xAngle, bone.AngleLimit);
			zAngle = ClampAngle(bone.Bone.rotation.z, bone.initialAngle.z, zAngle, bone.AngleLimit);

			bone.Bone.RotateAround(bone.Bone.transform.position, rightVector, zAngle);
			bone.Bone.RotateAround(bone.Bone.transform.position, forwardVector, xAngle);
	
			if(bone.ShowDebugVector){
				Debug.DrawRay(bone.Bone.position, boneToTargetXZ, Color.blue, 0.1f, false);
				Debug.DrawRay(bone.Bone.position, upVector, Color.red, 0.1f, false);
				Debug.DrawRay(bone.Bone.position, forwardVector, Color.green, 0.1f, false);
			}
		}
		
	}

	float ClampAngle(float baseRot, float initialAngle, float value, float limit){
		float angleClamped = Mathf.Clamp(baseRot + value, initialAngle - limit, initialAngle + limit);
		return angleClamped - baseRot;
	}

	void GetAxisVectors(out Vector3 forwardVector, out Vector3 rightVector, out Vector3 upVector, BoneAndAxis boneAndAxis){

		//Get Forward Vector of bone
		switch(boneAndAxis.ForwardAxis){
			case IK.Axis.FORWARD:
				forwardVector = boneAndAxis.Bone.forward;
				break;
			case IK.Axis.RIGHT:
				forwardVector = boneAndAxis.Bone.right;
				break;
			case IK.Axis.UP:
				forwardVector = boneAndAxis.Bone.up;
				break;
			default:
				forwardVector = Vector3.zero;
				break;
		}

		//Get Vector To Rotate Around
		switch(boneAndAxis.RightAxis){
			case IK.Axis.FORWARD:
				rightVector = boneAndAxis.Bone.forward;
				break;
			case IK.Axis.RIGHT:
				rightVector = boneAndAxis.Bone.right;
				break;
			case IK.Axis.UP:
				rightVector = boneAndAxis.Bone.up;
				break;
			default:
				rightVector = Vector3.zero;
				break;
		}

		switch(boneAndAxis.UpAxis){
			case IK.Axis.FORWARD:
				upVector = boneAndAxis.Bone.forward;
				break;
			case IK.Axis.RIGHT:
				upVector = boneAndAxis.Bone.right;
				break;
			case IK.Axis.UP:
				upVector = boneAndAxis.Bone.up;
				break;
			default:
				upVector = Vector3.zero;
				break;
		}
	}
}

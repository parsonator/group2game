﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [System.Serializable]
// public struct MinMax
// {
// 	public float min;
// 	public float max;
// }

// [System.Serializable]
// public class AngleLimits
// {
// 	public MinMax[] XLimit;
// 	public MinMax[] YLimit;
// 	public MinMax[] ZLimit;
// }

public class FABRIKLeg : MonoBehaviour {

#region PublicVar
	[Header("Joints")]
	public Transform[] lArm;
	public Transform[] rArm;

	[Header("Joints Attributes")]
	public IK.Axis lArmForward;
	public IK.Axis rArmForward;

	public float[] lArmDirectionLimit;
	public float[] rArmDirectionLimit;
	
	public AngleLimits lAngleLimit;
	public AngleLimits rAngleLimit;

	public Transform rTarget;
	public Transform lTarget;

	[Header("Debug")]
	public bool showDebugLines = false;
	public bool ShowXAngleLimits = false;
	public bool ShowYAngleLimits = false;
	public bool ShowZAngleLimits = false;

	public bool solveRight = false;
	public bool solveLeft = true;
	
#endregion

#region PrivateVar
	float[] rLengths;
	float[] lLengths;
	Vector3[] lArmForwardVectors;
	Vector3[] lArmRightVectors;
	Vector3[] lArmUpVectors;
	Vector3[] rArmRightVectors;
	Vector3[] lTwist;
	Vector3 rot;
#endregion

	void Start () {
		if(rArm.Length > 0){
			rLengths = new float[rArm.Length-1];
			rArmRightVectors = new Vector3[rArm.Length];

			for(int i = 1; i <= rArm.Length - 1; i++){
				rLengths[i-1] = Vector3.Distance(rArm[i-1].position, rArm[i].position);
			}

			for(int i = 0; i <= rArm.Length - 1; i++){
				rArmRightVectors[i] = GetAxis(rArm[i], rArmForward);
			}
		}

		if(lArm.Length > 0){
			lLengths = new float[lArm.Length-1];
			lArmForwardVectors = new Vector3[lArm.Length];
			lArmRightVectors = new Vector3[lArm.Length];
			lArmUpVectors = new Vector3[lArm.Length];
			lTwist = new Vector3[lArm.Length];
			for(int i = 1; i <= lArm.Length - 1; i++){
				lLengths[i-1] = Vector3.Distance(lArm[i-1].position, lArm[i].position);
			}

			for(int i = 0; i <= lArm.Length - 1; i++){
				lArmForwardVectors[i] = GetAxis(lArm[i], IK.Axis.FORWARD);
				lArmRightVectors[i] = GetAxis(lArm[i], IK.Axis.RIGHT);
				lArmUpVectors[i] = GetAxis(lArm[i], IK.Axis.UP);
				lTwist[i] = lArm[i].localEulerAngles;
 			}
		}
		
	}

	void Update(){
		
	}
	
	void LateUpdate () {
		if(solveRight){
			Vector3 rTargetPos = GetTargetPosition(rTarget, rArm[0], rLengths);
			FABRIKSolver(rArm, rLengths, rArmDirectionLimit, rArmRightVectors, rTargetPos, 10);
			CorrectJointAngle(rArm, rTarget);
		}

		if(solveLeft){

			Vector3 lTargetPos = GetTargetPosition(lTarget, lArm[0], lLengths);
			FABRIKSolver(lArm, lLengths, lArmDirectionLimit, lArmForwardVectors, lTargetPos, 10);
			CorrectJointAngle(lArm, lTarget);
		}

		

		if(showDebugLines){
			// foreach(Transform t in rArm){
			// 	float d = 0.2f;
			// 	Debug.DrawRay(t.position, t.up * d, Color.green, 0.1f);
			// 	Debug.DrawRay(t.position, t.right * d, Color.red, 0.1f);
			// 	Debug.DrawRay(t.position, t.forward * d, Color.blue, 0.1f);
			// }

			// foreach(Transform t in lArm){
			// 	float d = 0.2f;
			// 	Debug.DrawRay(t.position, t.up * d, Color.green, 0.1f);
			// 	Debug.DrawRay(t.position, t.right * d, Color.red, 0.1f);
			// 	Debug.DrawRay(t.position, t.forward * d, Color.blue, 0.1f);
			// }

			if(ShowXAngleLimits)
				DrawAngleLimitX();
			if(ShowYAngleLimits)
				DrawAngleLimitY();
			if(ShowZAngleLimits)
				DrawAngleLimtiZ();
		}
	}

	Vector3 GetTargetPosition(Transform target, Transform shoulder, float[] lengths){
		float armLength = 0;
		foreach(float f in lengths){
			armLength += f;
		}

		Vector3 targetPos;
		float targetDist = Vector3.Distance(target.position, shoulder.position);
		if(targetDist > armLength){
			Vector3 targetDir = (target.position - shoulder.position).normalized;
			targetPos = shoulder.position + (targetDir * (armLength));
		}else{
			targetPos = target.position;
		}
		return targetPos;
	}

	void CorrectJointAngle(Transform[] chain, Transform target){
		Vector3 firstdir = Vector3.ProjectOnPlane(((chain[chain.Length-1].position - chain[chain.Length-2].position).normalized), lArmUpVectors[0]);
		SetDirectionVector(chain[chain.Length-1], firstdir, lArmForward);
		chain[chain.Length-1].rotation = target.rotation;

		for(int i = chain.Length - 2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			chain[i+1].parent = null;
			SetDirectionVector(chain[i], (chain[i+1].position - chain[i].position).normalized, lArmForward);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
		
		rot = chain[0].eulerAngles;
		//chain[0].eulerAngles = new Vector3(0, rot.y, rot.z);
		// for(int i = 1; i < chain.Length; i++){
			
		// 	Vector3 localRot = chain[i].localEulerAngles;
		// 	chain[i].localEulerAngles = new Vector3(lTwist[i].x, localRot.y, localRot.z);
		// 	localRot = chain[i].localEulerAngles;
		// 	if(lAngleLimit.YLimit[i].max != 0 && lAngleLimit.YLimit[i].min !=0){
		// 		if(i == 1){
		// 			Debug.Log(chain[i].localEulerAngles.y + " : " + lAngleLimit.YLimit[i].min);
		// 		}

		// 		if(chain[i].localEulerAngles.y > lAngleLimit.YLimit[i].max){
		// 			chain[i].localEulerAngles = new Vector3(localRot.x, lAngleLimit.YLimit[i].max, localRot.z);
		// 		}

		// 		if(chain[i].localEulerAngles.y < lAngleLimit.YLimit[i].min){
		// 			chain[i].localEulerAngles = new Vector3(localRot.x, lAngleLimit.YLimit[i].min, localRot.z);
		// 		}
		// 	}
		// }	
	}

	void FABRIKSolver(Transform[] chain, float[] lengths, float[] angleLimits, Vector3[] rightVectors, Vector3 targetPos, int iterations){
		int i = 0;
		Vector3 start = chain[0].position;
		do{
			Backward(chain, lengths, targetPos, start);
			Forward(chain, lengths, angleLimits, rightVectors, targetPos, start);
			float dist = Vector3.Distance(chain[chain.Length-1].position, targetPos);
			if(dist < 0.01f) return;
			i++;
		}while(i <= iterations);
	}

	void Backward(Transform[] chain, float[]lengths, Vector3 targetPos, Vector3 start){

		Vector3 dir;
		chain[chain.Length-1].position = targetPos;

		for(int i = chain.Length-2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			dir = (chain[i].position - chain[i+1].position).normalized;
			dir = Vector3.ProjectOnPlane(dir, lArmUpVectors[0]);
			chain[i+1].parent = null;
			chain[i].position = chain[i+1].position + (dir * lengths[i]);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
	}

	void Forward(Transform[] chain, float[] lengths, float[] angleLimits, Vector3[] rightVectors, Vector3 targetPos, Vector3 start){

		chain[0].position = start;

		for(int i = 1; i <= chain.Length-1; i++){
			Vector3 dir; 
			dir = (chain[i].position - chain[i-1].position).normalized;
			Debug.DrawRay(chain[i].position, dir, Color.red, 0.1f);
			Vector3 newdir = Vector3.ProjectOnPlane(dir,  lArmUpVectors[0]);
			Vector3 target = chain[i-1].position + (dir * lengths[i-1]);
			float dist = Vector3.Distance(chain[i-1].position, target);

			float angle = Vector3.Dot(-rightVectors[i-1], dir);
		 	angle = Mathf.Acos(angle)*Mathf.Rad2Deg;

			if(angle > angleLimits[i-1]){
				Vector3 cross = Vector3.Cross(-rightVectors[i-1], dir);
				Vector3 newDir = Quaternion.AngleAxis(angleLimits[i-1], cross) * -rightVectors[i-1];
				chain[i].position = chain[i-1].position + newDir*dist;
			}else{
				chain[i].position = target;
			}
			
		}
	}

	Vector3 GetAxis(Transform bone, IK.Axis axis){
		//Get Forward Vector of bone
		Vector3 vector;
		switch(axis){
			case IK.Axis.FORWARD:
				vector = bone.forward;
				break;
			case IK.Axis.RIGHT:
				vector = bone.right;
				break;
			case IK.Axis.UP:
				vector = bone.up;
				break;
			case IK.Axis.NFORWARD:
				vector  = -bone.forward;
				break;
			case IK.Axis.NRIGHT:
				vector  = -bone.right;
				break;
			case IK.Axis.NUP:
				vector  = -bone.up;
				break;
			default:
				vector = Vector3.zero;
				break;

		}
		return vector;
	}

	void SetDirectionVector(Transform bone, Vector3 NewVector, IK.Axis axis){
		switch(axis){
			case IK.Axis.FORWARD:
				bone.forward = NewVector;
				break;
			case IK.Axis.RIGHT:
				bone.right = NewVector;
				break;
			case IK.Axis.UP:
				bone.up = NewVector;
				break;
			case IK.Axis.NFORWARD:
				bone.forward = -NewVector;
				break;
			case IK.Axis.NRIGHT:
				bone.right = -NewVector;
				break;
			case IK.Axis.NUP:
				bone.up = -NewVector;
				break;
			default:
				
				break;

		}
	}

	void DrawAngleLimitX(){
		for(int i = 0; i < lArm.Length; i++){
			// Vector3 RotatedVector = Quaternion.AngleAxis(lAngleLimit.XLimit[i].max, lArmRightVectors[i]) * lArmUpVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.red, 0.1f);
			// RotatedVector = Quaternion.AngleAxis(lAngleLimit.XLimit[i].min, lArmRightVectors[i]) * lArmUpVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.red, 0.1f);
		}
	}

	void DrawAngleLimitY(){
		for(int i = 0; i < lArm.Length; i++){
			// Vector3 RotatedVector = Quaternion.AngleAxis(lAngleLimit.YLimit[i].max, lArmUpVectors[i]) * lArmRightVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.green, 0.1f);
			// RotatedVector = Quaternion.AngleAxis(lAngleLimit.YLimit[i].min, lArmUpVectors[i]) * lArmRightVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.green, 0.1f);
		}
	}

	void DrawAngleLimtiZ(){
		for(int i = 0; i < lArm.Length; i++){
			// Vector3 RotatedVector = Quaternion.AngleAxis(lAngleLimit.ZLimit[i].max, lArmForwardVectors[i]) * -lArmRightVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.blue, 0.1f);
			// RotatedVector = Quaternion.AngleAxis(lAngleLimit.ZLimit[i].min, lArmForwardVectors[i]) * -lArmRightVectors[i];
			// Debug.DrawRay(lArm[i].position, RotatedVector, Color.blue, 0.1f);
		}
	}

	float GetAngleBetweenVectors(Vector3 a, Vector3 b){
		float angle = Vector3.Dot(a.normalized, b.normalized);
		angle = GetAngle360(Mathf.Acos(angle) * Mathf.Rad2Deg);
		if(angle > 180)
			return 360 - angle;
		else
			return angle;
	}

	float GetAngle360(float angle){
		if(angle < 0)
			return angle + 360;
		else
			return angle;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ArmIK : MonoBehaviour {

#region PublicVars
	public Transform lShoulder;
	public Transform lElbow;
	public Transform lHand;

	public Transform rShoulder;
	public Transform rElbow;
	public Transform rHand;

	public Transform lTarget;
	public Transform rTarget;

	public bool DebugVisuals = false;
	public bool SolveShoulder = false;
	public bool SolveElbow = false;
	public int Iterations = 5;
#endregion

#region PrivateVars
	private Vector3 target;
#endregion


	void LateUpdate(){		
		//Solve left chain
		AnalyticalIKSolver(lShoulder, lElbow, lHand, lTarget, false); 	
		//Solve right chain
		AnalyticalIKSolver(rShoulder, rElbow, rHand, rTarget, true); 
		
	}

	void AnalyticalIKSolver(Transform shoulder, Transform elbow, Transform hand, Transform transformTarget, bool right){
		int iterationCount = 0;	

		target = transformTarget.position;

		Vector3 pelvisFoot = Vector3.zero;
		Vector3 pelvisTarget = Vector3.zero;
		Vector3 footKnee = Vector3.zero;
		Vector3 kneeTarget = Vector3.zero;

		Vector3 pelvisAxisRot = Vector3.zero;
		Vector3 kneeAxisRot = Vector3.zero;

		while(iterationCount < Iterations){
			//Vectors to compute the dot product
			pelvisFoot = (hand.position - shoulder.position).normalized;
			pelvisTarget = (target - shoulder.position).normalized;
			
			//Angle
			float angle = Vector3.Dot(pelvisFoot, pelvisTarget);
			angle = Mathf.Acos(angle)*Mathf.Rad2Deg;
			//Axis rotation
			pelvisAxisRot = Vector3.Cross(pelvisTarget, pelvisFoot).normalized;
			//Rotate
			if(SolveShoulder)
				shoulder.Rotate(-pelvisAxisRot, angle, Space.World);

			//Vectors to compute the dot product
			footKnee = (hand.position - elbow.position).normalized;
			kneeTarget = (target - elbow.position).normalized;

			//Angle
			float angle2 = Vector3.Dot(footKnee, kneeTarget);
			angle2 = Mathf.Acos(angle2)*Mathf.Rad2Deg;
			//Axis rotation
			kneeAxisRot = Vector3.Cross(kneeTarget, footKnee).normalized;
			//Rotate
			if(SolveElbow)
				elbow.Rotate(-kneeAxisRot, angle2, Space.World);

			iterationCount++;
		}

		hand.rotation = transformTarget.rotation;

		if(DebugVisuals){
			//Need to get again their direction to visualise the updated chain
			footKnee = (hand.position - elbow.position).normalized;
			pelvisFoot = (hand.position - shoulder.position).normalized;

			//Draw elbow debug lines - magenta is the axis of rotation, blue is the desired vector, yellow is the bone rotation
			Debug.DrawLine(elbow.position, elbow.position + kneeTarget, Color.blue, 0.1f, false);
			Debug.DrawLine(elbow.position, elbow.position + footKnee, Color.yellow, 0.1f, false);
			Debug.DrawLine(elbow.position, elbow.position + kneeAxisRot, Color.magenta, 0.1f, false);
			//Draw shoulder debug lines - magenta is the axis of rotation, blue is the desired vector, yellow is the bone rotation
			Debug.DrawLine(shoulder.position, shoulder.position + pelvisFoot, Color.green, 0.1f, false);
			Debug.DrawLine(shoulder.position, shoulder.position + pelvisTarget, Color.yellow, 0.1f, false);
			Debug.DrawLine(shoulder.position, shoulder.position + pelvisAxisRot, Color.magenta, 0.1f, false);
		}
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		if(DebugVisuals)
			Gizmos.DrawWireSphere(target, 0.1f);
	}
}



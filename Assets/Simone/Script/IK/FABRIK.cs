﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FABRIK : MonoBehaviour {

#region PublicVar
	public Transform[] lArm;
	public Transform[] rArm;
	public float[] lArmAngleLimit;
	public float[] rArmAngleLimit;
	public float[] rAjdustAngles;
	public float[] lAdjustAngles;
	public Vector3[] lTwistLimit;
	public Transform rTarget;
	public Transform lTarget;
	public bool showDebugLines = false;
	public bool solveRight = false;
	public bool solveLeft = true;
	
#endregion

#region PrivateVar
	float[] rLengths;
	float[] lLengths;
	Vector3[] lArmRightVectors;
	Vector3[] rArmRightVectors;
	Vector3[] lTwist;
#endregion

	void Start () {
		if(rArm.Length > 0){
			rLengths = new float[rArm.Length-1];
			rArmRightVectors = new Vector3[rArm.Length];

			for(int i = 1; i <= rArm.Length - 1; i++){
				rLengths[i-1] = Vector3.Distance(rArm[i-1].position, rArm[i].position);
			}

			for(int i = 0; i <= rArm.Length - 1; i++){
				rArmRightVectors[i] = rArm[i].right;
			}
		}

		if(lArm.Length > 0){
			lLengths = new float[lArm.Length-1];
			lArmRightVectors = new Vector3[lArm.Length];
			lTwist = new Vector3[lArm.Length];
			for(int i = 1; i <= lArm.Length - 1; i++){
				lLengths[i-1] = Vector3.Distance(lArm[i-1].position, lArm[i].position);
			}

			for(int i = 0; i <= lArm.Length - 1; i++){
				lArmRightVectors[i] = lArm[i].right;
				lTwist[i] = lArm[i].localEulerAngles;
 			}
		}
		
	}
	
	void LateUpdate () {
		if(solveRight){
			Vector3 rTargetPos = GetTargetPosition(rTarget, rArm[0], rLengths);
			FABRIKSolver(rArm, rLengths, rArmAngleLimit, rArmRightVectors, rTargetPos, 10);
			CorrectJointAngle(rArm, rTarget, rAjdustAngles, true);
		}

		if(solveLeft){

			Vector3 lTargetPos = GetTargetPosition(lTarget, lArm[0], lLengths);
			FABRIKSolver(lArm, lLengths, lArmAngleLimit, lArmRightVectors, lTargetPos, 10);
			CorrectJointAngle(lArm, lTarget, lAdjustAngles, false);
		}

		

		if(showDebugLines){
			foreach(Transform t in rArm){
				float d = 0.2f;
				Debug.DrawRay(t.position, t.up * d, Color.green, 0.1f);
				Debug.DrawRay(t.position, t.right * d, Color.red, 0.1f);
				Debug.DrawRay(t.position, t.forward * d, Color.blue, 0.1f);
			}

			foreach(Transform t in lArm){
				float d = 0.2f;
				Debug.DrawRay(t.position, t.up * d, Color.green, 0.1f);
				Debug.DrawRay(t.position, t.right * d, Color.red, 0.1f);
				Debug.DrawRay(t.position, t.forward * d, Color.blue, 0.1f);
			}
		}
	}

	Vector3 GetTargetPosition(Transform target, Transform shoulder, float[] lengths){
		float armLength = 0;
		foreach(float f in lengths){
			armLength += f;
		}

		Vector3 targetPos;
		float targetDist = Vector3.Distance(target.position, shoulder.position);
		if(targetDist > armLength){
			Vector3 targetDir = (target.position - shoulder.position).normalized;
			targetPos = shoulder.position + (targetDir * (armLength - 0.05f));
		}else{
			targetPos = target.position;
		}
		return targetPos;
	}

	void CorrectJointAngle(Transform[] chain, Transform target, float[] shoulderCorrection, bool right){
		chain[chain.Length-1].right = (chain[chain.Length-2].position - chain[chain.Length-1].position).normalized;
		chain[chain.Length-1].rotation = target.rotation;

		for(int i = chain.Length - 2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			chain[i+1].parent = null;
			chain[i].right = (chain[i].position - chain[i+1].position).normalized;
			chain[i].localRotation *= Quaternion.Euler(shoulderCorrection[i], 0, 0);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
		
		for(int i = 0; i < chain.Length-1; i++){
			Vector3 euler = new Vector3();
			if(lTwistLimit[i].x != 0){
				euler.x = Mathf.Clamp(chain[i].localEulerAngles.x, lTwist[i].x - lTwistLimit[i].x, lTwist[i].x + lTwistLimit[i].x);
			}else euler.x = chain[i].localEulerAngles.x;

			if(lTwistLimit[i].y != 0){
				euler.y = Mathf.Clamp(chain[i].localEulerAngles.y, lTwist[i].y - lTwistLimit[i].y, lTwist[i].y + lTwistLimit[i].y);
			}else euler.y = chain[i].localEulerAngles.y;

			if(lTwistLimit[i].z != 0){
				euler.z = Mathf.Clamp(chain[i].localEulerAngles.z, lTwist[i].z - lTwistLimit[i].z, lTwist[i].z + lTwistLimit[i].z);
			}else euler.z = chain[i].localEulerAngles.z;

			chain[i].localEulerAngles = euler;
		}

		
	}

	void FABRIKSolver(Transform[] chain, float[] lengths, float[] angleLimits, Vector3[] rightVectors, Vector3 targetPos, int iterations){
		int i = 0;
		Vector3 start = chain[0].position;
		do{
			Backward(chain, lengths, targetPos, start);
			Forward(chain, lengths, angleLimits, rightVectors, targetPos, start);
			float dist = Vector3.Distance(chain[chain.Length-1].position, targetPos);
			if(dist < 0.01f) return;
			i++;
		}while(i <= iterations);
	}

	void Backward(Transform[] chain, float[]lengths, Vector3 targetPos, Vector3 start){

		Vector3 dir;
		chain[chain.Length-1].position = targetPos;

		for(int i = chain.Length-2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			dir = (chain[i].position - chain[i+1].position).normalized;
			chain[i+1].parent = null;
			chain[i].position = chain[i+1].position + (dir * lengths[i]);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
	}

	void Forward(Transform[] chain, float[] lengths, float[] angleLimits, Vector3[] rightVectors, Vector3 targetPos, Vector3 start){

		chain[0].position = start;

		for(int i = 1; i <= chain.Length-1; i++){
			Vector3 dir; 
			dir = (chain[i].position - chain[i-1].position).normalized;
			Vector3 target = chain[i-1].position + (dir * lengths[i-1]);
			float dist = Vector3.Distance(chain[i-1].position, target);

			float angle = Vector3.Dot(-rightVectors[i-1], dir);
		 	angle = Mathf.Acos(angle)*Mathf.Rad2Deg;

			if(angle > angleLimits[i-1]){
				Vector3 cross = Vector3.Cross(-rightVectors[i-1], dir);
				Vector3 newDir = Quaternion.AngleAxis(angleLimits[i-1], cross) * -rightVectors[i-1];
				chain[i].position = chain[i-1].position + newDir*dist;
			}else{
				chain[i].position = target;
			}
		}
	}

	
}

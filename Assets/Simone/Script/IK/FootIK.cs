﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FootIK : MonoBehaviour {

#region PublicVars
	public Transform Body;
	public Transform lPelvis;
	public Transform lKnee;
	public Transform lFoot;
	public Transform lToe;

	public Transform rPelvis;
	public Transform rKnee;
	public Transform rFoot;
	public Transform rToe;

	public bool SolveLeft = true;
	public bool SolveRight = true;
	
	public bool DebugVisuals = false;
	public bool SolvePelvis = false;
	public bool SolveKnee = false;
	public bool AdjustFootToSurface = false;
	public int Iterations = 10;
#endregion

#region PrivateVars
	private Vector3 target;
	private float t = 0;
	float floorDistance = 0;
	CharacterController CharController;
	Animator anim;
#endregion

	void Start(){
		CharController = GetComponent<CharacterController>();
		anim = GetComponentInChildren<Animator>();
	}

	void LateUpdate(){		
		//If we are not moving then we can adjust the collider and consequently apply the IK alg
		//Else reset everything to the intial state
		if(Mathf.Abs(anim.GetFloat("XVol")) < 0.1f && Mathf.Abs(anim.GetFloat("YVol")) < 0.1f){
			AdjustCollider();
		}

		//Solve left chain
		AnalyticalIKSolver(lPelvis, lKnee, lFoot, lToe); 	
		//Solve right chain
		AnalyticalIKSolver(rPelvis, rKnee, rFoot, rToe); 
		//Adjust left foot
		AdjustFoot(lFoot, lToe);
		//Adjust right foot
		AdjustFoot(rFoot, rToe);
	}

	void AdjustCollider(){	
		RaycastHit[] rHits;
		RaycastHit rHit = new RaycastHit();
		RaycastHit[] lHits;
		RaycastHit lHit = new RaycastHit();

		Vector3 rightSide = new Vector3(rFoot.position.x, CharController.transform.position.y + (CharController.height/2), rFoot.position.z);
		Vector3 leftSide = new Vector3(lFoot.position.x, CharController.transform.position.y + (CharController.height/2), lFoot.position.z);

		rHits = Physics.RaycastAll(rightSide, -Vector3.up * 1.10f, 1.10f).OrderBy(h=>h.distance).ToArray();;
		lHits = Physics.RaycastAll(leftSide, -Vector3.up * 1.10f, 1.10f).OrderBy(h=>h.distance).ToArray();;

		foreach(RaycastHit h in rHits){
			if(h.transform.tag == "world"){
				rHit = h;
				break;
			}
		}

		foreach(RaycastHit h in lHits){
			if(h.transform.tag == "world"){
				lHit = h;
				break;
			}
		}
		
		if(rHit.distance > lHit.distance)
			Body.position = new Vector3(Body.position.x, rHit.point.y + (CharController.height/1.9f), Body.position.z);
		else 
			Body.position = new Vector3(Body.position.x, lHit.point.y + (CharController.height/1.9f), Body.position.z);
	}

	void AnalyticalIKSolver(Transform pelvis, Transform knee, Transform foot, Transform toe){
		int iterationCount = 0;	
		
		//Get Ground for foot placement
		RaycastHit[] hits;
		RaycastHit hit = new RaycastHit();
		
		hits = Physics.RaycastAll(foot.position + (Vector3.up*0.3f), -Vector3.up*0.4f, 0.4f).OrderBy(h=>h.distance).ToArray();
		bool foundGround = false;
		foreach(RaycastHit h in hits){
			if(h.transform.tag == "world"){
				hit = h;
				target = h.point;
				foundGround = true;
			}
			if(foundGround) break;
		}
		if(!foundGround) return;

		Vector3 pelvisFoot = Vector3.zero;
		Vector3 pelvisTarget = Vector3.zero;
		Vector3 footKnee = Vector3.zero;
		Vector3 kneeTarget = Vector3.zero;

		Vector3 pelvisAxisRot = Vector3.zero;
		Vector3 kneeAxisRot = Vector3.zero;

		while(iterationCount < Iterations){
			//Vectors to compute the dot product
			pelvisFoot = (GetDisplacedFoot(foot, 0.1f) - pelvis.position).normalized;
			pelvisTarget = (target - pelvis.position).normalized;
			
			//Angle
			float angle = Vector3.Dot(pelvisFoot, pelvisTarget);
			angle = Mathf.Acos(angle)*Mathf.Rad2Deg;
			//Axis rotation
			pelvisAxisRot = Vector3.Cross(pelvisTarget, pelvisFoot).normalized;
			//Rotate
			if(SolvePelvis)
				pelvis.Rotate(-pelvisAxisRot, angle, Space.World);

			//Vectors to compute the dot product
			footKnee = (GetDisplacedFoot(foot, 0.1f) - knee.position).normalized;
			kneeTarget = (target - knee.position).normalized;

			//Angle
			float angle2 = Vector3.Dot(footKnee, kneeTarget);
			angle2 = Mathf.Acos(angle2)*Mathf.Rad2Deg;
			//Axis rotation
			kneeAxisRot = Vector3.Cross(kneeTarget, footKnee).normalized;
			//Rotate
			if(SolveKnee)
				knee.Rotate(-kneeAxisRot, angle2, Space.World);

			iterationCount++;
		}

		if(DebugVisuals){
			//Need to get again their direction to visualise the updated chain
			footKnee = (foot.position - knee.position).normalized;
			pelvisFoot = (foot.position - pelvis.position).normalized;

			//Draw knee debug lines - magenta is the axis of rotation, blue is the desired vector, yellow is the bone rotation
			Debug.DrawLine(knee.position, knee.position + kneeTarget, Color.blue, 0.1f, false);
			Debug.DrawLine(knee.position, knee.position + footKnee, Color.yellow, 0.1f, false);
			Debug.DrawLine(knee.position, knee.position + kneeAxisRot, Color.magenta, 0.1f, false);
			//Draw pelvis debug lines - magenta is the axis of rotation, blue is the desired vector, yellow is the bone rotation
			Debug.DrawLine(pelvis.position, pelvis.position + pelvisFoot, Color.green, 0.1f, false);
			Debug.DrawLine(pelvis.position, pelvis.position + pelvisTarget, Color.yellow, 0.1f, false);
			Debug.DrawLine(pelvis.position, pelvis.position + pelvisAxisRot, Color.magenta, 0.1f, false);
		}
	}

	void AdjustFoot(Transform foot, Transform toe){

		RaycastHit[] hits;
		RaycastHit hit = new RaycastHit();
		
		hits = Physics.RaycastAll(foot.position, -Vector3.up * 0.3f, 0.3f).OrderBy(h=>h.distance).ToArray();;
		foreach(RaycastHit h in hits){
			if(h.transform.tag == "world"){
				hit = h;
				break;
			}
		}

		if(AdjustFootToSurface){
			//Let's get the base of the foot - the foot bone is actually the ankle
			Vector3 footBase = GetDisplacedFoot(foot, 0.08f);
			Vector3 surfaceNormal = hit.normal;

			//Rotate the normal of the surface to point in the foot direction and flat on the surface
			Vector3 flatNormal = Quaternion.AngleAxis(90, foot.right) * surfaceNormal;

			//Use fromtorotation to align the right vector of the foot to the surface normal									   /**Adjust foot x rotation */
			foot.transform.rotation = Quaternion.FromToRotation(foot.transform.forward, surfaceNormal) * foot.transform.rotation * Quaternion.Euler(-30, 0, 0);

			if(DebugVisuals){
				Debug.DrawLine(footBase, footBase + flatNormal.normalized, Color.white, .1f, false); //flatNormal visualisation
				Debug.DrawLine(footBase, footBase + (toe.position - footBase).normalized, Color.blue, .1f, false); //foot direction visualisation
			}
		}
	}

	Vector3 GetDisplacedFoot(Transform foot, float by){
		return foot.position - new Vector3(0, by, 0);
	}

	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		if(DebugVisuals)
			Gizmos.DrawWireSphere(target, 0.1f);
	}

	public void RightOn(){
		SolveRight = true;
	}

	public void RightOff(){
		SolveRight = false;
	}

	public void LeftOn(){
		SolveLeft = true;
	}

	public void LeftOff(){
		SolveLeft = false;
	}
}

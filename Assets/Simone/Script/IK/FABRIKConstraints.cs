﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MinMax
{
	public float min;
	public float max;
}

[System.Serializable]
public class AngleLimits
{
	public MinMax[] Limit;
}

public class FABRIKConstraints : MonoBehaviour {

#region PublicVar
	public Transform[] Chain;
	private AngleLimits ChainAngleLimit;
	public Transform Target;
	public IK.Axis ConstarintOnPlane;
	public IK.Axis PlayerFacingDirection;
	public bool ShowDebugLines = false;
	public bool SolveChain = true;
	
#endregion

#region PrivateVar
	float[] Lengths;
	Vector3[] UpVectors;
	Vector3[] ForwardVectors;
	Vector3[] RightVectors;
	Vector3[] Plane;
#endregion

	void Start () {

		if(Chain.Length > 0){
			Lengths = new float[Chain.Length-1];
			UpVectors = new Vector3[Chain.Length];
			ForwardVectors = new Vector3[Chain.Length];
			RightVectors = new Vector3[Chain.Length];
			for(int i = 1; i <= Chain.Length - 1; i++){
				Lengths[i-1] = Vector3.Distance(Chain[i-1].position, Chain[i].position);
			}

			for(int i = 0; i <= Chain.Length - 1; i++){
				UpVectors[i] = GetAxis(Chain[i], IK.Axis.UP);
				ForwardVectors[i] = GetAxis(Chain[i], IK.Axis.FORWARD);
				RightVectors[i] = GetAxis(Chain[i], IK.Axis.RIGHT);
 			}
		}
		
		switch(ConstarintOnPlane){
			case IK.Axis.FORWARD:
				Plane = ForwardVectors;
				break;
			case IK.Axis.RIGHT:
				Plane = RightVectors;
				break;
			case IK.Axis.UP:
				Plane = UpVectors;
				break;
			default:
				break;
		}

	}
	
	void LateUpdate () {
		if(SolveChain){
			Vector3 lTargetPos = GetTargetPosition(Target, Chain[0], Lengths);
			FABRIKSolver(Chain, Lengths, ChainAngleLimit, RightVectors, lTargetPos, 10);
			CorrectJointAngle(Chain, Target);
		}

		if(ShowDebugLines){
			foreach(Transform t in Chain){
				float d = 0.2f;
				Debug.DrawRay(t.position, t.up * d, Color.green, 0.1f);
				Debug.DrawRay(t.position, t.right * d, Color.red, 0.1f);
				Debug.DrawRay(t.position, t.forward * d, Color.blue, 0.1f);
			}
		}
	}

	Vector3 GetTargetPosition(Transform target, Transform shoulder, float[] lengths){
		float armLength = 0;
		foreach(float f in lengths){
			armLength += f;
		}

		Vector3 targetPos;
		float targetDist = Vector3.Distance(target.position, shoulder.position);
		if(targetDist > armLength){
			Vector3 targetDir = (target.position - shoulder.position).normalized;
			targetPos = shoulder.position + (targetDir * (armLength - 0.05f));
		}else{
			targetPos = target.position;
		}

		targetPos = Vector3.ProjectOnPlane(target.position, Plane[0]);

		return targetPos;
	}

	void CorrectJointAngle(Transform[] chain, Transform target){

		chain[chain.Length-1].forward = -(chain[chain.Length-2].position - chain[chain.Length-1].position).normalized;
		chain[chain.Length-1].rotation = target.rotation;

		for(int i = chain.Length - 2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			chain[i+1].parent = null;
			Vector3 toNext = (chain[i].position - chain[i+1].position).normalized;
			chain[i].up = -toNext;
			toNext = Quaternion.AngleAxis(90, RightVectors[0]) * -toNext;
			//chain[i].LookAt(chain[i].position + RightVectors[0], toNext);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
		
	}

	void FABRIKSolver(Transform[] chain, float[] lengths, AngleLimits angleLimits, Vector3[] rightVectors, Vector3 targetPos, int iterations){
		int i = 0;
		Vector3 start = chain[0].position;
		do{
			Backward(chain, lengths, targetPos, start);
			Forward(chain, lengths, angleLimits, rightVectors, targetPos, start);
			float dist = Vector3.Distance(chain[chain.Length-1].position, targetPos);
			if(dist < 0.01f) return;
			i++;
		}while(i <= iterations);
	}

	void Backward(Transform[] chain, float[]lengths, Vector3 targetPos, Vector3 start){

		Vector3 dir;
		chain[chain.Length-1].position = targetPos;

		for(int i = chain.Length-2; i >= 0; i--){
			Vector3 scale = chain[i+1].localScale;
			dir = (chain[i].position - chain[i+1].position).normalized;
			dir = Vector3.ProjectOnPlane(dir, Plane[0]).normalized;
			chain[i+1].parent = null;
			chain[i].position = chain[i+1].position + (dir * lengths[i]);
			chain[i+1].parent = chain[i];
			chain[i+1].localScale = scale;
		}
	}

	void Forward(Transform[] chain, float[] lengths, AngleLimits angleLimits, Vector3[] rightVectors, Vector3 targetPos, Vector3 start){
		chain[0].position = start;

		for(int i = 1; i <= chain.Length-1; i++){
			Vector3 dir = (chain[i].position - chain[i-1].position).normalized;
			dir = Vector3.ProjectOnPlane(dir, Plane[0]).normalized;
			Vector3 target = chain[i-1].position + (dir * lengths[i-1]);
			float dist = Vector3.Distance(chain[i-1].position, target);

			if(i == 1){
				float angle = Vector3.Dot(-RightVectors[0], dir);
				angle = Mathf.Acos(angle)*Mathf.Rad2Deg;
				Vector3 cross = Vector3.Cross(-RightVectors[0], dir);
				Vector3 dir2 = Quaternion.AngleAxis(-90, cross) * dir;
				float ang1 = Mathf.Acos(Vector3.Dot(ForwardVectors[0], dir)) * Mathf.Rad2Deg;
				float ang2 = Mathf.Acos(Vector3.Dot(ForwardVectors[0], dir2)) * Mathf.Rad2Deg;
				chain[i].position = ang1 < ang2 ? chain[i-1].position + dir*(lengths[i-1]) : chain[i-1].position + dir2*(lengths[i-1]);
			}else{
				chain[i].position = target;
			}
		}
	}

	Vector3 GetAxis(Transform bone, IK.Axis axis){
		//Get Forward Vector of bone
		Vector3 vector;
		switch(axis){
			case IK.Axis.FORWARD:
				vector = bone.forward;
				break;
			case IK.Axis.RIGHT:
				vector = bone.right;
				break;
			case IK.Axis.UP:
				vector = bone.up;
				break;
			case IK.Axis.NFORWARD:
				vector  = -bone.forward;
				break;
			case IK.Axis.NRIGHT:
				vector  = -bone.right;
				break;
			case IK.Axis.NUP:
				vector  = -bone.up;
				break;
			default:
				vector = Vector3.zero;
				break;

		}
		return vector;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameTagFaceCamera : MonoBehaviour {

	private Camera camera;
	// Use this for initialization
	void Start () {
		camera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 v = camera.transform.position - transform.position;
		transform.LookAt(camera.transform.position); 
	}
}

﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbitImproved_Simon : MonoBehaviour
{
    public Transform target;
    public Vector3 lookOffset;
    public float distance = 5.0f;
    public float xSpeed = 120.0f;
    public float ySpeed = 120.0f;
    public float ZoomSpeed = 10f;
    public float yMinLimit = -20f;
    public float yMaxLimit = 80f;
    public float mouseDampening = 1f;
    public LayerMask CamNoCollide;
    public bool coll;
    public float camRecoverySpeed = .5f;
    public bool RightSide;
    public float distanceMin = .5f;
    public float distanceMax = 15f;
    private Rigidbody camRigidbody;
    float x = 0.0f;
    [SerializeField] float y = 0.0f;
    [SerializeField] Vector3 camAngle;
    float PlayerY;
    bool focus = true;
    [HideInInspector] public bool rotateCharacter = false;
    Animator animator;
    bool CanTurnRight = true;
    bool CanTurnLeft = true;

    // Use this for initialization
    void Start(){
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        camRigidbody = GetComponent<Rigidbody>();

        // Make the rigid body not change rotation
        if (camRigidbody != null)
            camRigidbody.freezeRotation = true;

        animator = target.GetComponent<Animator>();
    }

    void setFocus(bool focus){
        if (focus){
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void LateUpdate(){
        if (Input.GetKeyDown("b"))
            setFocus(!focus);

        if (target){
            Vector3 lookPoz;
            if (RightSide)
                lookPoz = target.position + ((target.right * lookOffset.x) + (target.up * lookOffset.y));
            else
                lookPoz = target.position + ((-target.right * lookOffset.x) + (target.up * lookOffset.y));

            RaycastHit hit;
            Vector3 v = transform.position + (-transform.forward * 0.1f);
            coll = Physics.Raycast(lookPoz, v - lookPoz, out hit, Vector3.Distance(lookPoz, v), CamNoCollide);
            Debug.DrawLine(transform.position, lookPoz);
            distance = Mathf.MoveTowards(distance, distanceMax, Time.deltaTime * camRecoverySpeed);

            if (coll == true)
                distance = Vector3.Distance(lookPoz, hit.point);

            PlayerY = target.eulerAngles.y;
            Vector3 playerAngle = target.eulerAngles;

            x += PlayerInput.MouseAxis().x * xSpeed* (mouseDampening*2) * 0.01f;
            y -= PlayerInput.MouseAxis().y * ySpeed * mouseDampening * 0.01f;

            y = ClampAngle(y, yMinLimit, yMaxLimit);
            camAngle = transform.eulerAngles - target.eulerAngles;
            camAngle = transform.eulerAngles - target.eulerAngles;

            Quaternion rotation = Quaternion.identity;
            rotation = Quaternion.Euler(y, x, 0);

            Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * negDistance + lookPoz;
            transform.rotation = rotation;
            transform.position = position;

            /**Turn player 90 degrees when rotate the camera over 90 degrees from the player forward vector */
            Vector3 CameraForward = (Quaternion.Euler(0, transform.eulerAngles.y, 0) * Vector3.forward).normalized;
            float CameraTargetAngle = Vector3.Dot(CameraForward, target.transform.forward);
            CameraTargetAngle = Mathf.Acos(CameraTargetAngle) * Mathf.Rad2Deg;
            float side = Vector3.Cross(CameraForward, target.transform.forward).y;
            side = side < 0 ? -1 : 1;

            if((CameraTargetAngle * side) > 75 && CanTurnLeft){
                animator.SetTrigger("TurnLeft");
                CanTurnLeft = false;
                StartCoroutine(ResetCanTurn("Left"));
            }else if((CameraTargetAngle * side) < -75 && CanTurnRight){
                animator.SetTrigger("TurnRight");
                CanTurnRight = false;
                StartCoroutine(ResetCanTurn("Right"));
            }
        }
    }

    private IEnumerator ResetCanTurn(string side){
        yield return new WaitForSeconds(1);
        if(side == "Right")
            CanTurnRight = true;
        else if(side == "Left")
            CanTurnLeft = true;
    }

    public static float ClampAngle(float angle, float min, float max){
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;

        return Mathf.Clamp(angle, min, max);
    }
}
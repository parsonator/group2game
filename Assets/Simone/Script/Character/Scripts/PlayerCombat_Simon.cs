﻿using UnityEngine;
using System.Collections;

public class PlayerCombat_Simon : Combat_Simon
{
    //public Ability ability;
    public AudioSource CombatSounds;
    Animator animator;
    Transform WeaponStartTransform;

    void Awake(){
        //ability = GetComponent<Ability>();
        CombatSounds = GetComponent<AudioSource>();
        if (GunPrefs == null)
            Debug.LogError("No Gun Set");
        currentAmmo = GunPrefs.maxAmmo;

        playerControler = GetComponent<PlayerControler_Simon>();
        animator = GetComponent<Animator>();

        WeaponStartTransform = Weapon.transform;
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update(){

        GunBarrle.LookAt(PlayerAiming_Simon.CroshairHit);
        if (PlayerInput.shootingButton() && !reloading){
            if(animator.GetFloat("XVol") > 0 || animator.GetFloat("YVol") > 0){
                animator.SetLayerWeight(1, 1);
                Weapon.transform.localPosition = new Vector3(-0.3619f, -0.304f, -0.1246f);
                Weapon.transform.localRotation = Quaternion.Euler(new Vector3(-231.585f, -276.275f, -67.82199f));
            }else{   
                StopCoroutine("MoveWeaponBackTo");
                animator.SetBool("Shoot", true);
                StartCoroutine(MoveWeaponTo(new Vector3(-0.357f, -0.199f, -0.167f), new Vector3(-220.649f, -288.485f, -75.65399f)));
            }
            
            if (currentAccuracy <= GunPrefs.accuracy)
                currentAccuracy = Mathf.MoveTowards(currentAccuracy, GunPrefs.accuracy, GunPrefs.stability * Time.deltaTime);

            StartCoroutine(NormalShoot());
        }else{
            StopCoroutine("MoveWeaponTo");
            animator.SetBool("Shoot", false);
            float delay = 0.05f;
            if(animator.GetFloat("XVol") > 0 || animator.GetFloat("YVol") > 0) delay = 0;
            StartCoroutine(MoveWeaponBackTo(delay, new Vector3(-0.267f, 0.01f, 0.265f), new Vector3(-223.98f, -226.595f, -31.45599f)));
            animator.SetLayerWeight(1, 0);            
           
            currentAccuracy = Mathf.MoveTowards(currentAccuracy, 0, 50 * Time.deltaTime);
        }

        // if (PlayerInput.AbilityButtonDown() && ability.abilityReady){
        //     ability.UseAbility();
        //     ability.cooldown = 0;
        //     ability.abilityReady = false;
        // }

        if (currentAmmo <= 0 || PlayerInput.ReloadButtonDown()){
            if (GunPrefs.slowReload)
                StartCoroutine(SlowReload());
            else StartCoroutine(Reload());
        }

        Debug.DrawLine(GunBarrle.position, GunBarrle.forward * 1000, Color.red);

        // if (!ability.abilityReady){
        //     ability.cooldown += Time.deltaTime;
        //     if (ability.cooldown > ability.abilityCooldownTime)
        //     {
        //         ability.abilityReady = true;
        //     }
        // }
    }

    public IEnumerator MoveWeaponTo(Vector3 pos, Vector3 rot){
        yield return new WaitForSeconds(0.1f);
        Weapon.transform.localPosition = pos;
        Weapon.transform.localRotation = Quaternion.Euler(rot);
    }

    public IEnumerator MoveWeaponBackTo(float delay, Vector3 pos, Vector3 rot){
        yield return new WaitForSeconds(delay);
        Weapon.transform.localPosition = pos;
        Weapon.transform.localRotation = Quaternion.Euler(rot);
    }

    public IEnumerator NormalShoot()
    {
        if (Shooting) yield break;

        Shooting = true;
        Vector3 noAngle = GunBarrle.forward;
        noAngle += new Vector3(Random.Range(-currentAccuracy, currentAccuracy), Random.Range(-currentAccuracy, currentAccuracy), Random.Range(-currentAccuracy, currentAccuracy));
        Attributes EnemyHit = ShootGun(noAngle, (int)playerControler.team);

        if (EnemyHit != null){}
        else { /*ScoreTracker.BrakeCombo();*/ }

        yield return new WaitForSeconds(GunPrefs.shotDelay);

        Shooting = false;
    }

    public void FootL(){}

    public void FootR(){}
}

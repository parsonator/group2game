﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerControler_Simon : Controler
{
    public enum PlayerClass{
        SOLDIER,
        MEDIC,
        PALADIN
    }
    [HideInInspector] public int connectionID = 0;
    public delegate void Event();
    public Event onDisconect;
    public Event onDeath;
    public static PlayerControler_Simon main;
    public PlayerClass playerClass = PlayerClass.SOLDIER;
    public Attributes attributes;
    public PlayerCombat combat;
    public PlayerUI playerUI;
    public HitBoxManager hitBoxManager;
    public float speed = 5;
    public float rotationSpeed = 5;
    public bool blockControl = false;
    public bool networked = false;
    public Team team;
    public Animator animator;
    public int secondsToRevive = 8;
    public GameObject CMCamera;
    private float LastFrameSpeed;
    private bool canStop = true;
    

    public bool livePlayer{
        get{
            return (connectionID == Client.ourClientID);
        }
    }

    private bool alive = true;

    public void Init( Team team, int connectionID){
        if(connectionID != -1){
            this.connectionID = connectionID;
        }

        this.team = team;
        print(connectionID);

        if (livePlayer){
            hitBoxManager.setLayer(8);
            main = this;
        }else{
            switch (team){
                case Team.BLUE:
                    hitBoxManager.setLayer(13);
                    break;
                case Team.RED:
                    hitBoxManager.setLayer(14);
                    break;
            }
        }
    }

    void Awake(){
        // main = this;
        attributes = GetComponent<Attributes>();
        combat = GetComponent<PlayerCombat>();
        playerUI = GetComponent<PlayerUI>();
        hitBoxManager = GetComponentInChildren<HitBoxManager>();
    }

    void Update(){
        applyMovement();

        if (attributes.CheckDead() && alive){
            alive = false;
            Die();
        }
    }

    private void applyMovement(){
        if (blockControl == false){
            //We only want to update if we are the owner of the character or if the game is not networked
            if (!networked || connectionID == Client.ourClientID){
                Vector3 MoveDirection = Vector3.zero;
                float axisY = PlayerInput.MoveAxis().y;
                Quaternion CameraRotation = Quaternion.Euler(0, CMCamera.transform.eulerAngles.y, 0);
                Vector3 CameraForward = CameraRotation * Vector3.forward;
                //MoveDirection += transform.forward * axisY;
                MoveDirection += CameraForward * axisY;
                MoveDirection += transform.right * PlayerInput.MoveAxis().x;

                if (PlayerInput.SprintButton() && axisY >= 0 && !PlayerInput.shootingButton()){
                    animator.SetBool("Sprint", true);
                    MoveDirection *= 2;
                   
                } else animator.SetBool("Sprint", false);

                animator.SetFloat("XVol", PlayerInput.MoveAxis().x);
                animator.SetFloat("YVol", axisY);

                float s = speed * MoveDirection.magnitude;
                if(s > 0 && LastFrameSpeed != 0 || PlayerInput.shootingButton())
                    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, CMCamera.transform.eulerAngles.y, transform.eulerAngles.z);
                else if(LastFrameSpeed == 0 && s > 0){
                    CMCamera.transform.parent = null;
                    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, CMCamera.transform.eulerAngles.y, transform.eulerAngles.z);
                    CMCamera.transform.parent = transform;
                }

                RaycastHit info;
                if(!animator.GetCurrentAnimatorStateInfo(0).IsName("RunToStop") && canStop){
                    
                    //Debug.DrawRay(transform.position + new Vector3(-0.2f, 1.58f/2, 0), transform.forward * 7, Color.red, 0.1f, false);
                    
                    if(Physics.Raycast(transform.position + new Vector3(0, 1.58f/2, 0), transform.forward * 7, out info, 7) && MoveDirection.magnitude == 2){
                        Debug.Log("Stop");
                        if(Vector3.Distance(transform.position + new Vector3(0, 1.58f/2, 0), info.point) > 6.9f){
                            animator.SetTrigger("Stop");
                            canStop = false;
                            StartCoroutine(ResetCanStop());
                        }
                        
                    }
                }
                Debug.DrawRay(transform.position + new Vector3(0, 0.1f, 0), -Vector3.up * 3f, Color.magenta, 0.1f, false);
                if(Physics.Raycast(transform.position + new Vector3(0, 0.1f, 0), -Vector3.up * 3f, out info, 3f)){
                    if(info.transform.tag == "world"){
                        animator.SetBool("MidAir", false);
                    }
                }else{
                    animator.SetBool("MidAir", true);
                    Debug.Log("Nothing underneath");
                }

                LastFrameSpeed = s;

                MoveCharacter(MoveDirection.normalized * s * Time.deltaTime);
            }
        }

        if (blockControl == true)
            gameObject.GetComponent<PlayerCombat>().enabled = false;

        applyGravity();
    }

    private IEnumerator ResetState(string Name){
        yield return new WaitForSeconds(0.2f);
        animator.SetBool(Name, false);
    }

    private IEnumerator ResetCanStop(){
        yield return new WaitForSeconds(0.5f);
        canStop = true;
    }

    private IEnumerator RespawnCountDown(){
        while (secondsToRevive > 0){
            yield return new WaitForSeconds(1);
            secondsToRevive--;
        }

        secondsToRevive = 8;
        Revive(Client.ourClientID);
    }

    public void Revive(int cnnID = 0, bool replicate = true){
        if (replicate && networked)
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Revive"), Client.reliableChannel);

        if (connectionID == Client.ourClientID){
            this.enabled = true;
            combat.enabled = true;
            attributes.enabled = true;
            playerUI.enabled = true;

            attributes.Revive();
            Client.UpdateSpawnCount(Client.ourPlayerTeam);
        } else gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(true);
    
        if (Client.players[cnnID].team == Team.BLUE)
            gameObject.transform.position = Client.spawnPointsBlue[Client.spawnPointIndexBlue].transform.position;
        else if (Client.players[cnnID].team == Team.RED)
            gameObject.transform.position = Client.spawnPointsRed[Client.spawnPointIndexRed].transform.position;

        gameObject.transform.Find("MainCharacterMesh").gameObject.SetActive(true);
        alive = true;
    }

    public void Die(bool replicate = true){
        if(onDeath != null)
            onDeath();
            
        if (replicate && networked)
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.RUNFUNCNOPARAMS, "REPLICATE_Die"), Client.reliableChannel);

        gameObject.transform.Find("MainCharacterMesh").gameObject.SetActive(false);
        if (connectionID == Client.ourClientID){

            this.enabled = false;
            combat.enabled = false;
            attributes.enabled = false;
            playerUI.enabled = false;

        }else gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(false);

        StartCoroutine("RespawnCountDown");
    } 
}

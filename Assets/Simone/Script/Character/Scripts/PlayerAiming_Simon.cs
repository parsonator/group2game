﻿using UnityEngine;
using System.Collections;
using Cinemachine;
public class PlayerAiming_Simon : MonoBehaviour
{
    static public Vector3 CroshairHit;
    public LayerMask Mask;
    public MouseOrbitImproved_Simon mouseOrbitImproved;
    public float ZoomSpeed = 10;
    public float ZoomFOV = 30;
    public float StartFOV;
    public float zoomLookDampening = 0.5f;
    
    public bool ZoomedIn = false;
    public Camera Cam;
    CinemachineVirtualCamera cinemachineVirtualCamera;

    // Use this for initialization
    void Start()
    {
        mouseOrbitImproved = GetComponent<MouseOrbitImproved_Simon>();
        if (Cam == null)
            Cam = Camera.main;

        if (StartFOV != 0)
            Cam.fieldOfView = StartFOV;

        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update(){
        RaycastHit hit;
        Ray ray = Cam.ScreenPointToRay(new Vector3(Cam.pixelWidth / 2, Cam.pixelHeight / 2, Cam.nearClipPlane));
        Debug.DrawRay(ray.origin, ray.direction * 1000, Color.yellow);
        if (Physics.Raycast(ray, out hit, 500, Mask))
            CroshairHit = hit.point;
        else
            CroshairHit = ray.GetPoint(500);

        // Aiming
        if (PlayerInput.ZoomButtonDown())
            ZoomedIn = !ZoomedIn;
        if (ZoomedIn){
            mouseOrbitImproved.mouseDampening = zoomLookDampening;
            cinemachineVirtualCamera.m_Lens.FieldOfView = Mathf.MoveTowards(Cam.fieldOfView, ZoomFOV, ZoomSpeed * Time.deltaTime);
        }else{
            mouseOrbitImproved.mouseDampening = 1f;
            cinemachineVirtualCamera.m_Lens.FieldOfView = Mathf.MoveTowards(Cam.fieldOfView, StartFOV, ZoomSpeed * Time.deltaTime);
        }
    }
}

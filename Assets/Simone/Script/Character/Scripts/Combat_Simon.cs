﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Combat_Simon : MonoBehaviour
{
    [HideInInspector]
    public PlayerControler_Simon playerControler;

    public bool Shooting, reloading;
    public Transform GunBarrle;
    public GameObject Weapon;

    public float currentAmmo = 6;

    public Gun_SO GunPrefs;
    protected float currentAccuracy;

    ParticleSystem shootFX, HitFX, entityHit;
    LineRenderer shotLine;
    Light MussleLight;

      void Start()
    {

        ShootSetup();


        if (GunPrefs.CanShoot)
        {
            ShootSetup();
        }

        currentAmmo = GunPrefs.maxAmmo;

    }

    public IEnumerator Reload()
    {
        if (reloading)
            yield break;

        reloading = true;

        yield return new WaitForSeconds(GunPrefs.reloadTime);
        currentAmmo = GunPrefs.maxAmmo;
        reloading = false;

    }

    public IEnumerator SlowReload()
    {

        while (currentAmmo != GunPrefs.maxAmmo)
        {
            reloading = true;

            currentAmmo++;
            yield return new WaitForSeconds(GunPrefs.reloadTime / GunPrefs.maxAmmo);

        }
        reloading = false;
    }
    public class ShotData
    {
        public Vector3 noAngle;
        public int team;
    }



    public Attributes ShootGun(Vector3 accuracy, int team, bool replicate = true)
    {
        // print(string.Format("{0},{1}",(Team)team));
        LayerMask Mask;
        float range = 20;
        RaycastHit hit;
        Attributes attributes = null;

        if (replicate && playerControler.networked)
        {
            Client.Send(NetworkParser.ParseIn(NetworkMsgType.SHOOT, new string[]{
             accuracy.x.ToString(),
             accuracy.y.ToString(),
             accuracy.z.ToString(),
             team.ToString(),
            } ), Client.reliableChannel);

            Mask = GlobleLayers.DetectEneimes;
        }
        else Mask = GlobleLayers.DetectPlayer;
        GunPrefs.CanShoot = true;

        if (shootFX == null)
            ShootSetup();
        shootFX.transform.position = GunBarrle.position;
        shootFX.transform.rotation = Quaternion.LookRotation(GunBarrle.forward);
        shootFX.Play();
        currentAmmo--;


        Vector3 latsHit = Vector3.zero;
        ParticleSystem hitEffect = HitFX;
        if (Physics.Raycast(GunBarrle.position, accuracy, out hit, GunPrefs.range, Mask))
        {
            Debug.DrawLine(hit.point, Camera.main.transform.position);
            int hitLayer = hit.transform.gameObject.layer;
            int teamLayer = (team == 0) ? 14 : 13;
            int enemyLayer = (team == 0) ? 13 : 14;
            // print(string.Format("team {1} hit {0}, vs {2}", hitLayer, enemyLayer, teamLayer));

            // if (IsInLayerMask(hit.transform.gameObject.layer, GlobleLayers.DetectEneimes))
            if (hitLayer == 8)//hits player
            {
                latsHit = hit.point;
                if (attributes = hit.collider.gameObject.GetComponentInParent<Attributes>())
                {
                    if (!attributes.IsDead())
                    {
                        float damage = GunPrefs.damage;
                        switch (hit.collider.gameObject.tag)
                        {
                            case "HBlow":
                                damage *= 0.5f;
                                break;
                            case "HBhigh":
                                damage *= 2f;
                                break;
                        }
                        if ((int)attributes.GetComponent<PlayerControler>().team != team)
                        {
                            attributes.Damage(damage);
                            if (attributes.CheckDead())
                            {
                                onKill(attributes);
                            }
                            hit.collider.gameObject.SendMessageUpwards("hitMark", transform.position, SendMessageOptions.DontRequireReceiver);
                        }
                    }
                    hitEffect = entityHit;
                }
            }
            else if (hitLayer == teamLayer || hitLayer == enemyLayer)
            {
                latsHit = hit.point;
                hitEffect = entityHit;
            }
            else
            {
                latsHit = hit.point;
                hitEffect = HitFX;
            }
        }
        if (latsHit != Vector3.zero)
        {
            StartCoroutine(shootEffect(GunBarrle, latsHit));
            hitEffect.gameObject.transform.position = latsHit;
            hitEffect.Play();
        }
        else
        {
            StartCoroutine(shootEffect(GunBarrle, GunBarrle.position + (accuracy * range)));
        }
        return attributes;
    }


    void onKill(Attributes killed)
    {

        //send Analytics                            
        // PlayerAnalytics.OnDeath(killed.GetComponent<PlayerControler>().playerClass.ToString(),
            // playerControler.playerClass.ToString(), GunPrefs.name);

        PlayerAnalytics.GunKills(GunPrefs.name);

        print("kill");
        //send to server                            
        Client.Send(NetworkParser.ParseIn(NetworkMsgType.GAMEMODE, new string[]{
                                ((int)GetComponent<PlayerControler>().team).ToString(),
                                "0",
                                "1"
                            }), Client.reliableChannel);
    }

    IEnumerator shootEffect(Transform Barrle, Vector3 hitPoint)

    {
        bool Effect = true;
        shotLine.useWorldSpace = true;

        Vector3 hit = hitPoint;
        Vector3 barrle = Barrle.position;
        // MussleLight.enabled = true;

        while (Effect)
        {
            shotLine.SetPosition(0, barrle);
            shotLine.SetPosition(1, hit);
            yield return new WaitForSeconds(0.001f);
            Effect = false;

        }
        shotLine.useWorldSpace = false;
        // MussleLight.enabled = false;

        shotLine.SetPosition(0, transform.InverseTransformPoint(barrle));
        shotLine.SetPosition(1, transform.InverseTransformPoint(barrle));
    }

    void ShootSetup()
    {
        GameObject Particles;
        Particles = Instantiate(GunPrefs.Shoot, transform.position, Quaternion.identity) as GameObject;
        shootFX = Particles.GetComponent<ParticleSystem>();
        shootFX.transform.parent = transform;
        shotLine = shootFX.gameObject.GetComponent<LineRenderer>();

        // MussleLight = shootFX.GetComponent<Light>();
        // MussleLight.enabled = false;
        Particles = Instantiate(GunPrefs.Hit, transform.position, Quaternion.identity) as GameObject;
        HitFX = Particles.GetComponent<ParticleSystem>();
        HitFX.transform.parent = transform;

        Particles = Instantiate(GunPrefs.eHit, transform.position, Quaternion.identity) as GameObject;
        entityHit = Particles.GetComponent<ParticleSystem>();
        entityHit.transform.parent = transform;

    }

    public void AOEDammage(Vector3 poz, float rad, float damage)
    {
        List<Attributes> hits = new List<Attributes>();
        Collider[] c = Physics.OverlapSphere(poz, rad);

        for (int i = 0; i < c.Length; i++)
        {

            Attributes a;
            if (a = c[i].GetComponentInParent<Attributes>())
            {
                if (!hits.Contains(a))
                {
                    hits.Add(a);
                    print(c[i].gameObject.name);
                }
            }
        }

        if (hits.Count != 0)
        {
            foreach (Attributes a in hits)
            {
                a.Damage(damage);
            }
        }
        // hit.collider.gameObject.SendMessageUpwards("Damage", damage, SendMessageOptions.DontRequireReceiver);
    }

    public static bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));

    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLocationBase : MonoBehaviour {

	public float updateRate = 0.5f;
	public float positionLerpVelocity = 30f;
	public float rotationLerpVelocity = 5f;
	public bool networkLocation = true;
	public bool networkRotation = true;

	private float pCurrentLerpTime = 0f;
	private float pLerpTime = 0.1f;

	private float rCurrentLerpTime = 0f;
	private float rLerpTime = 2f;

	private Vector3 lastPosition = Vector3.zero;
	private Quaternion lastRotation = Quaternion.identity;

	[HideInInspector] public Vector3 newPosition = Vector3.zero;
	[HideInInspector] public Quaternion newRotation = Quaternion.identity;

	protected void Interpolate(){
		//If the position has changed let's reset the interpolation time and start over
		//The last position is now the new position
		if(lastPosition != newPosition){
			pCurrentLerpTime = 0f;
			lastPosition = newPosition;
		}

		//If the rotation has changed let's reset the interpolation time and start over
		//The last rotation is now the new rotation
		if(lastRotation != newRotation){
			rCurrentLerpTime = 0f;
			lastRotation = newRotation;
		}

		//Increase the interpolation time to compute the correct percentage for position
		pCurrentLerpTime += Time.deltaTime;
		if(pCurrentLerpTime > pLerpTime)
			pCurrentLerpTime = pLerpTime;
		
		//Increase the interpolation time to compute the correct percentage for rotation
		rCurrentLerpTime += Time.deltaTime * rotationLerpVelocity;
		if(rCurrentLerpTime > rLerpTime)
			rCurrentLerpTime = rLerpTime;

		//Apply interpolation
		if(networkLocation)
			LerpToPosition();
		if(networkRotation)
			LerpToRotation();
	}

	protected void LerpToPosition(){
		float perc = pCurrentLerpTime / pLerpTime;
		transform.position = Vector3.Lerp(transform.position, newPosition, perc);
	}

	protected void LerpToRotation(){
		float perc = rCurrentLerpTime / rLerpTime;
		transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, perc);
	}
}

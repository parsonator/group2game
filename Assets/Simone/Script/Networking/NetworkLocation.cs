﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLocation : NetworkLocationBase {

	private PlayerControler controller;
	private float currentTime;

	void Start () {
		controller = GetComponentInParent<PlayerControler>();
		if(controller == null) controller = GetComponent<PlayerControler>();
		if(controller == null) controller = GetComponentInChildren<PlayerControler>();
		currentTime = Time.time;
	}

	void Update () {
		//update my location only if I'm the owner of this game object
		if(controller.networked){
			if(controller.connectionID == Client.ourClientID && !GetComponentInParent<Attributes>().IsDead()){
				//if previous time plus the update rate is less than the current time we update
				if(currentTime + updateRate < Time.time){
					Client.Send(NetworkParser.ParseIn(NetworkMsgType.TRANSFORM, transform.position, transform.rotation, this.name), Client.unreliableChannel);
					currentTime = Time.time;
				}	
			}			
		}

		//Lerp rotation and position only if I'm not the owner of this gameobject
		if(controller.networked && controller.connectionID != Client.ourClientID){
			Interpolate();
		}
	}
}

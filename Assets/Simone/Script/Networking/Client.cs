﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;
using System.Linq;

public class Player
{
	// public string playerName { set; get; }
	public GameObject avatar { set; get; }
	public int connectionID { set; get; }
	public Team team;

	public PlayerProfile playerProfile = new PlayerProfile();
}

public class Client : MonoBehaviour {

	#region PublicVarsHidden
	[HideInInspector] public static int ourClientID;
	[HideInInspector] public static int reliableChannel;
	[HideInInspector] public static int unreliableChannel;
	[HideInInspector] public static Dictionary<int, Player> players = new Dictionary<int, Player>();
	[HideInInspector] public static CaptureFlag flag;
	[HideInInspector] public static Team ourPlayerTeam;
	[HideInInspector] public static Match Match = new TDM();
	[HideInInspector] public static List<SpawnPoint> spawnPointsRed = new List<SpawnPoint>();
	[HideInInspector] public static List<SpawnPoint> spawnPointsBlue = new List<SpawnPoint>();
	[HideInInspector] public static int spawnPointIndexBlue = 0;
	[HideInInspector] public static int spawnPointIndexRed = 0;
	#endregion

	#region PublicVars
	public GameObject playerPrefab;
	public GameObject AIPrefab;
	public Texture2D redTeamIcon;
	public Texture2D redTeamTag;
	public Material redTeamMaterial; 
	public Image playerIconRed;
	public bool useDefaultServerAddress = false;
	#endregion

	#region PrivateVars
	private Sprite redTeamIconSprite;
	private Sprite redTeamTagSprite;
	private GameObject AIPlayer;	
	private const int MAX_CONNECTION = 100;
	public int port = 5701;
	public string IPaddress = "127.0.0.1";
	private static int hostID;
	private static int connectionID;
	private float connectionTime;
	private bool isConnected = false;
	private bool isStarted = false;
	private static byte error;
	private PlayerProfile playerProfile;
	#endregion

	void Start(){
		//We'll need to use the sprites more than once therefore we create them at start
		redTeamIconSprite = Sprite.Create(redTeamIcon, new Rect(0, 0, redTeamIcon.width, redTeamIcon.height), new Vector2(0.5f, 0.5f));
		redTeamTagSprite = Sprite.Create(redTeamTag, new Rect(0, 0, redTeamTag.width, redTeamTag.height), new Vector2(0.5f, 0.5f));

		SpawnPoint[] spawnPoints;
		spawnPoints = UnityEngine.Object.FindObjectsOfType<SpawnPoint>();
		foreach (SpawnPoint sp in spawnPoints)
		{
			if (sp.team == Team.BLUE)
				spawnPointsBlue.Add(sp);
			else if (sp.team == Team.RED)
				spawnPointsRed.Add(sp);
		}
	}

	public void Connect(string IPAddress, int port, PlayerProfile playerProfile)
	{
		//If we don't use the default address then assign the address
		if (!useDefaultServerAddress)
			IPaddress = IPAddress;
		//Assign the port
		this.port = port;
		//If we got a name assign it to the global variable
		this.playerProfile = playerProfile;
		//Initialise network and create the configuration
		NetworkTransport.Init();
		ConnectionConfig cc = new ConnectionConfig();
		//Create the channels to use for sending data
		reliableChannel = cc.AddChannel(QosType.Reliable);
		unreliableChannel = cc.AddChannel(QosType.Unreliable);
		//We need a topology using the channel and the max connections
		HostTopology topo = new HostTopology(cc, MAX_CONNECTION);
		//The host ID is given by add host which actually just open a port
		hostID = NetworkTransport.AddHost(topo, 0);
		//The connection ID is automatically generated
		connectionID = NetworkTransport.Connect(hostID, IPaddress, port, 0, out error);
		//Save connection time for future calculations and say that we are connected
		connectionTime = Time.time;
		isConnected = true;
	}

	void Update()
	{
		//If we are not connected don't update
		if (!isConnected)
			return;
		//Data to read using the Receive function
		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;
		//Read received data
		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		do
		{

			//switch on the type of data received
			switch (recData)
			{
				case NetworkEventType.Nothing:
					break;
				case NetworkEventType.DataEvent:
					ProcessReceiveData(recBuffer, dataSize);
					break;
				default:
					break;
			}
			recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);

		} while (recData != NetworkEventType.Nothing);
	}

	private void ProcessReceiveData(byte[] recBuffer, int dataSize)
	{
		string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
		string[] splitData = msg.Split('|');
		int cnnID = 0;
		switch (NetworkParser.GetMessageType(msg))
		{
			case NetworkMsgType.ASKNAME:
				OnAskName(NetworkParser.ParseArrayString(msg));
				break;
			case NetworkMsgType.CNN:
				string[] d = msg.Split('|');
				cnnID = int.Parse(d[1]);
				string playerName = d[2];
				PlayerAbilityClass playerAbilityClass = (PlayerAbilityClass)System.Enum.Parse(typeof(PlayerAbilityClass), d[3], true);
				Team team = (Team)System.Enum.Parse(typeof(Team), d[4], true);
				string gunName = d[5];
				SpawnPlayer(cnnID,playerName,playerAbilityClass,team,gunName);
				break;
			case NetworkMsgType.DC:
				PlayerDisconnected(int.Parse(splitData[1]));
				break;
			case NetworkMsgType.TRANSFORM:
				OnReceiveTransform(msg);
				break;
			case NetworkMsgType.GAMEMODE:
				UpdateClientGameMode(msg);
				break;
			case NetworkMsgType.GAMEOVER:
				EndGame(msg);
				break;
			case NetworkMsgType.RESTARTGAME:
				RestartGame();
				break;
			case NetworkMsgType.SHOOT:
				ShootingRelay(msg);
				break;
			case NetworkMsgType.RUNFUNCNOPARAMS:
				bool AI = false;
				if(splitData.Length < 3){
					AI = true;
					cnnID = -1;
				}else cnnID = int.Parse(splitData[2]);

				RunFunction(splitData[1], cnnID, AI);
				break;
			case NetworkMsgType.UPDATEANIMATION:
				string[] data = NetworkParser.ParseArrayString(msg);
				UpdateAnimator(data[0], data[1], data[2], int.Parse(data[3]));
				break;
			case NetworkMsgType.SPAWNMINION:
				SpawnMinion(cnnID,msg);
				break;
			case NetworkMsgType.SPAWNAI:
				SpawnAI(msg);
				break;
			case NetworkMsgType.AIDIE:
				KillAI(msg);
				break;
			case NetworkMsgType.HEALTH:
				UpdateHealth(msg);
				break;
			case NetworkMsgType.SENDDAMAGE:
				ApplyDamage(msg);
				break;
			case NetworkMsgType.MINIONDIE:
				KillMinion(msg);
				break;
			case NetworkMsgType.SPAWNALLMINIONS:
				SpawnAllMinions(msg);
				break;
			default:
				Debug.Log("Invalid message: " + msg);
				break;
		}
	}

	private void OnAskName(string[] data)
	{
		//Get client id
		ourClientID = int.Parse(data[0]);
		//Send our name back to the server
		Send(NetworkParser.ParseIn(NetworkMsgType.NAMEIS, new string[] {
			playerProfile.playerName, 
			playerProfile.PlayerAbilityClass.ToString(), 
			playerProfile.gunType}), 
			reliableChannel);

		//Need to create all the other players
		for (int i = 1; i < data.Length - 1; i++)
		{
			string[] d = data[i].Split('%');
			int cnnID = int.Parse(d[0]);
			string playerName = d[1];
			PlayerAbilityClass playerAbilityClass = (PlayerAbilityClass)System.Enum.Parse(typeof(PlayerAbilityClass), d[2], true);
			Team team = (Team)System.Enum.Parse(typeof(Team), d[4], true);
			string gunName = d[3];
			SpawnPlayer(cnnID,playerName,playerAbilityClass,team,gunName);
		}
	}

	private void OnReceivePosition(string msg){
		//Get the connectionID for the player to update
		string[] splitData = msg.Split('|');
		int cnnID = int.Parse(splitData[2]);
		//Reconstruct vector3 so that we can parse it using the NetworkParser
		string parseVector = splitData[0] + "|" + splitData[1];
		Vector3 playerVector = NetworkParser.ParseVector(parseVector);
		//Using the cnnID as the key we can get the right player and set their location
		players[cnnID].avatar.transform.position = playerVector;
	}

	private void OnReceiveTransform(string msg){
		//Get the connectionID for the player to update
		string[] splitData = msg.Split('|');
		string gameObjectName = splitData[3];
		int cnnID = 4 < splitData.Length ? int.Parse(splitData[4]) : -1;

		//Reconstruct transform so that we can parse it using the NetworkParser
		string parseVector = splitData[0] + "|" + splitData[1] + "|" + splitData[2];
		Vector3 position;
		Quaternion rotation;
		string objName;
		NetworkParser.ParseTransform(parseVector, out position, out rotation, out objName);
		//Using the cnnID as the key we can get the right player and set their location and rotation
		
		if ((cnnID != -1 && players.ContainsKey(cnnID)) && players [cnnID].avatar.name.Contains (gameObjectName)) {
			players [cnnID].avatar.GetComponent<NetworkLocationBase> ().newPosition = position;
			players [cnnID].avatar.GetComponent<NetworkLocationBase> ().newRotation = rotation;
		} else if (cnnID != -1 && players.ContainsKey(cnnID)) {
			GameObject player = Helper.FindInChildren (players[cnnID].avatar, gameObjectName);
			if(player)
			{
				player.GetComponent<NetworkLocation> ().newPosition = position;
				player.GetComponent<NetworkLocation> ().newRotation = rotation;
			}
		} else if (cnnID == -1) {
			GameObject obj = GameObject.Find (gameObjectName);
			if (obj != null) {
				obj.GetComponent<NetworkLocationBase> ().newPosition = position;
				obj.GetComponent<NetworkLocationBase> ().newRotation = rotation;
			}
		}
	}

	private void SpawnMinion(int ID,string msg){
		
        //Get the connectionID for the player to update
		string[] splitData = msg.Split('|');
		string gameObjectName = splitData[3];
		int cnnID = 4 < splitData.Length ? int.Parse(splitData[4]) : -1;
		//Reconstruct transform so that we can parse it using the NetworkParser
		string parseVector = splitData[0] + "|" + splitData[1] + "|" + splitData[2];
		Vector3 position;
		Quaternion rotation;
		string objName;
		NetworkParser.ParseTransform(msg, out position, out rotation, out objName);
		players[cnnID].avatar.GetComponent<PlayerMinion>().SpawnFinalMinion (true, position, rotation,players[cnnID].team);
	}

	void SpawnAllMinions(string msg)
	{
		string[] connIDs = NetworkParser.ParseArrayString(msg);
		for(int i = 0; i < connIDs.Length; i++)
		{
			GameObject player = players[int.Parse(connIDs[i])].avatar;
			player.GetComponent<PlayerMinion>().SpawnOtherClientMinion(int.Parse(connIDs[i]), player.GetComponent<PlayerControler>(), players[int.Parse(connIDs[i])].team);
		}	
	}

	private void UpdateClientGameMode(string msg)
	{
		string[] splitData = msg.Split('|');
		string RedKills = splitData[1];
		string RedScore = splitData[2];

		string BlueKills = splitData[3];
		string BlueScore = splitData[4];

		Match.RedTeam = new Match.MatchTeam((int)Team.RED,
			int.Parse(RedKills),
			int.Parse(RedScore));
		Match.BlueTeam = new Match.MatchTeam((int)Team.BLUE,
			int.Parse(BlueKills),
			int.Parse(BlueScore));
	}

	private void EndGame(string msg){
		string[] splitData = msg.Split('|');
		string WiningTeam = splitData[1];
		WiningScreen w = (WiningScreen)CanvasManager.GetScreen("WiningScreen");
		w.ShowEndScreen((Team)System.Enum.Parse(typeof(Team), WiningTeam));
	}

	private void ShootingRelay(string msg)
	{

		string[] splitData = msg.Split('|');
		Vector3 accuracy = new Vector3(
			float.Parse(splitData[1]),
			float.Parse(splitData[2]),
			float.Parse(splitData[3])
		);
		int team = int.Parse(splitData[4]);
		int cnnID = int.Parse(splitData[6]);
		CanvasManager.main.setActiveScreen(CanvasManager.GetScreen("HudManager"));
		GameObject player = players[cnnID].avatar;
		PlayerCombat playerCombat = player.GetComponent<PlayerCombat>();
		playerCombat.ShootGun(accuracy, team, false);
	}

	public void RestartGame()
	{
		foreach (KeyValuePair<int, Player> player in players)
		{
			PlayerControler playerControler = player.Value.avatar.GetComponent<PlayerControler>();
			playerControler.Die();
		}
		CanvasManager.main.setActiveScreen(CanvasManager.GetScreen("HudManager"));
	}

	private void SpawnPlayer(int cnnID,string playerName,PlayerAbilityClass playerAbilityClass,Team team, string gunName)
	{
		GameObject go;
		PlayerControler playerControler;
		if (team == Team.BLUE)
		{
			go = Instantiate(playerPrefab, spawnPointsBlue[spawnPointIndexBlue].transform.position, Quaternion.identity) as GameObject;
		}
		else
		{
			go = Instantiate(playerPrefab, spawnPointsRed[spawnPointIndexRed].transform.position, Quaternion.identity) as GameObject;
		}
		playerControler = go.GetComponent<PlayerControler>();
		Gun_SO Gun = playerControler.combat.allGuns.Find(x => x.name == gunName);

		UpdateSpawnCount(ourPlayerTeam);

		Player p = new Player();
		p.avatar = go;

		p.playerProfile.playerName = playerName;
		p.playerProfile.PlayerAbilityClass = playerAbilityClass;
		p.playerProfile.gunType = Gun.name;

		p.connectionID = cnnID;
		p.avatar.GetComponentInChildren<Text>().text = playerName;
		p.team = team;
		players.Add(cnnID, p);

		//If the spawned player is us blend between main menu and player HUD then disable the name tag and initialise the player UI
		if (cnnID == ourClientID)
		{
			ourPlayerTeam = team;
			GameObject playerHUD = GameObject.Find("PlayerHUD");
			GameObject.Find("MainMenu").GetComponent<CanvasGroup>().alpha = 0;
			playerHUD.GetComponent<CanvasGroup>().alpha = 1;

			if (team == Team.RED)
			{ //If we are red change icon on HUD
				GameObject.Find("PlayerIcon").GetComponent<Image>().sprite = redTeamIconSprite;
				playerHUD.FindInChildren("StaminaBar").GetComponent<Image>().color = Color.red;
				SkinnedMeshRenderer[] mesh = go.GetComponentsInChildren<SkinnedMeshRenderer>();
				mesh[0].material = redTeamMaterial;
				mesh[1].material = redTeamMaterial;
				go.GetComponent<MakeMapObject>().image = playerIconRed;
			}

			go.gameObject.transform.Find("NameTagCanvas").gameObject.SetActive(false);
			playerControler.Init(ourPlayerTeam, Gun, playerAbilityClass, ourClientID);
			go.GetComponentInChildren<PlayerUI>().Init();
			MiniMapController miniMap = GameObject.FindObjectOfType<MiniMapController>();
			if (miniMap != null){ miniMap.playerPos = go.transform; miniMap.mapCamera = go.GetComponentInChildren<Camera>(); }
			isStarted = true;
		}
		else
		{ //if the player spanwed is not us then disable all the components that do not need to be controlled
			if (team == Team.RED)
			{ //If we are red change name tag icon, mesh colour and minimap icon colour
				Image[] tagSprite = go.GetComponentsInChildren<Image>(true);
				foreach (Image img in tagSprite)
				{
					if (img.gameObject.name == "NameTag")
						img.sprite = redTeamTagSprite;
				}
				SkinnedMeshRenderer[] mesh = go.GetComponentsInChildren<SkinnedMeshRenderer>();
				mesh[0].material = redTeamMaterial;
				mesh[1].material = redTeamMaterial;
				go.GetComponent<MakeMapObject>().image = playerIconRed;
			}

			playerControler.Init(ourPlayerTeam, Gun, playerAbilityClass, cnnID);
			go.GetComponentInChildren<Camera>().gameObject.SetActive(false);
			go.GetComponentInChildren<PlayerUI>().enabled = false;
			go.GetComponentInChildren<Cinemachine.CinemachineVirtualCamera>().enabled = false;
			go.GetComponentInChildren<MouseOrbitImproved>().enabled = false;
			go.GetComponentInChildren<PlayerCombat>().enabled = false;
			go.GetComponentInChildren<PlayerAiming>().enabled = false;
		}
	}

	private void UpdateHealth(string msg)
	{
		string[] splitData = msg.Split('|');
		int cnnID = int.Parse(splitData[2]);
		float health = float.Parse(splitData[1]);
		if(players.ContainsKey(cnnID) && players[cnnID].avatar.FindInChildren("NameTagHealthBar") != null)
			players[cnnID].avatar.FindInChildren("NameTagHealthBar").GetComponent<Image>().fillAmount = health/100;
	}

	private void ApplyDamage(string msg)
	{
		string[] splitData = msg.Split('|');
		players[ourClientID].avatar.GetComponent<Attributes>().Damage(int.Parse(splitData[1]));
		HudManager[] hudManagers = Resources.FindObjectsOfTypeAll<HudManager>();
		foreach(HudManager hud in hudManagers)
		{
			if(hud.isActiveAndEnabled)
				hud.ShowDamageMarker(players[int.Parse(splitData[2])].avatar.GetComponent<PlayerControler>().MyMinion.transform, players[ourClientID].avatar.transform);
		}
	}

	private void KillMinion(string msg)
	{
		string[] splitData = msg.Split('|');
		Destroy(players[int.Parse(splitData[2])].avatar.GetComponent<PlayerControler>().MyMinion);
	}

	private void SpawnAI(string msg){
		if(AIPlayer != null)
			return;

		Vector3 pos;
		Quaternion rot;
		string objName;
		NetworkParser.ParseTransform(msg, out pos, out rot, out objName);
		AIPlayer = Instantiate(AIPrefab, pos, rot);
		AIPlayer.name = objName;
	}

	private void KillAI(string msg){
		Destroy(AIPlayer);
	}

	private void PlayerDisconnected(int cnnID){
		if (players[cnnID].avatar.GetComponent<PlayerControler>().onDisconect != null)
			players[cnnID].avatar.GetComponent<PlayerControler>().onDisconect();
		Destroy(players[cnnID].avatar);
		players.Remove(cnnID);
	}

	private void RunFunction(string funcName, int cnnID, bool isAI){
		GameObject networkEntety;
		object[] objects = new object[]{};

		if(isAI){
			networkEntety = AIPlayer;
		}else{
			networkEntety = players[cnnID].avatar;
			PlayerCombat playerCombat = networkEntety.GetComponent<PlayerCombat>();
		}

		switch (funcName){
			case "REPLICATE_Revive":
				objects = new object[] { cnnID, false };
				break;
			default:
				break;
		}

		if(objects.Length == 0)
			networkEntety.GetComponent<ReplicateFunctions>().SendMessage(funcName, false, SendMessageOptions.DontRequireReceiver);	
		else
			networkEntety.GetComponent<ReplicateFunctions>().SendMessage(funcName, objects, SendMessageOptions.DontRequireReceiver);
	}

	private void UpdateAnimator(string property, string value, string dataType, int cnnID){
		GameObject go;
		if(cnnID == -1)
		{
			if(AIPlayer != null)
			{
				go = AIPlayer;
				go.GetComponentInChildren<NetworkAnimationAI>().UpdateAnimation(property, value, dataType);	
			}
		}
		else
		{
			go = players[cnnID].avatar;
			go.GetComponent<NetworkAnimations>().UpdateAnimation(property, value, dataType);
		}
	}

	public static void Send(string message, int channelID){
		byte[] msg = Encoding.Unicode.GetBytes(message);
		NetworkTransport.Send(hostID, connectionID, channelID, msg, message.Length * sizeof(char), out error);
	}

	public static void UpdateSpawnCount(Team team){
		if (team == Team.BLUE)
		{
			if (spawnPointIndexBlue == spawnPointsBlue.Count - 1)
			{
				spawnPointIndexBlue = 0;
			}
			else
			{
				spawnPointIndexBlue++;
			}
		}
		if (team == Team.RED)
		{
			if (spawnPointIndexRed == spawnPointsRed.Count - 1)
			{
				spawnPointIndexRed = 0;
			}
			else
			{
				spawnPointIndexRed++;
			}
		}
	}

}

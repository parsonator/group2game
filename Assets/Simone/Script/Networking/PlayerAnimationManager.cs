﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class PlayerAnimationManager : AnimationManager {

	#if UNITY_EDITOR
	void OnValidate(){
		//If we set the bool to true run the function and then set back to false
		//This emulates the behaviour of a button but without having to create a GUI
		if(UpdateValues)
			GetValues();
		
		UpdateValues = false;
	}

	void GetValues(){
		//We clear both lists, but we don't clean the bools we need to keep their values
		anims.Clear();
		valueTypes.Clear();

		//The following gets all the serialized objects from character animations
		//Then using their iterator we can loop through them
		CharacterAnimations obj = ScriptableObject.CreateInstance<CharacterAnimations>();
		SerializedObject serializedObject = new UnityEditor.SerializedObject(obj);
		SerializedProperty property = serializedObject.GetIterator();

		do{
			if(property.type == "Array"){ //if the current property is an array get in here
				try{ //need to catch eventual exceptions
					if(property.arraySize > 0){ 
						if(property.GetArrayElementAtIndex(0).type == "string"){ //if the array contains a string keep going
							//Get first and second value of array - look at character animations class below for understanding
							anims.Add(property.GetArrayElementAtIndex(0).stringValue);
							valueTypes.Add(property.GetArrayElementAtIndex(1).stringValue);
						}
					}
				}catch(Exception e){ //just catch a generic exception 
					
				}
			}
		}while (property.Next(true)); //keep going while there is a next property
		
		//if the animations fields from character animations are less than the bools
		//it means we deleted some states, we want their counts to match
		if(anims.Count < replicate.Count){
			List<bool> temp = new List<bool>();
			for(int i = 0; i < anims.Count; ++i){
				temp.Add(replicate[i]);
			}	
			replicate.Clear();
			replicate = temp;
		}

		//if we added a new state add a default replication value of false
		for(int i = 0; i <= anims.Count; ++i){
			if(replicate.Count < i){
				replicate.Add(false);
			}
		}
	}

#endif
}


#if UNITY_EDITOR
[System.Serializable] //simple serialized class that contains all the animations values and their data type
public class CharacterAnimations : ScriptableObject{
	public string[] XVOL = new string[] { "XVol", "float"};
	public string[] YVOL = new string[] { "YVol", "float"};
	public string[] FIRE = new string[] { "Sprint", "bool"};
}
#endif


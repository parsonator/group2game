﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkAnimations : MonoBehaviour {

	public float updateRate = 0.5f;

	private Animator anim;
	private AnimationManager animManager;
	
	private Dictionary<string, string> animations = new Dictionary<string, string>();
	private float currentTime;
	private int controllerConnectionID;
	private bool updatedLastFrame = false;
	private bool networked = false;
	private Dictionary<string, float> updatedValues = new Dictionary<string, float>();

	void Start () {		
		anim = GetComponentInChildren<Animator>();
		animManager = GetComponent<AnimationManager>();
		for (int i = 0; i < animManager.anims.Count; i++){
			if(animManager.replicate[i] == true)
				animations.Add(animManager.anims[i], animManager.valueTypes[i]);			
		}
		currentTime = Time.time;
		networked = GetComponent<PlayerControler>().networked;
		controllerConnectionID = GetComponent<PlayerControler>().connectionID;
	}
	
	void LateUpdate () {
		if(networked){
			if(controllerConnectionID == Client.ourClientID){
				if(currentTime + updateRate < Time.time ){
					foreach(KeyValuePair<string, string> entry in animations){
						switch (entry.Value) {
							case "float":
								float floatValue = anim.GetFloat(entry.Key);
								Client.Send(NetworkParser.ParseIn(NetworkMsgType.UPDATEANIMATION, new string[]{ entry.Key, floatValue.ToString(), "float" }), Client.unreliableChannel);
								break;
							case "bool":
								bool boolValue = anim.GetBool(entry.Key);
								Client.Send(NetworkParser.ParseIn(NetworkMsgType.UPDATEANIMATION, new string[]{ entry.Key, boolValue.ToString(), "bool" }), Client.unreliableChannel);
								break;
							default:
								break;
						}
					}
					currentTime = Time.time;
				}
			}
		}
		
		if(networked || controllerConnectionID != Client.ourClientID){
			if(updatedLastFrame){
				updatedLastFrame = false;
			}else{
				foreach(KeyValuePair<string, float> entry in updatedValues){
					anim.SetFloat(entry.Key, entry.Value);
				}
			}
		}
	}

	public void UpdateAnimation(string property, string value, string dataType){
		if(networked || controllerConnectionID != Client.ourClientID){
			if(anim == null) return;
			updatedLastFrame = true;
			switch (dataType){
				case "float":
					anim.SetFloat(property, float.Parse(value));
					if(updatedValues.ContainsKey(property))
						updatedValues[property] = float.Parse(value);
					else
						updatedValues.Add(property, float.Parse(value));
					break;
				case "bool":
					anim.SetBool(property, bool.Parse(value));
					break;
				default:
					break;
			}
		}
	}
}

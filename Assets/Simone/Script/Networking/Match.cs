﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public abstract class Match  {

	[System.Serializable]
	public class MatchTeam{
		public Team team;
		public int Kills;
		public int Score;

        public MatchTeam(int _team)
        {
			this.team = (Team)_team;
			this.Kills = 0;
			this.Score = 0;
        }

        public MatchTeam(int _team,int _kills, int _score)
        {
			this.team = (Team)_team;
			this.Kills += _kills;
			this.Score += _score;
        }

        public void UpdateMatch( int _kills, int _score)
        {
			this.Kills += _kills;
			this.Score += _score;
        }
        public void RestetMatch()
        {
			this.Kills = 0;
			this.Score = 0;
        }
    }


	public MatchTeam RedTeam = new MatchTeam((int)Team.RED);
	public MatchTeam BlueTeam= new MatchTeam((int)Team.BLUE);
	

	public delegate void OnGameEnd();
	public OnGameEnd onGameEnd;
	public abstract Team WinCondition();
	public abstract void UpdateMatch();



}

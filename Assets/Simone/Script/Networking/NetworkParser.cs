﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class NetworkMsgType
{
	public const string ASKNAME = "ASKNAME"; //Server ask client for their name
	public const string NAMEIS = "NAMEIS"; //Player answer to server for providing name
	public const string CN = "CN"; //Connect
	public const string DC = "DC"; //Disconnect
	public const string DIE = "DIE"; //Player dies
	public const string CNN = "CNN"; //Connection
	public const string POSITION = "POSITION"; //Position (Vector3)
	public const string ROTATION = "ROTATION"; //Rotation (Quaterions)|
	public const string TRANSFORM = "TRANSFORM"; //Transform for position and rotation
	public const string HEALTH = "HEALTH"; //The player health that all the other players should know about
	public const string GAMEMODE = "GAMEMODE"; //update gamemode
	public const string GAMEOVER = "GAMEOVER"; //update gamemode
	public const string RESTARTGAME = "RESTARTGAME"; //update gamemode
	public const string SHOOT = "SHOOT"; //update for position and rotation
	public const string RUNFUNCNOPARAMS = "RUNFUNCNOPARAMS"; //Run a function on the other clients
	public const string UPDATEANIMATION = "UPDATEANIMATION"; //Update said animation in animator
	public const string SPAWNMINION = "SPAWNMINION"; //Message used to spawn minions
	public const string SPAWNAI = "SPAWNAI"; //The AI is spawned
	public const string AIDIE = "AIDIE"; //AI died on the server side
	public const string MINIONDIE = "MINIONDIE"; //The minion dies over the network
	public const string SPAWNALLMINIONS = "SPAWNALLMINIONS"; //When a player connects spawn all the minions present in the level
	public const string SENDDAMAGE = "SENDDAMAGE";
}

public class NetworkParser{
	public static string ParseIn(string msgType, string s)
	{
		string msg = msgType + "|";
		msg += s;
		return msg;
	}

	public static string ParseIn(string msgType, float f)
	{
		string msg = msgType + "|"; 
		msg += f.ToString();
		return msg;
	}

	public static string ParseIn(string msgType, float[] floats)
	{
		string msg = msgType + "|";
		foreach(float f in floats)
		{
			msg += f + "|";
		}
		msg = msg.Trim('|', ' ');
		return msg;
	}

	public static string ParseIn(string msgType, Vector3 v)
	{
		string msg = msgType + "|";
		msg += v.x.ToString() + "%" + v.y.ToString() + "%" + v.z.ToString();
		return msg;
	}

	public static string ParseIn(string msgType, Quaternion q)
	{
		string msg = msgType + "|";
		msg += q.x.ToString() + "%" + q.y.ToString() + "%" + q.z.ToString() + "%" + q.w.ToString();
		return msg;
	}

	public static string ParseIn(string msgType, Vector3 v, Quaternion q, string gameObjectName){
		string msg = msgType + "|";
		Vector3 r = new Vector3(q.eulerAngles.x, q.eulerAngles.y, q.eulerAngles.z);
		msg += v.x.ToString() + "%" + v.y.ToString() + "%" + v.z.ToString() + "|";
		msg += r.x.ToString() + "%" + r.y.ToString() + "%" + r.z.ToString() + "|";
		msg += gameObjectName;
		return msg;
	}

	public static string ParseIn(string msgType, string[] arrayString)
	{
		string msg = msgType + "|";
		foreach(string s in arrayString){
			msg += s + "|";
		}
		msg = msg.Trim('|', ' ');

		return msg;
	}

	public static string ParseIn(string msgType, Vector3[] vectors){
		string msg = msgType + "|";
		foreach(Vector3 v in vectors){
			msg += v.x + "%" + v.y + "%" + v.z + "|";
		} 
		msg = msg.Trim('|', ' ');
		return msg;
	} 

	public static string GetMessageType(string msg)
	{
		string[] splitData = msg.Split('|');
		return splitData[0];
	}

	public static Vector3 ParseVector(string msg){
		string[] splitData = msg.Split('|');
		string[] vector = splitData[1].Split('%');
		return new Vector3(float.Parse(vector[0]), float.Parse(vector[1]), float.Parse(vector[2]));
	}

	public static Quaternion ParseQuaternion(string msg){
		string[] splitData = msg.Split('|');
		string[] q = splitData[1].Split('%');
		return new Quaternion(float.Parse(q[0]), float.Parse(q[1]), float.Parse(q[2]), float.Parse(q[3]));
	}

	public static void ParseTransform(string msg, out Vector3 position, out Quaternion rotation, out string ObjectName){
		string[] splitData = msg.Split('|');
		string[] v = splitData[1].Split('%');
		string[] q = splitData[2].Split('%');
		ObjectName = splitData.Length > 3 ? splitData[3] : "NULL";
		position = new Vector3(float.Parse(v[0]), float.Parse(v[1]), float.Parse(v[2]));
		rotation = Quaternion.Euler(float.Parse(q[0]), float.Parse(q[1]), float.Parse(q[2]));
	}

	public static string ParseString(string msg)
	{
		string[] splitData = msg.Split('|');
		return splitData[1];
	}

	public static string[] ParseArrayString(string msg)
	{
		string[] splitData = msg.Split('|');
		string[] arrayString = new string[splitData.Length - 1];
		for (int i = splitData.Length - 1; i > 0; i--){
			arrayString[i-1] = splitData[i];
		}

		return arrayString;
	}

	public static float ParseFloat(string msg)
	{
		string[] splitData = msg.Split('|');
		return float.Parse(splitData[1]);
	}
}

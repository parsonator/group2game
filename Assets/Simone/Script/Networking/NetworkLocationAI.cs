﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLocationAI : NetworkLocationBase {

	public bool ServerSide;
	public bool ClientSide;
	private float currentTime;
	
	void Start () {
		currentTime = Time.time;
	}
	
	void Update () {
		if(ServerSide){
			if(currentTime + updateRate < Time.time){
				Server.Send(NetworkParser.ParseIn(NetworkMsgType.TRANSFORM, transform.position, transform.rotation, this.name), Server.reliableChannel, ServerUtils.clients);
				currentTime = Time.time;
			}
			return;	
		}

		if(ClientSide){
			Interpolate();
		}
	}
}

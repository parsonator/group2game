﻿using System.Linq;
using UnityEngine;
 
public static class Helper
{
    public static GameObject FindInChildren(this GameObject go, string name)
    {
        // GameObject gameobj = null;
        // //gameobj = 
        // if((from x in go.GetComponentsInChildren<Transform>()
        //         where x.gameObject != null && x.gameObject.name == name
        //         select x.gameObject).Count() > 0){
        //     (from x in go.GetComponentsInChildren<Transform>()
        //         where x.gameObject != null && x.gameObject.name == name
        //         select x.gameObject).First();
        // }
        // return gameobj;

        Transform[] children = go.GetComponentsInChildren<Transform>(true);
        foreach(Transform t in children)
        {
            if(t.gameObject.name == name)
                return t.gameObject;
        }

        return null;
    }
}
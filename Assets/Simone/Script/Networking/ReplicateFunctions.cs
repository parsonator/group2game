﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Simple generic functions used to replicate functions over the network
//Functions in here are prefixed with REPLICATE_ so to not mistakenly call
//Their the replicated functions using component.SendMessage() 
public class ReplicateFunctions : MonoBehaviour {

//Transform GunBarrle, float accuracy, int teamLayer, LayerMask Mask = new LayerMask(), bool replicate = true
	// public void REPLICATE_Shoot(object[] obj){
	// 	GetComponent<Combat>().ShootGun((Transform)obj[0], (float)obj[1], (int)obj[2], false);
	// }

	public void REPLICATE_Die(){
		GetComponent<PlayerControler>().Die(false);
	}

	public void REPLICATE_Revive(object[] obj){
		GetComponent<PlayerControler>().Revive((int)obj[0], false);
	}
	public void REPLICATE_Ability(){
		GetComponent<PlayerAbility>().ability.UseAbility(false);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

/**Simple class to  that contains basic methods to interact with the list of clients in the server and client side*/
/**It makes the server script easier to read*/

public enum Team{
	NULL,
	BLUE,
	RED
}

public class ServerClient{
	public int connectionID { set; get; }
	// public string playerName { set; get; }
	public Team team { set; get; }
	public GameObject dummyPlayer { set; get; }
	public PlayerProfile playerProfile = new PlayerProfile();
	public bool HasMinion = false;
}

public class ServerUtils : MonoBehaviour{
	[Header("Player")]
	public GameObject dummyPlayer;
	public int ProximityCheckDistance = 500;
	
	[Header("AI")]
	public bool AIInLevel = true;
	public GameObject dummyAI;
	public GameObject AISpawnPoint;
	public int AIRespawnTime = 10;
	public int AIDamage = 5;

	public List<Gun_SO> allGunsReference;

	static public List<ServerClient> clients = new List<ServerClient>();
	public Match match = new TDM();

	private GameObject AIMonster;
	private bool Tracking = true;
	private bool CanWait = true;

	public void RestartServer()
	{
		NetworkTransport.Shutdown();
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		NetworkTransport.Init();
	}

	public bool ValidateIPAddress(GameObject canvas)
	{
		string IPAddress = canvas.FindInChildren("IPAddressInputField").GetComponent<UnityEngine.UI.InputField>().text;
		bool result = true;
		string[] data = IPAddress.Split('.');
		if(data.Length < 4)
			result = false;

		foreach(string s in data)
		{
			if(s.Trim() == "")
				result = false;
		}

		return result;
	}

	//it returns a list of all the clients apart from the client ID specified
	public List<ServerClient> GetOtherClients(int cnnID)
	{
		List<ServerClient> temp = new List<ServerClient>();
		foreach (ServerClient client in clients)
		{
			if (client.connectionID != cnnID){
				bool near = ProximityCheck(cnnID, client.connectionID);
				bool canSee = CanPlayersSeeEachOther(cnnID, client.connectionID);

				if (CanWait && !canSee)
				{
					StartCoroutine("CountTo", 3);
					CanWait = false;
				}

				if (canSee)
				{
					StopCoroutine("CountTo");
					CanWait = true;
					Tracking = true;
				}

				if (near || !near && (canSee || Tracking))
					temp.Add(client);
			}
		}

		return temp;
	}

	public string[] GetAllClientsWithMinion()
	{
		List<string> clientIDs = new List<string>();
		foreach(ServerClient client in clients)
		{
			if(client.HasMinion)
				clientIDs.Add(client.connectionID.ToString());
		}

		return clientIDs.ToArray();
	}

	//Counts down based on number and two clients know from each other 
	IEnumerator CountTo(int number)
	{
		yield return new WaitForSeconds(number);
		Tracking = false;
	}

	//The client is firstly spawned then we receive the name and we need to set it
	public void SetClientProfile(int cnnID, PlayerProfile playerProfile)
	{
		clients.Find(x => x.connectionID == cnnID).playerProfile = playerProfile;
	}

	//Return the specified client team
	public Team GetClientTeam(int cnnID)
	{
		return clients.Find(x => x.connectionID == cnnID).team;
	}

	//Removes the client from both the server side world and the list of clients
	public void RemoveClient(int cnnID)
	{
		Destroy(clients.Find(x => x.connectionID == cnnID).dummyPlayer);
		clients.Remove(clients.Find(x => x.connectionID == cnnID));
	}

	public void ClientDie(int cnnID)
	{
		Destroy(clients.Find(x => x.connectionID == cnnID).dummyPlayer);
	}

	public void RespawnClient(int cnnID)
	{
		if(clients.Find(x => x.connectionID == cnnID).dummyPlayer == null)
			clients.Find(x => x.connectionID == cnnID).dummyPlayer = Instantiate(dummyPlayer, Vector3.zero, Quaternion.identity);
	}

	//This adds a dummy client on the server side to keep track of the client position
	public void AddClient(int cnnID)
	{
		ServerClient c = new ServerClient();
		c.connectionID = cnnID;
		c.playerProfile.playerName = "TEMP";
		c.playerProfile.gunType = allGunsReference[0].name;
		c.dummyPlayer = Instantiate(dummyPlayer, Vector3.zero, Quaternion.identity);
		c.team = clients.Count % 2 == 0 ? Team.RED : Team.BLUE;
		clients.Add(c);
	}

	//Get all the other clients details - such as connection ID, playername and the team
	public List<string> GetOtherClientDetails(int cnnID)
	{
		List<string> temp = new List<string>();
		temp.Add(cnnID.ToString());
		foreach (ServerClient sc in clients)
		{
			temp.Add(sc.connectionID.ToString() + "%"
				+ sc.playerProfile.playerName + "%"
				+ sc.playerProfile.PlayerAbilityClass + "%"
				+ sc.playerProfile.gunType + "%"
				+ sc.team);
		}
		return temp;
	}

	//Updates the dummy client position in the server side world
	public void UpdateClientTransform(int cnnID, string msg)
	{
		Vector3 v = Vector3.zero;
		Quaternion q = Quaternion.identity;
		string objName;
		NetworkParser.ParseTransform(msg, out v, out q, out objName);

		if (objName.Contains("MainCharacter")){
			if(clients.Find(x => x.connectionID == cnnID).dummyPlayer == null)return;
			clients.Find(x => x.connectionID == cnnID).dummyPlayer.transform.position = v;
			clients.Find(x => x.connectionID == cnnID).dummyPlayer.transform.rotation = q;			
		}
	}

	//Get the player dummy based on connection ID
	public GameObject GetClientDummy(int cnnID)
	{
		var client = clients.Find(x => x.connectionID == cnnID);
		if(client != null)
			return clients.Find(x => x.connectionID == cnnID).dummyPlayer;
		else
			return null;
	}

	//Checks if two players are in line of sight and therefore can see each other
	public bool CanPlayersSeeEachOther(int cnnID1, int cnnID2)
	{
		//Get the two players dummy characters from the server side world
		GameObject client1 = GetClientDummy(cnnID1);
		GameObject client2 = GetClientDummy(cnnID2);
		if (client1 == null || client2 == null) return true;
		//Raycast between them and tell if they can see each other or not
		RaycastHit hit;
		Debug.DrawRay(client1.transform.position + Vector3.up, (client2.transform.position + Vector3.up) - (client1.transform.position + Vector3.up), Color.red, 0.1f);
		if (Physics.Raycast(client1.transform.position + Vector3.up, (client2.transform.position + Vector3.up) - (client1.transform.position + Vector3.up), out hit, 1000)){
			if(hit.transform.parent == null) return true;
			if(hit.transform.parent.gameObject != null && hit.transform.parent.gameObject == client2)
				return true;
			else return false;
		}
		else return false;
	}

	//Performs a proximity check between two clients based on a distance
	public bool ProximityCheck(int cnnID1, int cnnID2)
	{
		//Get the two players dummy characters from the server side world
		GameObject client1 = GetClientDummy(cnnID1);
		GameObject client2 = GetClientDummy(cnnID2);
		if (client1 == null || client2 == null) return true;
		//If the two players are in range then they should know about each other otherwise they shouldn't
		if ((client2.transform.position - client1.transform.position).magnitude > ProximityCheckDistance)
			return false;
		else return true;
	}

	//Instantiate the AI Monster
	public void SpawnAIMonster()
	{
		if(AIMonster == null && AIInLevel)
			AIMonster = Instantiate(dummyAI, AISpawnPoint.transform.position, AISpawnPoint.transform.rotation);
	}

	//Raycast to simulate player shooting at AI
	public void DummyPlayerShoot(Vector3 origin, Vector3 direction)
	{
		Debug.DrawRay(origin, direction * 10000, Color.green, 2);
		RaycastHit info;
		if(Physics.Raycast(origin, direction * 10000, out info, 10000)){
			if(info.transform.gameObject.tag == "Enemy"){
				DamageAI();
			}
		}
	}

	//Apply Damage to AI
	public void DamageAI()
	{
		AIMonster.GetComponent<Attributes>().Health -= AIDamage;
		if(AIMonster.GetComponent<Attributes>().Health <= 0){
			KillAI();
		}
	}

	//Kill the AI and start spawn count down
	public void KillAI()
	{
		Destroy(AIMonster);
		Server.Send(NetworkParser.ParseIn(NetworkMsgType.AIDIE, "1"), Server.reliableChannel, clients);
		AIMonster = null;
		StartCoroutine(CountdownAI(AIRespawnTime));
	}

	//Count down for AI to respawn in the level
	private IEnumerator CountdownAI(float respawnTime)
	{
		while(true){
			yield return new WaitForSeconds(respawnTime);
			SpawnAIMonster();
			break;
		}
	}
}

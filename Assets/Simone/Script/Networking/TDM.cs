﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDM : Match {

	public override void UpdateMatch()
	{
		BlueTeam.Score = BlueTeam.Kills;
		RedTeam.Score = RedTeam.Kills;
	}
	public override Team WinCondition()
	{
		if(RedTeam.Score >= 5)
			return RedTeam.team;
 		if(BlueTeam.Score >= 5)
			return BlueTeam.team;
		
		return Team.NULL;
	}


}

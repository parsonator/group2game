﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkLocationMinion : NetworkLocationBase {

	private PlayerControler controller;
	public bool Owner;
	public bool Local;
	private float currentTime;



	void Start () {
		controller = GetComponentInParent<PlayerControler>();
		if(controller == null) controller = GetComponent<PlayerControler>();
		if(controller == null) controller = GetComponentInChildren<PlayerControler>();
		currentTime = Time.time;
	}
	
	void Update () {
		if(Owner){
			if(currentTime + updateRate < Time.time){
				string msg = NetworkParser.ParseIn(NetworkMsgType.TRANSFORM, transform.position, transform.rotation, this.name) + "|-1";
				Client.Send(msg, Client.unreliableChannel);
				currentTime = Time.time;
			}
			return;	
		}

		if(Local){
			Interpolate();
		}
	}
}

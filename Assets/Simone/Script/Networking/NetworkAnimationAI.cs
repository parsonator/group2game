﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkAnimationAI : MonoBehaviour {

	public float updateRate = 0.5f;
	public bool serverSide = true;
	public bool clientSide = false;

	private Animator anim;
	private AnimationManager animManager;
	
	private Dictionary<string, string> animations = new Dictionary<string, string>();
	private float currentTime;
	private bool updatedLastFrame = false;
	private Dictionary<string, float> updatedValues = new Dictionary<string, float>();

	void Start () {		
		anim = GetComponentInChildren<Animator>();
		animManager = GetComponentInParent<TreeAnimationManager>();
		for (int i = 0; i < animManager.anims.Count; i++){
			if(animManager.replicate[i] == true)
				animations.Add(animManager.anims[i], animManager.valueTypes[i]);			
		}
		currentTime = Time.time;
	}
	
	void LateUpdate () {
		if(serverSide){
			if(currentTime + updateRate < Time.time ){
				foreach(KeyValuePair<string, string> entry in animations){
					switch (entry.Value) {
						case "float":
							float floatValue = anim.GetFloat(entry.Key);
							Server.Send(NetworkParser.ParseIn(NetworkMsgType.UPDATEANIMATION, new string[]{ entry.Key, floatValue.ToString(), "float" , "-1"}), Server.unreliableChannel, ServerUtils.clients);
							break;
						case "bool":
							bool boolValue = anim.GetBool(entry.Key);
							Server.Send(NetworkParser.ParseIn(NetworkMsgType.UPDATEANIMATION, new string[]{ entry.Key, boolValue.ToString(), "bool", "-1" }), Server.unreliableChannel, ServerUtils.clients);
							break;
						default:
							break;
					}
				}
				currentTime = Time.time;
			}
		}
	}

	public void UpdateAnimation(string property, string value, string dataType){
		if(clientSide){
			if(anim == null) return;
			updatedLastFrame = true;
			switch (dataType){
				case "float":
					anim.SetFloat(property, float.Parse(value));
					break;
				case "bool":
					if(property == "Idle" && value == "false") Debug.Log("Idle Reset");
					anim.SetBool(property, bool.Parse(value));
					break;
				default:
					break;
			}
		}
	}
}

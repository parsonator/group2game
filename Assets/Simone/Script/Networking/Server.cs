﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text;
using UnityEngine.SceneManagement;

public class Server : MonoBehaviour
{
	public int port = 5701;
	public string IPAddress = "127.0.0.1";
	public bool getPublicAddressAutomatically = true;
	public InputField IPAddressInputField;
	public Text runningFeedback;
	public Toggle getIPAddressAuto;
	public int numOfPlayerToSpawnAI = 2;
	private const int MAX_CONNECTION = 100;
	static public int hostID;
	static public int reliableChannel;
	static public int unreliableChannel;
	private bool isStarted = false;
	private bool ServerRuning;
	private byte error;
	private ServerUtils serverUtils;

	void Awake()
	{
		IPAddressInputField.readOnly = getPublicAddressAutomatically;
		getIPAddressAuto.isOn = getPublicAddressAutomatically;
	}

	public void StartServer(GameObject canvas)
	{
		serverUtils = GetComponent<ServerUtils>();
		if(!serverUtils.ValidateIPAddress(canvas) && !getPublicAddressAutomatically) return;

		IPAddress = canvas.FindInChildren("IPAddressInputField").GetComponent<UnityEngine.UI.InputField>().text;
		port = int.Parse(canvas.FindInChildren("PortInputField").GetComponent<UnityEngine.UI.InputField>().text);

		if (getPublicAddressAutomatically) 
			StartCoroutine(CheckIP(canvas));
		else 
		{
			Connect();
			runningFeedback.gameObject.SetActive(true);
			canvas.FindInChildren("IPAddressInputField").GetComponent<UnityEngine.UI.InputField>().readOnly = true;
			canvas.FindInChildren("PortInputField").GetComponent<UnityEngine.UI.InputField>().readOnly = true;
			canvas.FindInChildren("StartServerBtn").GetComponent<Button>().interactable = false;
		}
	}

	void Update()
	{
		if (!ServerRuning)
			StartCoroutine (RunServer ());
	}

	//Creates the server connection opening the specified port and using the specified IP address
	void Connect()
	{
		NetworkTransport.Init();
		ConnectionConfig cc = new ConnectionConfig();

		reliableChannel = cc.AddChannel(QosType.Reliable);
		unreliableChannel = cc.AddChannel(QosType.Unreliable);

		HostTopology topo = new HostTopology(cc, MAX_CONNECTION);

		hostID = NetworkTransport.AddHost(topo, port, IPAddress);
		isStarted = true;
	}

	IEnumerator RunServer()
	{
		//If the server hasn't started yet don't update
		while (isStarted)
		{        //Data to read using the Receive function
			ServerRuning = true;
			int recHostId;
			int connectionID;
			int channelId;
			byte[] recBuffer = new byte[1024];
			int bufferSize = 1024;
			int dataSize;
			byte error;
			//Read received data
			NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionID, out channelId, recBuffer, bufferSize, out dataSize, out error);
			do
			{
				//switch on the type of data received
				switch (recData){
					case NetworkEventType.ConnectEvent:   //Sombody connected to the server
						OnConnection(connectionID);
						break;
					case NetworkEventType.DataEvent:       //Data has been received
						ProcessReceiveData(recBuffer, dataSize, connectionID);
						break;
					case NetworkEventType.DisconnectEvent: //Somebody Disconnected from the server
						OnDisconnect(connectionID);
						break;
				}
				recData = NetworkTransport.Receive(out recHostId, out connectionID, out channelId, recBuffer, bufferSize, out dataSize, out error);
			} while (recData != NetworkEventType.Nothing);
			yield return new WaitForSeconds(0.0001f);

		}
	}

	//Process the data received in the udpate function
	private void ProcessReceiveData(byte[] recBuffer, int dataSize, int connectionID)
	{
		string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
		string[] splitData = msg.Split('|');
		switch (splitData[0]){
			case NetworkMsgType.NAMEIS:
				OnNameIs(connectionID, msg);
				break;
			case NetworkMsgType.TRANSFORM:
				OnTransform(connectionID, msg);
				break;
			case NetworkMsgType.RUNFUNCNOPARAMS:
				RunFunction(connectionID, msg);
				break;
			case NetworkMsgType.GAMEMODE:
				UpdateServerGameMode(connectionID, msg);
				break;
			case NetworkMsgType.SHOOT:
				SendShoot(connectionID, msg);
				break;
			case NetworkMsgType.UPDATEANIMATION:
				UpdateAnimation(connectionID, msg);
				break;
			case NetworkMsgType.SPAWNMINION:
				SpawnMinion (connectionID, msg);
				break;
			case NetworkMsgType.HEALTH:
				UpdateHealth(connectionID, msg);
				break;
			case NetworkMsgType.SENDDAMAGE:
				SendDamage(connectionID, msg);
				break;
			case NetworkMsgType.MINIONDIE: 
				KillMinion(connectionID, msg);
				break;
			default:
				break;
		}
	}

	private void UpdateServerGameMode(int cnnID, string msg)
	{
		string[] splitData = msg.Split('|');
		string team = splitData[1];
		string Score = splitData[2];
		string Kills = splitData[3];

		if ((Team)int.Parse(team) == Team.RED)
			serverUtils.match.RedTeam.UpdateMatch(int.Parse(Kills), int.Parse(Score));
		else
			serverUtils.match.BlueTeam.UpdateMatch(int.Parse(Kills), int.Parse(Score));

		serverUtils.match.UpdateMatch();

		CallGameModeUpdate();
	}

	private void CallGameModeUpdate()
	{
		Send(NetworkParser.ParseIn(NetworkMsgType.GAMEMODE, new string[]{
			serverUtils.match.RedTeam.Kills.ToString(), serverUtils.match.RedTeam.Score.ToString(),
			serverUtils.match.BlueTeam.Kills.ToString(), serverUtils.match.BlueTeam.Score.ToString(),
		}), reliableChannel, ServerUtils.clients);
		Team winingTeam = serverUtils.match.WinCondition();
		if(winingTeam != Team.NULL){
			Send(NetworkParser.ParseIn(NetworkMsgType.GAMEOVER, new string[]{
				winingTeam.ToString()
				}), reliableChannel, ServerUtils.clients);
			StartCoroutine(SendRestart());
		}
	}

	IEnumerator SendRestart()
	{
		yield return new WaitForSeconds(5);
		serverUtils.match.RedTeam.RestetMatch();
		serverUtils.match.BlueTeam.RestetMatch();

		Send(NetworkParser.ParseIn(NetworkMsgType.RESTARTGAME, "restart"), reliableChannel, ServerUtils.clients);
		CallGameModeUpdate();
	}

	//Once the connection is established the client tells us its name
	private void OnNameIs(int cnnID, string msg)
	{
		string[] d = msg.Split('|');
		string playerName = d[1];
		PlayerAbilityClass playerAbilityClass = (PlayerAbilityClass)System.Enum.Parse(typeof(PlayerAbilityClass), d[2], true);

		Gun_SO Gun = serverUtils.allGunsReference.Find(x => x.name == d[3]);
		//Assign the name to the right connection ID
		serverUtils.SetClientProfile(cnnID, new PlayerProfile(playerName,playerAbilityClass,Gun.name));

		//Tell other players that one more person has connected
		//They need to spawn the character		
		// Send(NetworkParser.ParseIn(NetworkMsgType.CNN,
		//     new string[] { playerName, cnnID.ToString(),
		//     serverUtils.GetClientTeam(cnnID).ToString() }),
			
		//      reliableChannel, ServerUtils.clients);

		Send(NetworkParser.ParseIn(NetworkMsgType.CNN, new string[] {
			cnnID.ToString(),
			playerName, 
			playerAbilityClass.ToString(),
			serverUtils.GetClientTeam(cnnID).ToString(), 
			Gun.name}), 
			reliableChannel,ServerUtils.clients);

		if(ServerUtils.clients.Count >= numOfPlayerToSpawnAI)
			serverUtils.SpawnAIMonster();
	}

	//Notifies all the other clients about the specified client change in transform
	private void OnTransform(int cnnID, string msg)
	{
		serverUtils.UpdateClientTransform(cnnID, msg);
		List<ServerClient> temp = serverUtils.GetOtherClients(cnnID);
		msg += "|" + cnnID.ToString();
		Send(msg, unreliableChannel, temp);
	}

	//On connection a client is created and the rest of the clients are notified
	private void OnConnection(int cnnID)
	{
		// add player to list
		serverUtils.AddClient(cnnID);
		//send back the player id
		//request the name of the player and send the other players name
		List<string> temp = serverUtils.GetOtherClientDetails(cnnID);
		string msg = NetworkParser.ParseIn(NetworkMsgType.ASKNAME, temp.ToArray());
		Send(msg, reliableChannel, cnnID);

		Send(NetworkParser.ParseIn(NetworkMsgType.SPAWNALLMINIONS, serverUtils.GetAllClientsWithMinion()), reliableChannel, cnnID);
		CallGameModeUpdate();
	}
	//Disconnects the client and updates all the other clients about it
	private void OnDisconnect(int cnnID)
	{
		//remove player from client list 
		serverUtils.RemoveClient(cnnID);
		//update everyone about the player disconnecting
		Send(NetworkParser.ParseIn(NetworkMsgType.DC, cnnID), reliableChannel, ServerUtils.clients);
	}

	//It runs a function on all the clients apart from the one who sent
	private void RunFunction(int cnnID, string msg)
	{
		string[] splitData = msg.Split('|');
		if(splitData[1] == "REPLICATE_Die")
			serverUtils.ClientDie(cnnID);
	
		if(splitData[1] == "REPLICATE_Revive")
			serverUtils.RespawnClient(cnnID);
		


		List<ServerClient> temp = serverUtils.GetOtherClients(cnnID);
		msg += "|" + cnnID.ToString();
		Send(msg, reliableChannel, temp);
	}

	//Update animation states on all clients apart from the one who sent
	private void UpdateAnimation(int cnnID, string msg)
	{
		List<ServerClient> temp = serverUtils.GetOtherClients(cnnID);
		msg += "|" + cnnID.ToString();
		Send(msg, reliableChannel, temp);
	}

	void SpawnMinion(int connectionID, string msg)
	{
		ServerUtils.clients.Find(x => x.connectionID == connectionID).HasMinion = true;
		List<ServerClient> temp = serverUtils.GetOtherClients (connectionID);
		msg += "|" + connectionID.ToString();
		Send (msg, reliableChannel, temp);
	}

	private void SendShoot(int cnnID, string msg)
	{
		List<ServerClient> temp = serverUtils.GetOtherClients(cnnID);
		string[] splitData = msg.Split('|');
		string[] sDir = splitData[5].Split('%');
		Vector3 dir = new Vector3(float.Parse(splitData[1]), float.Parse(splitData[2]), float.Parse(splitData[3]));
		Vector3 origin = new Vector3(float.Parse(sDir[0]), float.Parse(sDir[1]), float.Parse(sDir[2]));
		serverUtils.DummyPlayerShoot(origin, dir);
		msg += "|" + cnnID.ToString();
		Send(msg, reliableChannel, temp);
	}


	private void UpdateHealth(int cnnID, string msg)
	{
		List<ServerClient> temp = serverUtils.GetOtherClients(cnnID);
		string[] splitData = msg.Split('|');
		msg += '|' + cnnID.ToString();
		Send(msg, reliableChannel, temp);
	}

	private void SendDamage(int cnnID, string msg)
	{
		string[] splitData = msg.Split('|');
		string newMsg = NetworkParser.ParseIn(NetworkMsgType.SENDDAMAGE, splitData[2]);
		newMsg += "|" + cnnID.ToString();
		Send(newMsg, Server.reliableChannel, int.Parse(splitData[1]));
	}
	
	private void KillMinion(int connectionID, string msg)
	{
		ServerUtils.clients.Find(x => x.connectionID == connectionID).HasMinion = false;
		msg += "|" + connectionID;
		Send(msg, Server.reliableChannel, serverUtils.GetOtherClients(connectionID));
	}

	//Send message with the specified channel to the specified client
	static public void Send(string message, int channelID, int cnnID)
	{
		List<ServerClient> c = new List<ServerClient>();
		c.Add(ServerUtils.clients.Find(x => x.connectionID == cnnID));
		Send(message, channelID, c);
	}

	//Send message with the specified channel to the list of specified clients
	static public void Send(string message, int channelID, List<ServerClient> c)
	{
		byte[] msg = Encoding.Unicode.GetBytes(message);
		foreach (ServerClient sc in c)
		{
			byte error;
			NetworkTransport.Send(hostID, sc.connectionID, channelID, msg, message.Length * sizeof(char), out error);
		}
	}

	private IEnumerator CheckIP(GameObject canvas)
	{
		WWW myExtIPWWW = new WWW("http://checkip.dyndns.org");
		if (myExtIPWWW == null) yield return 0;
		yield return myExtIPWWW;
		string myExtIP = myExtIPWWW.text;
		myExtIP = myExtIP.Substring(myExtIP.IndexOf(":") + 1);
		myExtIP = myExtIP.Substring(0, myExtIP.IndexOf("<"));
		IPAddress = myExtIP.Trim();

		canvas.FindInChildren("IPAddressInputField").GetComponent<UnityEngine.UI.InputField>().text = IPAddress;
		canvas.FindInChildren("IPAddressInputField").GetComponent<UnityEngine.UI.InputField>().readOnly = true;
		canvas.FindInChildren("PortInputField").GetComponent<UnityEngine.UI.InputField>().readOnly = true;
		canvas.FindInChildren("StartServerBtn").GetComponent<Button>().interactable = false;

		Connect();

		runningFeedback.gameObject.SetActive(true);
	}

	public void SetIPAddressAuto()
	{
		getPublicAddressAutomatically = !getPublicAddressAutomatically;
		IPAddressInputField.readOnly = !IPAddressInputField.readOnly;
	}
}

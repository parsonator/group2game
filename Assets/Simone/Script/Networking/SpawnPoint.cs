﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SpawnPoint : MonoBehaviour {

	public Team team = Team.BLUE;
#if UNITY_EDITOR
	void Reset(){
		OnValidate();	
		NameObject();	
	}

	void OnValidate(){
		if(team == Team.BLUE)
			AssignLabelBlue(transform.gameObject);
		else if(team == Team.RED)
			AssignLabelRed(transform.gameObject);
	}

	void NameObject(){
		int last = 1;
		GameObject[] objs = UnityEngine.Object.FindObjectsOfType<GameObject>() ;
		foreach(GameObject obj in objs){
			if(obj.name.Contains("SpawnPoint") &&  obj != gameObject){
				string[] s = obj.name.Split('-');
				if(int.Parse(s[1]) > last){
					last = int.Parse(s[1]);
				}
			}
		}

		gameObject.name = "SpawnPoint-" + (last + 1).ToString();
	}

	public static void AssignLabelBlue(GameObject g){
		Texture2D tex = EditorGUIUtility.IconContent("sv_label_1").image as Texture2D;
		Type editorGUIUtilityType  = typeof(EditorGUIUtility);
		BindingFlags bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		object[] args = new object[] {g, tex};
		editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
	}

	public static void AssignLabelRed(GameObject g){
		Texture2D tex = EditorGUIUtility.IconContent("sv_label_6").image as Texture2D;
		Type editorGUIUtilityType  = typeof(EditorGUIUtility);
		BindingFlags bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		object[] args = new object[] {g, tex};
		editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
	}

#endif
	
}

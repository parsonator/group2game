﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakeMapObject : MonoBehaviour {

	public Image image;

	void Start (){
		MiniMapController.RegisterMapObject(this.gameObject, image);
	}
	
	void OnDestroy(){
		MiniMapController.RemoveMapObject(this.gameObject);
	}
}

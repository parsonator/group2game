﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObject
{
	public Image icon { get; set; }
	public GameObject owner { get; set; }
}

public class MiniMapController : MonoBehaviour {

	public Transform playerPos;
	public Camera mapCamera;
	public int iconSize = 32;
	
	private static List<MapObject> mapObjects = new List<MapObject>();

	void Update()
	{
		if(mapCamera != null)
			DrawMapIcon();
	}

	public static void RegisterMapObject(GameObject obj, Image image)
	{
		//Create a new map object and add it to the list
		Image i = Instantiate(image);
		mapObjects.Add(new MapObject(){owner = obj, icon = i});
	}
	
	public static void RemoveMapObject(GameObject obj)
	{
		//Look for the specific map object remove from the list and construct new list
		List<MapObject> newList = new List<MapObject>();
		for(int i = 0; i < mapObjects.Count; i++)
		{
			if(mapObjects[i].owner == obj)
			{
				Destroy(mapObjects[i].icon);
				continue;
			}
			else
				newList.Add(mapObjects[i]);
		}
		mapObjects.RemoveRange(0, mapObjects.Count);
		mapObjects.AddRange(newList);
	}

	void DrawMapIcon()
	{
		//Foreach object convert world to screen coordinate and place the icon on screen
		foreach(MapObject mo in mapObjects)
		{
			Vector3 screenPos = mapCamera.WorldToViewportPoint(mo.owner.transform.position);
			mo.icon.transform.SetParent(this.transform);

			RectTransform rt = this.GetComponent<RectTransform>();
			//little trick to get the bottom left corner offset of the rect
			Vector3[] corners = new Vector3[4];
			rt.GetWorldCorners(corners);
			//we need to add the panel width and height to the screen position to properly offset the icon
			screenPos.x *= rt.rect.width;
			screenPos.y *= rt.rect.height;

			//Set the local scale to be one, set the bottom and top corner of the image to make it the right sizes
			screenPos.z = 0;
			mo.icon.rectTransform.localScale = Vector3.one;
			mo.icon.rectTransform.offsetMin = screenPos - Vector3.one * (iconSize/2);
			mo.icon.rectTransform.offsetMax = screenPos + Vector3.one * (iconSize/2);

		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class PlayerAnalytics : MonoBehaviour
{

    public static void OnDeath(string PlayerAbilityClass, string killerClass, string deathFrom)
    {
        Analytics.CustomEvent("Killed", new Dictionary<string, object>
        {
            { "PlayerAbilityClass", PlayerAbilityClass },
            { "killerClass", killerClass },
            { "deathFrom", deathFrom }
        });
    }

    public static void GunKills(string gun)
    {
        Analytics.CustomEvent("GunKills", new Dictionary<string, object>
        {
            { "Gun", gun },
        });
    }

    public static void OnUseAbility(string AbilityName)
    {
        Analytics.CustomEvent("Ability", new Dictionary<string, object>
        {
            { "AbilityType", AbilityName}
        });
    }

    public static void AbilityKills(string ability)
    {
        Analytics.CustomEvent("AbilityKills", new Dictionary<string, object>
        {
            { "AbilityType", ability }
        });
    }
    public static void AbilityDamage(string ability)
    {
        Analytics.CustomEvent("AbilityDamage", new Dictionary<string, object>
        {
            { "AbilityType", ability }
        });
    }
}
